package xyz.thinkglobal.models;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Data
@Entity
@Table(name = "waiting_queue_enrollees",
        uniqueConstraints = @UniqueConstraint(columnNames = {"enrollee_id", "waiting_queue_id"}))
@ToString(exclude = {"enrollee", "waitingQueue"})
@EqualsAndHashCode(exclude = {"enrollee", "waitingQueue"})
public class WaitingQueueEnrollee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "waiting_queue_id")
    private WaitingQueue waitingQueue;
    @ManyToOne
    @JoinColumn(name = "enrollee_id")
    private Enrollee enrollee;
    @Column(name = "status")
    private String status;
}
