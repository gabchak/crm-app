package xyz.thinkglobal.models;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.Set;

@Getter
@Setter
@Entity
@EqualsAndHashCode(exclude = {"studyGroups", "waitingQueue"})
@Table(name = "study_class")
public class StudyClass {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "name")
    private int name;

    @OneToMany(mappedBy = "studyClass", fetch = FetchType.LAZY)
    private Set<StudyGroup> studyGroups;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_school_id")
    private School school;

    @OneToOne(mappedBy = "studyClass", fetch = FetchType.LAZY)
    private WaitingQueue waitingQueue;
}
