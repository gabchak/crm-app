package xyz.thinkglobal.models;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.Set;

@Getter
@Setter
@Entity
@ToString(exclude = {"studyClasses", "events", "positions", "address"})
@Table(name = "school")
public class School {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "code")
    private String code;
    @Column(name = "term_of_partnership")
    private String termOfPartnership;
    @Column(name = "status")
    private String status;
    @Column(name = "zoho_id")
    private Long zohoId;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "address_id")
    private Address address;


    @OneToMany(mappedBy = "school", fetch = FetchType.LAZY)
    private Set<StudyClass> studyClasses;

    @OneToMany(mappedBy = "school", fetch = FetchType.LAZY)
    private Set<Event> events;

    @OneToMany(mappedBy = "school", fetch = FetchType.LAZY)
    private Set<Position> positions;

}
