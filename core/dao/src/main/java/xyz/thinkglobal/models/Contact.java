package xyz.thinkglobal.models;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import java.time.LocalDate;
import java.util.Set;

@Setter
@Getter
@Entity
@EqualsAndHashCode(callSuper = true, exclude = {"registrationAddress", "residenceAddress", "students", "enrolleeSet"})
@ToString(exclude = {"registrationAddress", "residenceAddress", "students", "enrolleeSet"})
@PrimaryKeyJoinColumn(name = "id")
@Table(name = "contact")
public class Contact extends User {

    @Column(name = "series_and_passport_number")
    private String seriesAndPassportNumber;
    @Column(name = "date_of_issue_of_passport")
    private LocalDate dateOfIssueOfPassport;
    @Column(name = "who_issued_passport")
    private String whoIssuedPassport;
    @Column(name = "inn")
    private Integer inn;
    @Column(name = "lead")
    private Long lead;

    @ManyToOne
    @JoinColumn(name = "registration_address_id")
    private Address registrationAddress;

    @ManyToOne
    @JoinColumn(name = "residence_address_id")
    private Address residenceAddress;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "familyRepresentativeSet")
    private Set<Student> students;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "familyRepresentativeSet")
    private Set<Enrollee> enrolleeSet;
}
