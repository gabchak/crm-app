package xyz.thinkglobal.models;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import java.util.Set;

@Getter
@Setter
@ToString(exclude = {"bookingDetails", "waitingQueueEnrolleeSet", "familyRepresentativeSet"})
@EqualsAndHashCode(callSuper = true, exclude = {"bookingDetails", "waitingQueueEnrolleeSet", "familyRepresentativeSet"})
@Entity
@PrimaryKeyJoinColumn(name = "id")
@Table(name = "enrollee")
public class Enrollee extends Child {

    @Column(name = "primary_queue")
    private Long primaryQueue;

    @OneToOne(mappedBy = "enrollee", fetch = FetchType.LAZY)
    private BookingDetails bookingDetails;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "enrollee", cascade = CascadeType.ALL)
    private Set<WaitingQueueEnrollee> waitingQueueEnrolleeSet;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "contact_enrollee",joinColumns = {@JoinColumn(name = "enrollee_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "contact_id", referencedColumnName = "id")})
    private Set<Contact> familyRepresentativeSet;
}
