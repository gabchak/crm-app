package xyz.thinkglobal.models.filters;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.jpa.domain.Specification;
import xyz.thinkglobal.models.Enrollee;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

@Getter
@Setter
public class EnrolleeFilter extends ChildFilter<Enrollee>  implements Specification<Enrollee> {

    @Override
    protected List<Predicate> buildPredicates(Root<Enrollee> root, CriteriaQuery<?> query,
                                              CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicates = super.buildPredicates(root, query, criteriaBuilder);


        return predicates;
    }
}
