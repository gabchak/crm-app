package xyz.thinkglobal.models;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true, exclude = {"city", "leadEventSet"})
@ToString(exclude = {"city", "leadEventSet"})
@Entity
@PrimaryKeyJoinColumn(name = "id")
@Table(name = "lead", uniqueConstraints = {@UniqueConstraint(columnNames = {"email"})})
public class Lead extends Person {

    @Column(name = "email")
    private String email;
    @Column(name = "status")
    private String status;
    @Column(name = "how_you_find_us")
    private String howYouFindUs;
    @Column(name = "phone")
    private String phone;
    @Column(name = "additional_phone")
    private String additionalPhone;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_CITY_ID")
    private City city;

    @OneToMany(mappedBy = "lead", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<LeadEvent> leadEventSet;
}
