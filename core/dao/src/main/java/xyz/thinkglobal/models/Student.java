package xyz.thinkglobal.models;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Set;

@Getter
@Setter
@ToString(exclude = {"studyGroup"})
@EqualsAndHashCode(callSuper = true, exclude = {"studyGroup"})
@Entity
@Table(name = "student")
public class Student extends Child {
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_study_group_id")
    private StudyGroup studyGroup;


    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "contact_students",joinColumns = {@JoinColumn(name = "students_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "family_representative_set_id", referencedColumnName = "id")})
    private Set<Contact> familyRepresentativeSet;
}
