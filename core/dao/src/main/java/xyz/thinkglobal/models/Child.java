package xyz.thinkglobal.models;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.time.LocalDate;

@Setter
@Getter
@Entity
@Table(name = "child", uniqueConstraints = {@UniqueConstraint(columnNames = {"birth_certificate"})})
public class Child extends Person {

    @Column(name = "date_of_birth")
    private LocalDate dateOfBirth;
    @Column(name = "birth_certificate")
    private String birthCertificate;

    @Column(name = "major_family_representative")
    private Long majorFamilyRepresentative;
}
