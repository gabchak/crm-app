package xyz.thinkglobal.models.filters;

import lombok.Getter;
import lombok.Setter;
import xyz.thinkglobal.models.Child;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

@Setter
@Getter
public class ChildFilter<T extends Child> extends PersonFilter<T> {

    @Override
    protected List<Predicate> buildPredicates(Root<T> root, CriteriaQuery<?> query,
                                              CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicates = super.buildPredicates(root, query, criteriaBuilder);
        return predicates;
    }
}
