package xyz.thinkglobal.models.filters;

import lombok.Getter;
import lombok.Setter;
import org.springframework.util.StringUtils;
import xyz.thinkglobal.models.Lead;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

@Getter
@Setter
public class LeadFilter extends PersonFilter<Lead>{

    private String email;
    private String phone;

    @Override
    protected List<Predicate> buildPredicates(Root<Lead> root, CriteriaQuery<?> query,
                                              CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicates = super.buildPredicates(root, query, criteriaBuilder);
        if (!StringUtils.isEmpty(getEmail())) {
            predicates.add(criteriaBuilder.equal(root.get("email"), getEmail()));
        }
        if (!StringUtils.isEmpty(getPhone())) {
            predicates.add(criteriaBuilder.equal(root.get("phone"), getPhone()));
        }
        return predicates;
    }
}
