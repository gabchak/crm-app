package xyz.thinkglobal.models;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import java.util.Set;

@Getter
@Setter
@Entity
@EqualsAndHashCode(callSuper = true)
@PrimaryKeyJoinColumn(name = "id")
@Table(name = "speaker")
public class Speaker extends User {
    //it's text field witch will show on event page. It isn't Position witch give you access to CRM.
    @Column(name = "position")
    private String position;
    @Column(name = "photo")
    private String photo; //TODO: implementation

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "event_speaker", joinColumns = {@JoinColumn(name = "events_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "speaker_id", referencedColumnName = "id")})
    private Set<Event> events;
}
