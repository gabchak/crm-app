package xyz.thinkglobal.models;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.time.LocalDate;
import java.util.Set;

@Getter
@Setter
@Entity
@EqualsAndHashCode(exclude = {"openDate", "closeDate",
        "status", "studyClass", "students", "booking"})
@Table(name = "study_group")
public class StudyGroup {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "number_of_seats")
    private int numberOfSeats;
    @Column(name = "open_date")
    private LocalDate openDate;
    @Column(name = "close_date")
    private LocalDate closeDate;
    @Column(name = "status")
    private String status;
    @Column(name = "zoho_id")
    private Long zohoId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "study_class_id")
    private StudyClass studyClass;

    @OneToMany(mappedBy = "studyGroup", fetch = FetchType.LAZY)
    private Set<Student> students;

    @OneToOne(mappedBy = "studyGroup", fetch = FetchType.LAZY)
    private Booking booking;
}
