package xyz.thinkglobal.models.filters;

import lombok.Data;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;
import xyz.thinkglobal.models.Person;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Data
public class PersonFilter<T extends Person> implements Specification<T> {

    private String firstName;
    private String lastName;

    public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicates = buildPredicates(root, query, criteriaBuilder);
        if (predicates != null && !predicates.isEmpty()) {
            return predicates.size() > 1 ? criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]))
                    : predicates.get(0);
        }
        return null;
    }

    protected List<Predicate> buildPredicates(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicates = new ArrayList<>();
        if (!StringUtils.isEmpty(getFirstName())) {
            predicates.add(criteriaBuilder.equal(root.get("firstName"), getFirstName()));
        }
        if (!StringUtils.isEmpty(getLastName())) {
            predicates.add(criteriaBuilder.equal(root.get("lastName"), getLastName()));
        }
        return predicates;
    }

}
