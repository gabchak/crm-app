package xyz.thinkglobal.models;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Set;

@Getter
@Setter
@Entity
@ToString(exclude = {"school", "speakers", "leadEventSet"})
@Table(name = "event")
public class Event {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    @Column(name = "start_date")
    private LocalDateTime startDate;
    @Column(name = "status")
    private String status;
    @Column(name = "type")
    private String type;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_SCHOOL_ID")
    private School school;

    @ManyToMany(mappedBy = "events", fetch = FetchType.LAZY)
    private Set<Speaker> speakers;

    @OneToMany(mappedBy = "event", fetch = FetchType.LAZY)
    private Set<LeadEvent> leadEventSet;

    public String getFormattedStartDate() {
        return startDate.format(DateTimeFormatter.ofPattern("dd.MM.yyyy"));
    }

    public String getFormattedStartTime() {
        return startDate.format(DateTimeFormatter.ofPattern("HH:mm"));
    }
}
