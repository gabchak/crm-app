package xyz.thinkglobal.models;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
//
//@Entity
//@Setter
//@Getter
public class Counterparty {
    @Id
    private String id;

    @Column(name = "code_edrpou")
    private Integer codeEdrpou;

    @Column(name = "full_name")
    private String fullName;

    @Column(name = "short_name")
    private String shortName;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "Director")
    private User director;

    @Column(name = "acts_on_basis")
    private String actsOnBasis;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "address")
    private Address address;

    @Column
    private String requisites;



}
