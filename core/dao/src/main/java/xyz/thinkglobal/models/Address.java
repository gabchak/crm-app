package xyz.thinkglobal.models;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Getter
@Setter
@Entity
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"flat", "house", "street", "fk_city_id"})})
@EqualsAndHashCode(exclude = {"city"})
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "street")
    private String street;
    @Column(name = "house")
    private String house;
    @Column(name = "flat")
    private String flat;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_city_id")
    private City city;

    public String getFormattedAddress() {
        StringBuilder address = new StringBuilder();
        address.append("м.");
        address.append(city.getName());
        address.append(", ");
        address.append(street);
        address.append(" ");
        address.append(house);

        if (flat != null) {
            address.append(", кв.").append(flat);
        }
        return address.toString();
    }
}

