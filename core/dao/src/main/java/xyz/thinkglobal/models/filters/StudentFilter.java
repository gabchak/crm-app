package xyz.thinkglobal.models.filters;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.jpa.domain.Specification;
import xyz.thinkglobal.models.Student;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Set;

@Setter
@Getter
public class StudentFilter extends ChildFilter<Student> implements Specification<Student> {

    private Long studyGroup;
    private Set<Long> familyRepresentativeSet;

    @Override
    protected List<Predicate> buildPredicates(Root<Student> root, CriteriaQuery<?> query,
                                              CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicates = super.buildPredicates(root, query, criteriaBuilder);
        if (familyRepresentativeSet != null && !familyRepresentativeSet.isEmpty()) {

        }
        if (studyGroup != null) {

        }
        return predicates;
    }
}
