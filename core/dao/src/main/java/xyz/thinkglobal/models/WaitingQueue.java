package xyz.thinkglobal.models;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = {"waitingQueueEnrolleeList", "studyClass"})
@EqualsAndHashCode(exclude = {"waitingQueueEnrolleeList", "studyClass"})
@Table(name = "waiting_queue")
public class WaitingQueue implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "unique_code")
    private String uniqueCode;
    @Column(name = "registration_date")
    private LocalDate registrationDate;
    @Column(name = "status")
    private String status;
    @Column(name = "expiration_date")
    private LocalDate expirationDate;
    @Column(name = "condition_for_booking_opening")
    private String conditionForBookingOpening;
    @Column(name = "has_scheduled_study_room")
    private String hasScheduledStudyRoom;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "waitingQueue", orphanRemoval = true)
    @OrderBy(value = "id")
    private List<WaitingQueueEnrollee> waitingQueueEnrolleeList;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "study_class_id")
    private StudyClass studyClass;

}
