package xyz.thinkglobal.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import xyz.thinkglobal.models.School;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface SchoolRepository extends JpaRepository<School, Long> {

    List<School> findAllByAddress_City_Id(Long id);

    School findByStudyClasses_StudyGroups_Id(Long id);

    Optional<School> findByZohoId(Long id);

    Set<School> findAllByZohoIdIsNotNull();
}
