package xyz.thinkglobal.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import xyz.thinkglobal.models.WaitingQueue;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface WaitingQueueRepository extends
        JpaRepository<WaitingQueue, Long> {
    Optional<WaitingQueue> findByStudyClass_Id(Long classId);

    List<WaitingQueue> findAllByStudyClass_School_Id(Long id);

    List<WaitingQueue> findAllByStudyClass_School_IdIn(List<Long> ids);
}
