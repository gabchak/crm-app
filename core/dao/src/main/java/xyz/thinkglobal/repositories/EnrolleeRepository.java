package xyz.thinkglobal.repositories;

import xyz.thinkglobal.models.Enrollee;

import java.util.List;
import java.util.Set;

public interface EnrolleeRepository extends ChildRepository<Enrollee> {
    List<Enrollee> findAllByFamilyRepresentativeSet_Id(Long familyRepresentativeSetId);

    Set<Enrollee> findAllByIdIn(Set<Long> ids);
}
