package xyz.thinkglobal.repositories;

import org.springframework.stereotype.Repository;
import xyz.thinkglobal.models.Lead;

import java.util.List;
import java.util.Optional;

@Repository
public interface LeadRepository extends PersonRepository<Lead> {

    List<Lead> findLeadsByStatus(String status);

    Optional<Lead> findByEmail(String email);
}
