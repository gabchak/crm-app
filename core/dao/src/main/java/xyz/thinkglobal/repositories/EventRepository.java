package xyz.thinkglobal.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import xyz.thinkglobal.models.Event;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

public interface EventRepository extends JpaRepository<Event, Long> {

    List<Event> findAllByStatusAndStartDateBetween(String status, LocalDateTime fromTime, LocalDateTime toTime);

    //TODO: Is it right query?
    @Query(value = "select e.end_date from event e " +
            "inner join lead_to_event lte on e.id = lte.event_id " +
            "where lte.lead_id = :leadId and lte.status = :leadStatus" +
            "order by e.end_date desc limit 1", nativeQuery = true)
    LocalDateTime findLastNotVisitedEventDate(@Param(value = "leadId") Long leadId, @Param("leadStatus") String leadStatus);

    Set<Event> findAllBySchool_Address_City_Id(Long id);

    List<Event> findAllByStartDateGreaterThan(LocalDateTime localDateTime);
}
