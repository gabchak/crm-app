package xyz.thinkglobal.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import xyz.thinkglobal.models.LeadEvent;

import java.util.List;

public interface LeadEventRepository extends JpaRepository<LeadEvent, LeadEvent.LeadEventId> {

    List<LeadEvent> findAllByEventId(Long eventId);
}
