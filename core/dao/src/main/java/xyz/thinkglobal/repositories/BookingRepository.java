package xyz.thinkglobal.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import xyz.thinkglobal.models.Booking;

import java.util.List;

public interface BookingRepository extends JpaRepository<Booking, Long> {

    List<Booking> findAllByFreePlacesGreaterThan(int freePlacesGraterThan);
}
