package xyz.thinkglobal.repositories;

import xyz.thinkglobal.models.User;

import java.util.Optional;

public interface UserRepository<T extends User> extends PersonRepository<T>{

    Optional<T> findByEmail(String email);
}
