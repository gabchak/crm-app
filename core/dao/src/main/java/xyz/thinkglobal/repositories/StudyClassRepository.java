package xyz.thinkglobal.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import xyz.thinkglobal.models.StudyClass;

import java.util.List;
import java.util.Set;

public interface StudyClassRepository extends JpaRepository<StudyClass, Long> {
    List<StudyClass> findAllBySchool_Id(Long id);

    List<StudyClass> findAllBySchool_IdAndWaitingQueueNotNull(Long id);

    Set<StudyClass> findAllBySchool_IdIn(Set<Long> schoolIdSet);
}