package xyz.thinkglobal.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import xyz.thinkglobal.models.Role;

import java.util.Set;

public interface RoleRepository extends JpaRepository<Role, Long> {
    Role findByRole(String role);
}
