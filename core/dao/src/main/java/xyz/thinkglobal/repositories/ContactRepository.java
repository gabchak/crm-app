package xyz.thinkglobal.repositories;

import xyz.thinkglobal.models.Contact;

import java.util.Set;

public interface ContactRepository extends UserRepository<Contact> {
    Set<Contact> findAllByIdIn(Set<Long> familyRepresentativeSet);
}
