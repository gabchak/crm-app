package xyz.thinkglobal.repositories;

import xyz.thinkglobal.models.Speaker;

public interface SpeakerRepository extends UserRepository<Speaker> {
}
