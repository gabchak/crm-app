package xyz.thinkglobal.repositories;

import xyz.thinkglobal.models.Student;

import java.util.List;
import java.util.Set;

public interface StudentRepository extends ChildRepository<Student> {

    List<Student> findAllByFamilyRepresentativeSet_Id(Long familyRepresentativeSetId);

    Set<Student> findAllByIdIn(Set<Long> students);
    Set<Student> findAllByStudyGroup_StudyClass_School_IdIn(Set<Long> schoolIdSet);
}
