package xyz.thinkglobal.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import xyz.thinkglobal.models.Position;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public interface PositionRepository extends JpaRepository<Position, Long> {

    Optional<Position> findBySchool_idAndPosition(Long schoolId, String position);

    Set<Position> findAllByPositionIn(Set<String> positions);

    Set<Position> findAllByUser_Email(String email);

    List<Position> findAllByUser_IdAndPositionIn(Long userId, Long positionId);

    @Query(value = "select * from position as p where p.position=" +
            "(select p.position from position as p where p.id = :id)",
    nativeQuery = true)
    List<Position> findAllRelatedByPositionId(@Param("id") Long positionId);
    List<Position> findAllByUser_IdAndIdIn(Long userId, Long positionId);
}
