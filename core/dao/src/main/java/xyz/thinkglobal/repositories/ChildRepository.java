package xyz.thinkglobal.repositories;

import xyz.thinkglobal.models.Child;

import java.util.Optional;

public interface ChildRepository<T extends Child> extends PersonRepository<T> {

    Optional<T> findByBirthCertificate(String birthCertificate);

//    List<T> findAllByFamilyRepresentativeSet(Long familyRepresentativeSetId);
}
