package xyz.thinkglobal.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import xyz.thinkglobal.models.WaitingQueueEnrollee;

import java.util.List;
import java.util.Set;

public interface WaitingQueueEnrolleeRepository extends JpaRepository<WaitingQueueEnrollee, Long> {

    Set<WaitingQueueEnrollee> findAllByIdIn(Set<Long> wqeId);

    List<WaitingQueueEnrollee> findAllByWaitingQueue_Id(Long id);
}
