package xyz.thinkglobal.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import xyz.thinkglobal.models.BookingDetails;

public interface BookingDetailsRepository extends JpaRepository<BookingDetails, Long> {

    void deleteByEnrollee_Id(Long id);
}
