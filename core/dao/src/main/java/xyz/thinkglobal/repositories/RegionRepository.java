package xyz.thinkglobal.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import xyz.thinkglobal.models.Region;

public interface RegionRepository extends JpaRepository<Region, Long> {
}
