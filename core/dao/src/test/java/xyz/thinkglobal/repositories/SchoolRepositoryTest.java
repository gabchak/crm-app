package xyz.thinkglobal.repositories;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import xyz.thinkglobal.models.School;

import javax.transaction.Transactional;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = xyz.thinkglobal.TGApplicationTest.class)
//@Sql({"classpath:test-initial-data.sql"})
public class SchoolRepositoryTest {

    @Autowired
    private SchoolRepository schoolRepository;

    @Test
    @Transactional
    public void findAllByAddress_City_IdTest() throws Exception {

        List<School> schools = schoolRepository.findAllByAddress_City_Id(280L);
        Long id = schools.get(0).getId();
        Long expectedId = 1L;
        Assert.assertEquals(expectedId, id);
    }
}
