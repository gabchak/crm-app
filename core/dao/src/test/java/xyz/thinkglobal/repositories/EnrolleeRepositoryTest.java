package xyz.thinkglobal.repositories;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import xyz.thinkglobal.models.Enrollee;

import javax.transaction.Transactional;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = xyz.thinkglobal.TGApplicationTest.class)
//@Sql(value = {"classpath:test-initial-data.sql"})
public class EnrolleeRepositoryTest {

    @Autowired
    private EnrolleeRepository enrolleeRepository;

    @Test
    @Transactional
    public void findAllByFamilyRepresentativeSetContains() {
        Long enrolleeId = 4L;
        Long contactId = 3L;

        List<Enrollee> optionalList = enrolleeRepository.findAllByFamilyRepresentativeSet_Id(contactId);

        Enrollee enrollee = optionalList.get(0);
        Assert.assertEquals(enrolleeId, enrollee.getId());
    }
}
