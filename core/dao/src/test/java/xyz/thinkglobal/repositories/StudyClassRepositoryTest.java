package xyz.thinkglobal.repositories;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import xyz.thinkglobal.models.StudyClass;

import javax.transaction.Transactional;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = xyz.thinkglobal.TGApplicationTest.class)
//@Sql({"classpath:test-initial-data.sql"})
public class StudyClassRepositoryTest {

    @Autowired
    private StudyClassRepository studyClassRepository;

    @Test
    @Transactional
    public void findAllBySchool_IdAndWaitingQueueNotNullTest() throws Exception {
        Long schoolId = 1L;
        List<StudyClass> studyClasses = studyClassRepository.findAllBySchool_IdAndWaitingQueueNotNull(schoolId);
        Long expectedClassId = 1L;

        Assert.assertEquals(expectedClassId, studyClasses.get(0).getId());

    }
}
