package xyz.thinkglobal.repositories;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import xyz.thinkglobal.models.Student;

import javax.transaction.Transactional;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = xyz.thinkglobal.TGApplicationTest.class)
//@Sql({"classpath:test-initial-data.sql"})
public class StudentRepositoryTest {

    @Autowired
    private StudentRepository studentRepository;

    @Test
    @Transactional
    public void findAllByFamilyRepresentativeSetContains() {
        Long contactId = 3L;
        Long studentId = 2L;


        List<Student> optionalStudents = studentRepository.findAllByFamilyRepresentativeSet_Id(contactId);

        Student student = optionalStudents.get(0);
        Long id = student.getId();

        Assert.assertEquals(studentId, id);
    }
}