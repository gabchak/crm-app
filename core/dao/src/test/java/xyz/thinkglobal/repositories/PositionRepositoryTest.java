package xyz.thinkglobal.repositories;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import xyz.thinkglobal.models.Position;

import javax.transaction.Transactional;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = xyz.thinkglobal.TGApplicationTest.class)
//@Sql({"classpath:test-initial-data.sql"})
public class PositionRepositoryTest {

    @Autowired
    private PositionRepository positionRepository;

    @Test
    @Transactional
    public void findBySchool_idAndPositionTest() throws Exception {
        Optional<Position> position = positionRepository.findBySchool_idAndPosition(1L,"DIRECTOR");
        position.ifPresent(p ->
                Assert.assertEquals("DIRECTOR", p.getPosition())
        );

    }
}
