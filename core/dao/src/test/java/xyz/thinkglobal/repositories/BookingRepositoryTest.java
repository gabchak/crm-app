package xyz.thinkglobal.repositories;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit4.SpringRunner;
import xyz.thinkglobal.models.Booking;

import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = xyz.thinkglobal.TGApplicationTest.class)
@Sql({"classpath:test-initial-data.sql"})
public class BookingRepositoryTest {

    @Autowired
    private BookingRepository bookingRepository;

    @Test
    public void findAllByFreePlacesGreaterThan() {
        Long bookingId = 1L;

        List<Booking> bookingList = bookingRepository.findAllByFreePlacesGreaterThan(0);
        Booking firstBooking = bookingList.get(0);

        assertEquals(bookingId, firstBooking.getId());
    }
}