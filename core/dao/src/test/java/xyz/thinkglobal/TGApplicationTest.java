package xyz.thinkglobal;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.jdbc.Sql;

@Configuration
@EnableAutoConfiguration
@ComponentScan
@PropertySource(value = "classpath:test-application.properties")
@EnableJpaRepositories("xyz.thinkglobal.repositories")
public class TGApplicationTest {

}

