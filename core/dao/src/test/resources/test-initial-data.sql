INSERT INTO role (id, role) VALUES (1, 'ROLE_CONTACT_MANAGER');
INSERT INTO role (id, role) VALUES (2, 'ROLE_SCHOOL_ADMINISTRATOR');
INSERT INTO role (id, role) VALUES (3, 'ROLE_FAMILY_PARTNER');
INSERT INTO role (id, role) VALUES (4, 'ROLE_FAMILY_REPRESENTATIVE');
INSERT INTO role (id, role) VALUES (5, 'ROLE_PARTNER_MANAGER');
INSERT INTO role (id, role) VALUES (6, 'ROLE_CONTRACT_MANAGER_BO');
INSERT INTO role (id, role) VALUES (7, 'ROLE_GUEST');
INSERT INTO role (id, role) VALUES (8, 'ROLE_SYSTEM_ADMINISTRATOR');


INSERT INTO region (id, name) VALUES (11, 'Київська обл.');

INSERT INTO city (id, name, fk_region_id) VALUES (280, 'Київ', 11);
INSERT INTO address (id, flat, house, street, fk_city_id) VALUES (1, '100', '17', 'Зелена', 280);

INSERT INTO person (id, first_name, last_name, middle_name) VALUES (1, 'y', 'o', 'v');

INSERT INTO school (id, code, name, status, term_of_partnership, address_id) VALUES (1, 'string', 'First test school', 'WORKING', 'test', 1);

INSERT INTO registered_user (password, id, email, additional_phone, phone)
VALUES ('test', 1, 'y.ostapiuk@thinkglobal.xyz', '0678887744', '0509996633');
INSERT INTO position (user_id, school_id, position, id, role)
VALUES (1, 1, 'FAMILY_PARTNER', 1, 3);

--create study class
INSERT INTO study_class (id, name, fk_school_id)
VALUES (1, 1, 1);

--create WQ and connect WQ with study class
INSERT INTO waiting_queue (id, registration_date, status, study_class_id)
VALUES (1, '2019-02-08', 'OPEN', 1);
-- UPDATE study_class SET waiting_queue_id = 1 WHERE id = 1;

--create study group
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
 values (1, '2020-06-01', 'a', 14, '2019-09-01', 'OPEN', 1);

--create contact
INSERT INTO person (id, first_name, last_name, middle_name)
 VALUES (3, 'y', 'o', 'v');
insert into registered_user (password, id, email, additional_phone, phone)
values ('test', 3, 'contact@thinkglobal.xyz', '0975558811', '0975558822');
INSERT INTO address (id, flat, house, street, fk_city_id) VALUES (2, '78', '23', 'Зелена', 280);
INSERT INTO address (id, flat, house, street, fk_city_id) VALUES (3, '87', '23', 'Зелена', 280);

insert into contact (date_of_issue_of_passport, inn, series_and_passport_number, who_issued_passport,
                     id, registration_address_id, residence_address_id)
 values ('2001-10-10', '4456465', 'se789456', 'who', 3, 2, 3);

--create child
INSERT INTO person (id, first_name, last_name, middle_name)
 VALUES (2, 'student name', 'student surname', 'student middle name');

insert into child (birth_certificate, date_of_birth, major_family_representative, id)
 values ('123456789', '2010-02-08', 1, 2);


INSERT INTO person (id, first_name, last_name, middle_name)
 VALUES (4, 'enrollee name', 'enrollee surname', 'enrollee middle name');

insert into child (birth_certificate, date_of_birth, major_family_representative, id)
 values ('465494654651', '2009-02-08', 1, 4);


--create students
insert into student (id, fk_study_group_id) values (2, 1);

--connection contact with student
insert into contact_students (family_representative_set_id, students_id)
 values (3, 2);

--create enrollee
insert into enrollee (id)
 values (4);

 --connection contact with enrollee
 insert into contact_enrollee (contact_id, enrollee_id)
 values (3, 4);

--create booking
insert into booking (capacity, name, open_date, status, free_places, study_group_id)
values (15, 'booking1', '2019-02-19', 'OPEN', 1, 1);
