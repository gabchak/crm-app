INSERT INTO role (id, role)
VALUES (1, 'ROLE_CONTACT_MANAGER');
INSERT INTO role (id, role)
VALUES (2, 'ROLE_SCHOOL_ADMINISTRATOR');
INSERT INTO role (id, role)
VALUES (3, 'ROLE_FAMILY_PARTNER');
INSERT INTO role (id, role)
VALUES (4, 'ROLE_FAMILY_REPRESENTATIVE');
INSERT INTO role (id, role)
VALUES (5, 'ROLE_PARTNER_MANAGER');
INSERT INTO role (id, role)
VALUES (6, 'ROLE_CONTRACT_MANAGER_BO');
INSERT INTO role (id, role)
VALUES (7, 'ROLE_GUEST');
INSERT INTO role (id, role)
VALUES (8, 'ROLE_SYSTEM_ADMINISTRATOR');

INSERT INTO region (id, name)
VALUES (11, 'Київська обл.');
INSERT INTO city (id, name, fk_region_id)
VALUES (280, 'Київ', 11);
INSERT INTO address (id, flat, house, street, fk_city_id)
VALUES (1, '100', '17', 'Зелена', 280);
INSERT INTO person (id, additional_phone, first_name, last_name, middle_name, phone)
VALUES (1, null, 'y', 'o', 'v', '1231231');
INSERT INTO person (id, additional_phone, first_name, last_name, middle_name, phone)
VALUES (2, null, 'y', 'o', 'v', '1231231');
INSERT INTO school (id, code, name, status, term_of_partnership, address_id)
VALUES (1, 'string', 'First test school', 'WORKING', 'test', 1);
INSERT INTO registered_user (password, id, email)
VALUES ('test', 1, 'y.ostapiuk@thinkglobal.xyz');
INSERT INTO position (user_id, school_id, position, id, role)
VALUES (1, 1, 'DIRECTOR', 1, 7);

INSERT INTO event (id, description, name, start_date, status, type, fk_school_id)
VALUES (1, 'des', 'name', '2019-03-09 18:50:43.009000', 'new', 'type', 1);
insert into lead (status, id, fk_city_id, how_you_find_us, email)
VALUES ('NEW', 2, 280, 'FACEBOOK', 'test@thinkglobal.xyz');


INSERT INTO registered_user (password, id, email)
VALUES ('test', 2, 't.test@thinkglobal.xyz');
INSERT INTO contact(date_of_issue_of_passport, inn, series_and_passport_number,
                    who_issued_passport, id, registration_address_id, residence_address_id)
values ('2001-10-10', '111111', 'MA', 'who', 2, 1, 1);