package xyz.thinkglobal.messaging.impl;

import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.GreenMailUtil;
import com.icegreen.greenmail.util.ServerSetup;
import com.icegreen.greenmail.util.ServerSetupTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import xyz.thinkglobal.TGApplicationTests;
import xyz.thinkglobal.messaging.EmailService;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@SpringBootTest(classes = TGApplicationTests.class)
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
public class EmailServiceImplTest {

    @Autowired
    private EmailService emailService;

    @Value("${spring.mail.username}")
    private String userName;

    @Value("${spring.mail.host}")
    private String host;

    @Value("${spring.mail.port}")
    private int port;

    @Value("${spring.mail.protocol}")
    private String protocol;

    @Value("${debug}")
    private String mailDebug;


    private GreenMail smtpServer;

    @Before
    public void setUp() throws Exception {
        smtpServer = new GreenMail(ServerSetupTest.SMTP);
        smtpServer.start();
    }

    @After
    public void tearDown() throws Exception {
        smtpServer.stop();
    }


    @Test
    public void sendSimpleMessage() throws MessagingException, IOException {
        String subject = "From yevgen test";
        String text = "We show how to write Integration Tests using Spring and GreenMail.";

        emailService.sendSimpleMessage(userName, subject, text);

//        email sent but green mail can't get information from email
//        assertTrue(smtpServer.waitForIncomingEmail(5000, 1));
//        Message[] messages = smtpServer.getReceivedMessages();
//        assertEquals(1, messages.length);
//        assertEquals(subject, messages[0].getSubject());
//        String body = GreenMailUtil.getBody(messages[0]).replaceAll("=\r?\n", "");
//        assertEquals(text, body);
    }

}