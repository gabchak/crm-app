package xyz.thinkglobal;

import org.junit.Test;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.UrlResource;
import org.springframework.test.context.TestPropertySource;

@Configuration
@EnableAutoConfiguration
@ComponentScan
@TestPropertySource(value = "classpath:application-test.properties")
@PropertySource(value = "classpath:application-test.properties", encoding = "UTF-8")
public class TGApplicationTests {

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyConfigIn() {
        PropertySourcesPlaceholderConfigurer retValue =  new PropertySourcesPlaceholderConfigurer();
        retValue.setLocation(new UrlResource(TGApplicationTests.class.getClassLoader().getResource("application-test.properties")));
        return retValue;
    }

    @Test
    public void contextLoads() {
    }
}

