package xyz.thinkglobal.service.impl;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import xyz.thinkglobal.dto.LeadDto;
import xyz.thinkglobal.models.LeadEvent;
import xyz.thinkglobal.repositories.LeadEventRepository;
import xyz.thinkglobal.services.impl.LeadEventServiceImpl;

import javax.transaction.Transactional;

@SpringBootTest(classes = xyz.thinkglobal.TGApplicationTests.class)
@RunWith(SpringRunner.class)
@Sql({"classpath:test-initial-data.sql"})
public class LeadEventServiceImplTest {

    @Autowired
    private LeadEventServiceImpl leadEventService;

    @Autowired
    private LeadEventRepository leadEventRepository;

    @Test
    @Transactional
    public void registerLeadForEventTest() throws Exception {
        String leadDtoEmail = "thinkglobal.test@gmail.com";
        String leadDtoFirstName = "Register";
        String leadDtoLastName = "Lead";
        String leadDtoPhone = "0738884433";
        String leadHowYouFindUs = "FACEBOOK";

        Long cityId = 280L;
        Long eventId = 1L;

        LeadDto leadDto = new LeadDto();
        leadDto.setFirstName(leadDtoFirstName);
        leadDto.setLastName(leadDtoLastName);
        leadDto.setPhone(leadDtoPhone);
        leadDto.setEmail(leadDtoEmail);
        leadDto.setCity(cityId);
        leadDto.setHowYouFindUs(leadHowYouFindUs);
        leadDto.setStatus("NEW");

        LeadDto savedLeadDto = leadEventService.registerLead(leadDto, eventId);

        Assert.assertEquals(leadDtoEmail, savedLeadDto.getEmail());
        Assert.assertEquals(leadDtoFirstName, savedLeadDto.getFirstName());
        Assert.assertEquals(leadDtoLastName, savedLeadDto.getLastName());
        Assert.assertEquals(leadDtoPhone, savedLeadDto.getPhone());
        Assert.assertEquals("IN_PROGRESS", savedLeadDto.getStatus());
        Assert.assertEquals(leadHowYouFindUs, savedLeadDto.getHowYouFindUs());

        LeadEvent.LeadEventId leadEventId = new LeadEvent.LeadEventId(savedLeadDto.getId(), eventId);
        LeadEvent leadEvent = leadEventRepository.getOne(leadEventId);
        Assert.assertEquals(eventId, leadEvent.getEventId());
        Assert.assertEquals(savedLeadDto.getId(), leadEvent.getLeadId());
    }
}
