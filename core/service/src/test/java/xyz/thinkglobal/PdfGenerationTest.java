package xyz.thinkglobal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import xyz.thinkglobal.dto.pdf.ContractDto;
import xyz.thinkglobal.services.pdf.PdfGeneratorService;
import xyz.thinkglobal.util.TemplateUtil;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

@SpringBootTest(classes = xyz.thinkglobal.TGApplicationTests.class)
@RunWith(SpringRunner.class)
public class PdfGenerationTest {

    @Autowired
    private TemplateUtil templateUtil;

    @Autowired
    private PdfGeneratorService pdfGeneratorService;


    @Test
    public void pdfCreationFromPdfTemplateFile() throws Exception {
        Map<String, Object> model = new HashMap<>();

        ContractDto contractDto = new ContractDto();

        contractDto.setVarContent(true);
        contractDto.setFixed(true);
        contractDto.setContractTitle("contractTitle");
        contractDto.setContractSigningDate("contractSigningDate");
        contractDto.setLookupCounterpartyName("lookupCounterpartyName");
        contractDto.setLookupCounterpartyEdrpouCode("lookupCounterpartyEdrpouCode");
        contractDto.setLookupCounterpartyAddress("lookupCounterpartyAddress");
        contractDto.setLookupCounterpartyActOnBasis("lookupCounterpartyActOnBasis");
        contractDto.setStudentLastName("studentLastName");
        contractDto.setStudentFirstName("studentFirstName");
        contractDto.setStudentMiddleName("studentMiddleName");
        contractDto.setContactLastName("contactLastName");
        contractDto.setContactFirstName("contactFirstName");
        contractDto.setContactMiddleName("contactMiddleName");
        contractDto.setContractSchool("contractSchool");
        contractDto.setContractServiceStartDate("contractServiceStartDate");
        contractDto.setContractServiceEndDate("contractServiceEndDate");
        contractDto.setContractFullNumberPlusInWords("contractFullNumberPlusInWords");
        contractDto.setDateOfTheEndOfContract("dateOfTheEndOfContract");
        contractDto.setContactRegistrationCity("contactRegistrationCity");
        contractDto.setContactRegistrationStreet("contactRegistrationStreet");
        contractDto.setContactRegistrationHouse("contactRegistrationHouse");
        contractDto.setContactRegistrationFlat("contactRegistrationFlat");
        contractDto.setContactPhone("contactPhone");
        contractDto.setLookupCounterpartyNameShort("lookupCounterpartyNameShort");
        contractDto.setLookupCountrePartyPhone("lookupCountrePartyPhone");
        contractDto.setLookupCounterpartyRequisites("lookupCounterpartyRequisites");
        contractDto.setLookupCounterpartyDirector("lookupCounterpartyDirector");
        contractDto.setLookupCounterpartyDirectorShort("lookupCounterpartyDirectorShort");

        model.put("contractDto", contractDto);

        File file =  pdfGeneratorService.getPdfFileFromTemplate(model, "additional_service_contract.ftl");

    }
}
