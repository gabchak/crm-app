<?xml version="1.0" encoding="UTF-8"?>
<html>
    <head>
        <meta charset='UTF-8'/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <style>
            body {
                font-family: Arial Unicode MS, Lucida Sans Unicode, Arial, verdana, arial, helvetica, sans-serif;
                font-size: 8.8pt;
            }
            table, th, td {
                border: 1px solid black;
                border-collapse: collapse;
            }
            th, td {
                padding: 5px;
                text-align: left;
            }
        </style>
    </head>
    <body>
        <h3>Договір № ${contractTheme}</h3>
        <h5>про надання допоміжних послуг у сфері освіти</h5>
        <h5>м. Київ</h5>
        <h5>${contractSigningDate}</h5>
        <p>${countrAgentName}
            (Код
            за
            ЄДРПОУ
        ${countrAgentCode} ,
            місцезнаходження:
        ${countrAgentAddress}) в особі директора ${countrAgentDirector} , який діє на підставі ${countrAgentActBasedOn}, надалі
            іменується «Виконавець» та
            Учень (одержувач) ${enrolleeLastName} ${enrolleeFirstName} ${enrolleeMiddleName} в особі законного представника
            (батька/матері/особи, що їх замінює), ${contactLastName} ${contactFirstName} ${contactMiddleName} , який діє на
            підставі ч. 1 ст. 242
            Цивільного кодексу України, надалі іменується «Замовник», надалі разом іменовані «Сторони» , а кожна окрема –
            «Сторона», уклали цей
            договір про надання допоміжних послуг у сфері освіти, надалі «Договір», про таке:</p>


        <table style="width:100%">
            <caption>Monthly savings</caption>
            <tr>
                <th>Тест</th>
                <th>Тест</th>
            </tr>
            <tr>
                <td>Тест</td>
                <td>Тест</td>
            </tr>
            <tr>
                <td>february</td>
                <td>$50</td>
            </tr>
        </table>
    </body>
</html>