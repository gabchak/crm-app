<#-- @ftlvariable name="contractDto" type="xyz.thinkglobal.dto.pdf.ContractDto" -->

<?xml version="1.0" encoding="UTF-8"?>
<html>
<head>
    <meta charset='UTF-8'/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <style>
        body {
            font-family: "Times New Roman";
            font-size: 11pt;
        }

        div.cell {
            padding: 3ex;
        }

        .placeholder1 {
        }

        .fixed-head {
            min-height: 4em;
        }

        .placeholder2 {
            min-height: 8em;
            margin-right: 10em;
        }

        .placeholder3 {
            min-height: 18em;
        }

        p.paragraph, p.sub-paragraph {
            margin-top: 0.5ex;
            margin-bottom: 0.5ex;

        }

        h3.main-title {
            text-align: center;
            font-weight: bold;
            margin-top: 0.5ex;
            margin-bottom: 0.5ex;
        }

        div.sub-title {
            position: relative;

        }

        h5.main-title-left {
            font-weight: bold;
        }

        h5.main-title-right {
            position: absolute;
            right: 0;
            top: 0;
            margin: 0;
            font-weight: bold;
        }

        h1.doc-header {
            text-align: center;
            font-size: 120%;
            margin-top: 0.5ex;
            margin-bottom: 0.5ex;

        }

        div.variable-content {
        <#if contractDto.varContent>
            visibility: visible;
        <#else>
            visibility: hidden;
        </#if>
        }

        .fixed-content {
        <#if contractDto.fixed>
            visibility: visible;
        <#else>
            visibility: hidden;
        </#if>
        }

        table {
            width: 100%
        }

        th, td {
            vertical-align: top;
            width: 50%;
        }

        th {
            text-align: center;
        }

        table, th, td {
            border: 1px solid white;
            border-collapse: collapse;
        <#if contractDto.fixed>
            border-color: black;
        </#if>
        }

        td {
            padding: 5px;
            text-align: left;
        }
    </style>
</head>

<body>

<div class="placeholder1 variable-content">
    <h3 class="main-title">Договір № ${contractDto.contractTitle}</h3>
    <h3 class="main-title">про надання освітніх послуг</h3>
    <div class="sub-title">
        <h5 class="main-title-left">м. Київ</h5>
        <h5 class="main-title-right">${contractDto.contractSigningDate}</h5>
    </div>
</div>

<div class="fixed-content fixed-head">
    <p>
        <span class="bold">${contractDto.lookupCounterpartyName}</span> (Код за ЄДРПОУ <span
            class="bold">${contractDto.lookupCounterpartyEdrpouCode}</span>,
        місцезнаходження: ${contractDto.lookupCounterpartyAddress}) в особі
        директора ${contractDto.lookupCounterpartyDirector}, який діє на
        підставі ${contractDto.lookupCounterpartyActOnBasis}, надалі іменується <span class="bold">«Виконавець»</span>
        та
    </p>
</div>


<div class="placeholder2 variable-content">
    <p>
        <span class="bold">Учень (одержувач) ${contractDto.studentLastName} ${contractDto.studentFirstName} ${contractDto.studentMiddleName}</span>
        в особі законного представника
        (батька/матері/особи, що їх замінює), <span
            class="bold">${contractDto.contactLastName} ${contractDto.contactFirstName} ${contractDto.contactMiddleName}</span>,
        який діє на підставі
        ч. 1 ст. 242 Цивільного кодексу України, надалі іменується <span class="bold">«Замовник»</span>, надалі разом
        іменовані <span class="bold">«Сторони»</span>, а кожна окрема
        – <span class="bold">«Сторона»</span>, уклали цей договір про надання допоміжних послуг у сфері освіти, надалі
        <span class="bold">«Договір»</span>, про таке:
    </p>
</div>


<div class="fixed-content">
    <h1 class="doc-header">ВИЗНАЧЕННЯ ТЕРМІНІВ</h1>
    <p class="paragraph">
        <span class="bold">Батьки</span> - законні представники дитини, відповідно до чинного законодавства України.
    </p>
    <h1 class="doc-header">1. ПРЕДМЕТ ДОГОВОРУ</h1>
    <p class="paragraph">
        1.1. Предметом Договору є надання освітньої послуги.
    </p>
    <p class="paragraph">
        1.2. Виконавець бере на себе зобов’язання за рахунок коштів Замовника здійснити надання Одержувачу освітньої
        послуги, а саме: надати можливість навчання учня за дистанційною або екстернатною формою навчання та отримати
        атестацію відповідного рівня освіти та документ про освіту державного зразка на основі отриманої ним освіти:
        свідоцтво про завершення базового рівня повної загальної середньої освіти або профільного рівня повної загальної
        середньої освіти.
    </p>
    <p class="paragraph">
        1.3. Строк надання послуги вимірюється навчальним періодом:
    </p>
    <p class="sub-paragraph">Початок навчання: ${contractDto.contractServiceStartDate}.</p>
    <p class="sub-paragraph">Закінчення навчання: ${contractDto.contractServiceEndDate}.</p>
    <h1 class="doc-header">2. ПРАВА ТА ОБОВ’ЯЗКИ ВИКОНАВЦЯ</h1>
    <p class="paragraph">
        <span class="bold">2.1. Виконавець зобов’язаний:</span>
    </p>
    <p class="sub-paragraph">1) надати Одержувачу освітню послугу в обсязі, передбаченому чинними стандартами та вимогами
        Міністерства освіти і науки України;</p>
    <p class="sub-paragraph">2) надати Одержувачу освітньої послуги доступу до системи проведення занять онлайн та контролю
        знань онлайн;</p>
    <p class="sub-paragraph">3) надавати Одержувачу освітньої послуги та Замовнику повну інформацію про етапи, зміст, мету
        та якість навчання, умови отримання освітньої послуги, про права та обов’язки сторін;</p>
    <p class="sub-paragraph">4) підготувати та надати Одержувачу освітньої послуги навчальні матеріали в необхідному для
        навчання обсязі в електронній формі;</p>
    <p class="sub-paragraph">5) вести особову справу Одержувача освітньої послуги;</p>
    <p class="sub-paragraph">6) надати Замовнику інформацію про успішність Одержувача освітньої послуги;</p>
    <p class="sub-paragraph">7) ознайомити Одержувача освітньої послуги та Замовника з внутрішніми правилами, інструкціями
        та заходами навчального закладу в процесі навчання;</p>
    <p class="sub-paragraph">8) проводити державні підсумкові атестації для класів, в яких вона передбачена;</p>
    <p class="sub-paragraph">9) видати Одержувачу освітньої послуги документ про освіту державного зразка (якщо відповідно
        до законодавства за освітньою програмою передбачено видачу такого документа) за умови виконання Одержувачем
        навчального навантаження в обсязі, необхідному для здобуття певного ступеня освіти.</p>
    <p class="paragraph">
        <span class="bold">2.2. Виконавець має право:</span>
    </p>
    <p class="sub-paragraph">1) вимагати від Замовника своєчасно вносити плату за освітню послугу в розмірах та в порядку,
        встановлених цим Договором.</p>
    <h1 class="doc-header">3. ПРАВА ТА ОБОВ’ЯЗКИ ОДЕРЖУВАЧА ОСВІТНЬОЇ ПОСЛУГИ ТА ЗАМОВНИКА</h1>
    <p class="paragraph">
        3.1. Одержувач освітньої послуги в особі Замовника бере на себе такі зобов’язання:
    </p>
    <p class="sub-paragraph">1) у повному обсязі та своєчасно сплачувати Виконавцю плату за надання освітньої послуги в
        порядку та розмірі, встановлених Договором;</p>
    <p class="sub-paragraph">2) дотримуватись вимог законодавства України, моральних, етичних норм, правил внутрішнього
        розпорядку, виконувати вимоги Статуту «Виконавця», накази (розпорядження) директора, інших посадових осіб та вимог
        вчителів стосовно навчального процесу та дисципліни;</p>
    <p class="sub-paragraph">3) дотримуватись конфіденційності інформації, яка є комерційною таємницею, не розповсюджувати і
        не передавати стороннім особам чи організаціям без дозволу керівництва навчального закладу методичні розробки,
        документи, літературу та інші інтелектуальні цінності, що належать Виконавцю;</p>
    <p class="sub-paragraph">4) виконувати в повному обсязі та належним чином усі завдання, які даються вчителями та іншим
        персоналом на самостійне опрацювання.</p>
    <p class="paragraph">
        <span class="bold">3.2. Замовник має право вимагати від Виконавця:</span>
    </p>
    <p class="sub-paragraph">1) надання освітньої послуги Одержувачу освітньої послуги на рівні стандартів освіти за
        дистанційною формою або екстернатною формою навчання;</p>
    <p class="sub-paragraph">2) видачі Одержувачу освітньої послуги документа про освіту державного зразка за умови
        виконання Одержувачем освітньої послуги навчального навантаження в обсязі, необхідному для здобуття певного ступеня
        освіти;</p>
    <p class="sub-paragraph">3) інформування Одержувача освітньої послуги про правила та вимоги щодо організації надання
        освітньої послуги, її якості та змісту, про його права і обов’язки під час надання та отримання зазначеної
        послуги.</p>
    <h1 class="doc-header">4. ВАРТІСТЬ ОСВІТНЬОЇ ПОСЛУГИ ТА ПОРЯДОК РОЗРАХУНКІВ</h1>
    <p class="paragraph">
        4.1. Розмір плати за надання освітньої послуги у повному обсязі встановлюється в національній валюті України –
        гривні.
    </p>
    <p class="paragraph">
        4.2. Форма розрахунків: безготівкова шляхом перерахування коштів на рахунок Виконавця.
    </p>
    <p class="paragraph">
        4.3. Загальна вартість освітньої послуги за весь строк навчання становить
    </p>
    <span class="bold">${contractDto.contractFullNumberPlusInWords}</span>
    <p class="paragraph">
        4.4. Одержувач освітньої послуги зараховується до навчального закладу в п'ятиденний термін після повної сплати
        Замовником вартості послуги в розмірі, встановленому в п. 4.3. цього Договору.
    </p>
    <p class="paragraph">
        4.5. Замовник та Виконавець як Сторони даного договору імперативно без будь-яких виключень підписанням даного
        договору встановили, що забороняється повернення здійснених Замовником Виконавцю оплат за період, за який послуги
        були вже надані Виконавцем Замовнику.
    </p>
    <p class="paragraph">
        4.6. Замовник та Виконавець як Сторони даного договору імперативно без будь-яких виключень підписанням даного
        договору встановили, що в разі непідписання Сторонами за їх згодою угоди про розірвання даного договору протягом
        періоду дії даного договору, за який надається послуга, здійснена оплата не повертається Виконавцем Замовнику. В
        такому випадку надана за цим договором Виконавцем Замовнику послуга, за яку здійснена оплата за період дії даного
        договору, вважається наданою у відповідності до умов даного договору та всіх вимог чинного законодавства України.
    </p>
    <p class="paragraph">
        4.7. У випадку розірвання угоди сторони проводять процедуру звірки наданих послуг та сплачених коштів і в разі
        наявності заборгованості сторона-боржник зобов’язується виконати свої фінансові зобов’язання.
    </p>
    <p class="paragraph">
        4.8. У разі дострокового розірвання Договору Виконавець повертає гроші Замовнику за невикористаний період з
        розрахунку періоду надання послуг, вказаного в п. 1.3.
    </p>
    <h1 class="doc-header">5. ВІДПОВІДАЛЬНІСТЬ СТОРІН ЗА НЕВИКОНАННЯ АБО НЕНАЛЕЖНЕ ВИКОНАННЯ ЗОБОВ’ЯЗАНЬ</h1>
    <p class="paragraph">
        5.1. За невиконання або неналежне виконання зобов’язань цього договору Сторони несуть відповідальність, передбачену
        чинним законодавством України та цим Договором.
    </p>
    <h1 class="doc-header">6. ВНЕСЕННЯ ЗМІН ТА РОЗІРВАННЯ ДОГОВОРУ</h1>
    <p class="paragraph">
        6.1. Зміни до цього Договору можуть бути внесені за взаємною згодою Сторін шляхом укладення та підписання Сторонами
        додаткових угод до нього.
    </p>
    <p class="paragraph">
        6.2. Договір може бути розірвано:
    </p>
    <p class="sub-paragraph">1) за згодою Сторін;</p>
    <p class="sub-paragraph">2) у випадку несплати Замовником послуг протягом 15 календарних днів після підписання
        договору;</p>
    <p class="sub-paragraph">3) за ініціативи Замовника шляхом повідомлення Виконавця рекомендованим листом за 30
        календарних днів та за умови проведення звірки фінансових зобов’язань сторін шляхом складання акту звірки фінансових
        зобов’язань та укладання додаткової угоди про розрив Договору, на підставі якої Замовнику буде повернуто кошти за
        невикористаний період.</p>
    <h1 class="doc-header">7. СТРОК ДІЇ ДОГОВОРУ ТА ПОРЯДОК ВИРІШЕННЯ СПОРІВ</h1>
    <p class="paragraph">
        7.1. Даний Договір вступає в силу з дня його підписання Сторонами та діє до <span
            class="bold">${contractDto.dateOfTheEndOfContract}</span>, а
        в частині зобов’язань Сторін та відповідальності сторін, до повного та належного їх виконання Сторонами.
    </p>
    <p class="paragraph">
        7.2. Усі додаткові договори до даного Договору є його невід’ємними частинами.
    </p>
    <p class="paragraph">
        7.3. Замовник підписанням цього договору стверджує на підставі ч. 2 ст. 65, ч.ч. 1, 2 ст. 74 Сімейного кодексу
        України, що діє за згодою другого з подружжя, а в разі перебування в незареєстрованому шлюбі за згодою іншої особи,
        з якою проживає однією сім'єю.
    </p>
    <p class="paragraph">
        7.4. Даний договір складений при повному розумінні Сторонами його умов та термінології українською мовою в 2 (двох)
        оригінальних примірниках, що мають рівну юридичну силу, і обов’язковий для виконання обома сторонами.
    </p>
    <p class="paragraph">
        7.5. Усі зміни, доповнення і додатки до даного Договору є його невід’ємною частиною і мають силу тільки в разі
        підписання їх Сторонами.
    </p>
    <p class="paragraph">
        7.6. Сторони засвідчують та гарантують, що надані один одному для виконання зобов'язань за даним Договором
        персональні дані були отримані та знаходяться у користуванні відповідно до чинного законодавства України, не
        порушуючи прав третіх осіб.
    </p>
    <p class="paragraph">
        7.7. Замовник не має права передавати права та обов’язки по даному Договору третім особам без письмової згоди
        Виконавця.
    </p>
    <p class="paragraph">
        7.8. Замовник надає Виконавцю згоду на обробку та передачу третім особам персональних даних, отриманих Виконавцем
        під час надання послуг.
    </p>
    <h1 class="doc-header">8. РЕКВІЗИТИ СТОРІН:</h1>
    </div>
</div>

<table>
    <thead>
        <tr>
            <th class="fixed-content">Замовник (учень)</th>
            <th class="fixed-content">Виконавець</th>
        </tr>
    </thead>
    <tbody>
    <tr>
        <td>
            <div class="placeholder3 cell variable-content">
                Громадянин(ка) України
                <br/>
            ${contractDto.studentLastName} ${contractDto.studentFirstName} ${contractDto.studentMiddleName}
                <br/>
                в особі законного представника, батька/матері:
                <br/>
            ${contractDto.contactLastName} ${contractDto.contactFirstName} ${contractDto.contactMiddleName}
                <br/>
            ${contractDto.fullPasportData}
                <br/>
                Місце реєстрації:
                <br/>
            ${contractDto.contactRegistrationCity}, ${contractDto.contactRegistrationStreet}, ${contractDto.contactRegistrationHouse},  ${contractDto.contactRegistrationFlat}
                <br/>
                моб.тел. ${contractDto.contactPhone}
                <br/>
                дом.тел.___________________
                <br/>
            ${contractDto.contactLastName} ${contractDto.contactFirstName} ${contractDto.contactMiddleName}
                /_______________/
            </div>
        </td>

        <td>
            <div class="cell fixed-content">
            ${contractDto.lookupCounterpartyNameShort}
                <br/>
                Код ЄДРПОУ ${contractDto.lookupCounterpartyEdrpouCode},
                <br/>
                Юридична адреса: ${contractDto.lookupCounterpartyAddress}
                <br/>
                телефон ${contractDto.lookupCountrePartyPhone}
                <br/>
            ${contractDto.lookupCounterpartyRequisites}
                <br/>
                Директор
                <br/>
            ${contractDto.lookupCounterpartyDirectorShort}/_______________/
            </div>
        </td>
    </tr>
    </tbody>
</table>
</body>
</html>
