<#-- @ftlvariable name="hotline" type="java.lang.String" -->
<#-- @ftlvariable name="facebookLink" type="java.lang.String" -->
<#-- @ftlvariable name="leadEvent" type="xyz.thinkglobal.models.LeadEvent" -->

<#--Template name: лист-підтвердження на івент-->

<html>
    <body>
    <p>Доброго дня, ${leadEvent.lead.firstName}!</p>

    <p>Дякую Вам за цікавість до нашої школи!</p>

    <p>Цей лист є підтвердженням реєстрації на ${leadEvent.event.name},
        яка відбудеться ${leadEvent.event.getFormattedStartDate()} о ${leadEvent.event.getFormattedStartTime()}
        за адресою:  ${leadEvent.event.school.address.getFormattedAddress()}</p>

    <p>Слідкуйте за анонсами подій на нашій сторінці <a href="${facebookLink}">Facebook</a></p>
    <p>З питаннями звертайтесь, будь ласка, за номером
        ${hotline}
    </p>
    <p>Вдалого Вам дня!</p>

    </body>
</html>