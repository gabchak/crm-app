<#-- @ftlvariable name="leadEvent" type="xyz.thinkglobal.models.LeadEvent" -->
<#-- @ftlvariable name="familyPartner" type="xyz.thinkglobal.models.User" -->

<#--Template name: Надсилає лист "у продовження лекції" з лінком щоб потрапити у waiting list-->

<html>
    <body>
        <p>Доброго дня, ${leadEvent.lead.firstName}!</p>
        <p>Вдячні вам за участь у зустрічі і чудову атмосферу.</p>

        <h3>Матеріали з лекції, про які йшлося на зустрічі є нижче:</h3>

        <ol>
            <li><a href="https://docs.google.com/document/d/14fz2EMXm1aUHLTW8363m4Hnplv-5qAPZj1Vh11Qmsng/edit">Концепція ThinkGlobal</a>.</li>
            <li>Приклади програм (1-8 класи):</li>
            <ul>
                <li><a href="https://drive.google.com/file/d/1PrLwtKYkPtnJ92RPi8smuB7GKsG0qGM9/view">Бізнес, кар'єра та управління</a>;</li>
                <li><a href="https://drive.google.com/file/d/1kIf9kCscOUzgY-2fDjpCOw0xatQaSo21/view">Комп'ютерні науки</a>;</li>
                <li><a href="https://drive.google.com/file/d/11WfyLNNRM27ARf54Wa-UQM3my0YLu_Af/view">Математика</a>;</li>
                <li><a href="https://drive.google.com/file/d/1sNhmxlHds5ibXmo5iDs2BWiPtcrFt7z4/view">Мистецтва</a>;</li>
                <li><a href="https://drive.google.com/file/d/1qTXDiV-cy-aQ9BTIQE2YW72Zc9bPdBsy/view">Мови світу</a>.</li>
                <li><a href="https://drive.google.com/file/d/1xv1oZy0uFA91jJqcE6W3Z0sY5-p5Uv-c/view">Підготовка до школи</a>;</li>
                <li><a href="https://drive.google.com/file/d/1IHq1NcjLo3VvBqpbrrRrSUFA3BDJVV8S/view">Спілкування та мислення</a>;</li>
                <li><a href="https://drive.google.com/file/d/14s91NKQOg0az_TjhaIAJQTMe7Otzm2vj/view">Спорт і здоров'я</a>;</li>
                <li><a href="https://drive.google.com/file/d/1QJxlAF2ljrIKvQGKHExKDJrZa9m7snU0/view">Соціальні науки</a>;</li>
            </ul>
            <li><a href="https://docs.google.com/document/d/1auBvIWYQsPC-W8NI2F1emOrxYiZHNs-PAHPyBnReK5M/edit">Навчальний план</a> (загальний).</li>
            <li><a href="https://creator.zohopublic.com/thinkglobal/schedule/view-perma/HR_schedule/YXaOC8ArCZZOsdy9Nk7DR1CZDs9HvjjrAErnwSkQm2gDBAb6tTrYdWpPkz3Gman5f6FknGykjE7PhYfZmrTTCYA2vNX9vtmO9ydJ">Приклад розкладу</a>.</li>
            <li><a href="https://docs.google.com/document/d/1A-M7sjkd1hZ9CkXrMLxCZ2mRvT6kg3jTeffbQMzlY_I/edit#heading=h.4y5hh8jjmic2">Система оцінювання</a>.</li>
            <li><a href="https://docs.google.com/presentation/d/1uwWOygo6II57zIfZexlNcdjJkbyUx12U7Q_Sd4gCgfM/edit#slide=id.g2ed8e3ec22_0_212">Презентація з лекції</a>.</li>
        </ol>

        <p>Щоб оформити документи і вступити до школи, перейдіть за <a href="${contactRegistrationLink}">посиланням</a>.</p>
        <p>З питаннями звертайтесь за
            <#if familyPartner.additionalPhone??>
                номерами:
                <br>
                ${familyPartner.phone},
                <br>
                ${familyPartner.additionalPhone}.


            <#else>
                номером ${familyPartner.phone}.
            </#if>

        </p>

    </body>
</html>