<#-- @ftlvariable name="event" type="xyz.thinkglobal.models.Event" -->
<html>
    <body>
        <h3>Доброго дня!</h3>
        <p>
            <a href="${eventLink}">Ось посилання</a>
            на реєстрацію на ${event.name},
            <br>
            яка відбудеться ${event.getFormattedStartDate()} о ${event.getFormattedStartTime()}
            <br>
            за адресою ${event.school.address.getFormattedAddress()}.
        </p>
    </body>
</html>
