<#-- @ftlvariable name="leadEvent" type="xyz.thinkglobal.models.LeadEvent" -->

<#--Template name: Запрошуємо в школу ThinkGlobal" (меседж -  запрошуємо на наступний івент) з можливістю реєстрації-->

<html>
    <body>
        <p>Доброго дня, ${leadEvent.lead.firstName}!
        <p>Шкода, що вам не вдалось, відвідати подію ${leadEvent.event.name},
            яка відбулась ${leadEvent.event.getFormattedStartDate()} о ${leadEvent.event.getFormattedStartTime()},
            за адресою: ${leadEvent.event.school.address.getFormattedAddress()}.
            Пропонуємо записатись на найближчі події у вашому місті, для ближчого знайомства зі школою.</p>
        <p>
            <#list events as event>
                <ul>
                    <li><a href="">${event.name}</a>  ${event.getFormattedStartDate()} о ${event.getFormattedStartTime()}
                        за адресою: ${event.school.address}</li>
                </ul>
            </#list>
        </p>
        <p>З питаннями звертайтесь за номером
            ${hotline}
        </p>
    </body>
</html>