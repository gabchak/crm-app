<#-- @ftlvariable name="event" type="xyz.thinkglobal.models.Event" -->
<#--Template name: Лист контакту, який ще раз відвідав івент-->

<html>
    <body>
        <p>На заплановану подію ${event.name}, яка відбулась ${event.getFormattedStartDate()} о ${event.getFormattedStartTime()},
            на локації  ${event.school.address.getFormattedAddress()}, прийшло ${leadQuanity}  учасників.</p>
        <p>Зв’яжіться із організатором події та з’ясуйте причини. </p>
    </body>
</html>