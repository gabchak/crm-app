<#-- @ftlvariable name="leadEvent" type="xyz.thinkglobal.models.LeadEvent" -->

<#--Template name: Лист контакту, який ще раз відвідав івент-->

<html>
    <body>
        <p>Доброго дня, ${leadEvent.lead.firstName}!</p>
        <p>Вдячні вам за участь у зустрічі і чудову атмосферу.</p>
        <p>Щоб оформити документи і записатись в чергу до іншої школи,
            увійдіть в свій
            <a href="${accountLink}">кабінет</a>.</p>
        <p>
            З питаннями звертайтесь за
            <#if familyPartner.additionalPhone??>
                номерами:
                <br>
                ${familyPartner.phone},
                <br>
                ${familyPartner.additionalPhone}.
            <#else>
                номером ${familyPartner.phone}.
            </#if>
        </p>
    </body>
</html>


