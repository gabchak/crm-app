package xyz.thinkglobal.dto;

import lombok.Data;

@Data
public class WaitingQueueEnrolleeDto {

    private Long id;
    private Long waitingQueue;
    private Long enrollee;
    private String status;
}
