package xyz.thinkglobal.dto.mappers;

import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import xyz.thinkglobal.dto.PositionDto;
import xyz.thinkglobal.dto.enums.Positions;
import xyz.thinkglobal.dto.enums.UserRole;
import xyz.thinkglobal.models.Position;
import xyz.thinkglobal.models.Role;
import xyz.thinkglobal.models.School;
import xyz.thinkglobal.models.User;
import xyz.thinkglobal.repositories.RoleRepository;
import xyz.thinkglobal.repositories.SchoolRepository;
import xyz.thinkglobal.repositories.UserRepository;

import java.util.Set;

@Component
public class PositionMapper extends ConfigurableMapper {

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private SchoolRepository schoolRepository;

    @Override
    protected void configure(MapperFactory factory) {
        factory.classMap(PositionDto.class, Position.class)
                .exclude("school")
                .exclude("user")
                .exclude("role")
                .customize(new CustomMapper<PositionDto, Position>() {
                    @Override
                    public void mapAtoB(PositionDto positionDto, Position position, MappingContext context) {
                        super.mapAtoB(positionDto, position, context);
                        position.setRole(roleRepository.findByRole(Positions.getRoleByPositionName(positionDto.getPosition())));
                        School school = schoolRepository.getOne(positionDto.getSchool());
                        position.setSchool(school);
                    }

                    @Override
                    public void mapBtoA(Position position, PositionDto positionDto, MappingContext context) {
                        super.mapBtoA(position, positionDto, context);
                        if (position.getSchool() != null) {
                            positionDto.setSchool(position.getSchool().getId());
                        }
                        positionDto.setUser(position.getUser().getId());
                    }
                })
                .byDefault()
                .register();
    }
}
