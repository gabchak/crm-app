package xyz.thinkglobal.dto.mappers;

import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import xyz.thinkglobal.dto.LeadEventDto;
import xyz.thinkglobal.models.Event;
import xyz.thinkglobal.models.Lead;
import xyz.thinkglobal.models.LeadEvent;
import xyz.thinkglobal.repositories.EventRepository;
import xyz.thinkglobal.repositories.LeadRepository;

@Component
public class LeadEventMapper extends ConfigurableMapper {

    @Autowired
    private LeadRepository leadRepository;

    @Autowired
    private EventRepository eventRepository;

    /**
     * Mapper configuration method
     *
     * @param factory mapper factory
     **/
    @Override
    protected void configure(MapperFactory factory) {
        factory.classMap(LeadEventDto.class, LeadEvent.class)
                .exclude("lead")
                .exclude("event")
                .exclude("leadId")
                .exclude("eventId")
                .customize(new CustomMapper<LeadEventDto, LeadEvent>() {
                    @Override
                    public void mapAtoB(LeadEventDto leadEventDto, LeadEvent leadEvent, MappingContext context) {
                        Lead lead = leadRepository.getOne(leadEventDto.getLead());
                        Event event = eventRepository.getOne(leadEventDto.getEvent());
                        leadEvent.setLead(lead);
                        leadEvent.setEvent(event);
                    }

                    @Override
                    public void mapBtoA(LeadEvent leadEvent, LeadEventDto leadEventDto, MappingContext context) {
                        leadEventDto.setEvent(leadEvent.getEventId());
                        leadEventDto.setLead(leadEvent.getLeadId());
                    }
                })
                .byDefault()
                .register();
    }
}
