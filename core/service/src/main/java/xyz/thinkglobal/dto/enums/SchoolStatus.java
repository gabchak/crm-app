package xyz.thinkglobal.dto.enums;

import java.util.HashMap;
import java.util.Map;

public enum SchoolStatus {

    WORKING("working", "test description"),
    PLANNED("planned", "planned"),
    CLOSED("closed", "school closed");

    private String name;
    private String description;

    private static final Map<String, SchoolStatus> NAMES_VALUES = new HashMap<>();

    static {
        for (SchoolStatus val : values()) {
            NAMES_VALUES.put(val.getName(), val);
        }
    }

    public static SchoolStatus findByName(String name) {
        return NAMES_VALUES.get(name);
    }

    public static Map<String, SchoolStatus> getNamesValues() {
        return NAMES_VALUES;
    }

    SchoolStatus(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
}
