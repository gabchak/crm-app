package xyz.thinkglobal.dto.pdf;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ContractDto {

    private boolean varContent;
    private boolean fixed;
    private String contractTitle;
    private String contractSigningDate;
    private String lookupCounterpartyName;
    private String lookupCounterpartyEdrpouCode;
    private String lookupCounterpartyAddress;
    private String lookupCounterpartyActOnBasis;
    private String studentLastName;
    private String studentFirstName;
    private String studentMiddleName;
    private String contactLastName;
    private String contactFirstName;
    private String contactMiddleName;
    private String contractSchool;
    private String contractServiceStartDate;
    private String contractServiceEndDate;
    private String contractFullNumberPlusInWords;
    private String dateOfTheEndOfContract;
    private String contactRegistrationCity;
    private String contactRegistrationStreet;
    private String contactRegistrationHouse;
    private String contactRegistrationFlat;
    private String contactPhone;
    private String lookupCounterpartyNameShort;
    private String lookupCountrePartyPhone;
    private String lookupCounterpartyRequisites;
    private String lookupCounterpartyDirector;
    private String lookupCounterpartyDirectorShort;
    private String fullPasportData;


}
