package xyz.thinkglobal.dto.enums;

import java.util.HashMap;
import java.util.Map;

public enum ContractType {

    ADVANCED_SERVICE_CONTRACT("Договір про надання додаткових послуг", "additional_service_contract.ftl"),
    EDUCATION_SERVICE_CONTRACT("Договір про надання освітніх послу", "education_service_contract.ftl");

    private String description;
    private String template;

    ContractType(String description, String template) {
        this.description = description;
        this.template = template;
    }

    private final static Map<Integer, String> map = new HashMap<>(ContractType.values().length);

    static {
        int i = 0;
        for (ContractType contractType : ContractType.values()) {
            map.put(i++, contractType.getDescription());
        }
    }

    public static Map<Integer, String> getAllWithDescription() {
        return map;
    }

    public String getDescription() {
        return description;
    }

    public String getTemplate() {
        return template;
    }
}
