package xyz.thinkglobal.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PersonDto {

    private Long id;
    private String firstName;
    private String middleName;
    private String lastName;
}
