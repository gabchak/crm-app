package xyz.thinkglobal.dto.filters;

import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class ChildrenFilterDto {

    Long positionId;
    boolean lookForStudents;
    boolean lookForEnrollee;
    Set<Long> schools;
 }
