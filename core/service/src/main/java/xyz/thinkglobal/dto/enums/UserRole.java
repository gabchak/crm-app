package xyz.thinkglobal.dto.enums;

import java.util.HashMap;
import java.util.Map;

public enum UserRole {

    /** The role of super admin */
    ROLE_SYSTEM_ADMINISTRATOR("SYSTEM_ADMINISTRATOR"),

    /** The role School administrator. */
    ROLE_SCHOOL_ADMINISTRATOR("SCHOOL_ADMINISTRATOR"),

    /** The role Family partner. */
    ROLE_FAMILY_PARTNER("FAMILY_PARTNER"),

    /** The role Family representative (contact). */
    ROLE_FAMILY_REPRESENTATIVE("FAMILY_REPRESENTATIVE"),

    /** The role Partner-manager. */
    ROLE_PARTNER_MANAGER("PARTNER_MANAGER"),

    /** The role Contract-manager (Back-office). */
    ROLE_CONTRACT_MANAGER_BO("CONTRACT_MANAGER_BO"),

    /** The role Guest (Back-office). */
    ROLE_GUEST("GUEST");


    private final String securityName;

    private static final Map<String, UserRole> NAMES_VALUES = new HashMap<>();

    static {
        for (UserRole value : values()) {
            NAMES_VALUES.put(value.getSecurityName(), value);
        }
    }

    public static UserRole findByName(String securityName) {
        return NAMES_VALUES.get(securityName);
    }

    UserRole(final String securityName) {
        this.securityName = securityName;
    }

    public String getSecurityName() {
        return securityName;
    }

    public String getFullName() {
        return "ROLE_" + securityName;
    }

}
