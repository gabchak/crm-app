package xyz.thinkglobal.dto;

import lombok.Data;

import java.time.LocalDate;
import java.util.Set;

@Data
public class BookingWithBookingDetailsDto {

    private Long id;
    private String name;
    private LocalDate openDate;
    private Integer capacity;
    private Integer freePlaces;
    private Long enrolleeId;

    private String status;
    private Long studyGroup;

    private Set<BookingDetailsDto> bookingDetailsSet;
}
