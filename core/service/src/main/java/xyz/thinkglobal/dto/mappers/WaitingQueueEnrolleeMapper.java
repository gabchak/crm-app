package xyz.thinkglobal.dto.mappers;

import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import xyz.thinkglobal.dto.WaitingQueueEnrolleeDto;
import xyz.thinkglobal.models.Enrollee;
import xyz.thinkglobal.models.WaitingQueue;
import xyz.thinkglobal.models.WaitingQueueEnrollee;
import xyz.thinkglobal.repositories.EnrolleeRepository;
import xyz.thinkglobal.repositories.WaitingQueueRepository;

@Component
public class WaitingQueueEnrolleeMapper extends ConfigurableMapper {

    private EnrolleeRepository enrolleeRepository;
    private WaitingQueueRepository waitingQueueRepository;

    @Autowired
    public WaitingQueueEnrolleeMapper(EnrolleeRepository enrolleeRepository, WaitingQueueRepository waitingQueueRepository) {
        this.enrolleeRepository = enrolleeRepository;
        this.waitingQueueRepository = waitingQueueRepository;
    }

    @Override
    protected void configure(MapperFactory factory) {
        factory.classMap(WaitingQueueEnrollee.class, WaitingQueueEnrolleeDto.class)
                .exclude("waitingQueue")
                .exclude("enrollee")
                .customize(new CustomMapper<WaitingQueueEnrollee, WaitingQueueEnrolleeDto>() {
                    @Override
                    public void mapAtoB(WaitingQueueEnrollee waitingQueueEnrollee,
                                        WaitingQueueEnrolleeDto waitingQueueEnrolleeDto,
                                        MappingContext context) {
                        super.mapAtoB(waitingQueueEnrollee, waitingQueueEnrolleeDto, context);
                        Long enrolleeId = waitingQueueEnrollee.getEnrollee().getId();
                        waitingQueueEnrolleeDto.setEnrollee(enrolleeId);

                        Long waitingQueueId = waitingQueueEnrollee.getWaitingQueue().getId();
                        waitingQueueEnrolleeDto.setWaitingQueue(waitingQueueId);
                    }

                    @Override
                    public void mapBtoA(WaitingQueueEnrolleeDto waitingQueueEnrolleeDto,
                                        WaitingQueueEnrollee waitingQueueEnrollee,
                                        MappingContext context) {
                        super.mapBtoA(waitingQueueEnrolleeDto, waitingQueueEnrollee, context);
                        Enrollee enrollee = enrolleeRepository
                                .getOne(waitingQueueEnrolleeDto.getEnrollee());
                        waitingQueueEnrollee.setEnrollee(enrollee);

                        WaitingQueue waitingQueue = waitingQueueRepository.getOne(waitingQueueEnrolleeDto.getWaitingQueue());
                        waitingQueueEnrollee.setWaitingQueue(waitingQueue);
                    }
                })
                .byDefault()
                .register();
    }
}
