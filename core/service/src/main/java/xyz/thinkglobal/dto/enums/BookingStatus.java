package xyz.thinkglobal.dto.enums;

import java.util.HashMap;
import java.util.Map;

public enum BookingStatus {

    OPEN("open", "when booking opened"),
    CLOSE("close", "when booking closed");

    private String name;
    private String description;

    private static final Map<String, BookingStatus> NAMES_VALUES = new HashMap<>();

    static {
        for (BookingStatus value : NAMES_VALUES.values()) {
            NAMES_VALUES.put(value.getName(), value);
        }
    }

    public static BookingStatus findByName(String name) {
        return NAMES_VALUES.get(name);
    }

    BookingStatus(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
}
