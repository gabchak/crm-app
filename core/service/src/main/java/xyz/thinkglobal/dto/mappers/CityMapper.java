package xyz.thinkglobal.dto.mappers;

import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import xyz.thinkglobal.dto.CityDto;
import xyz.thinkglobal.models.City;

@Component
public class CityMapper extends ConfigurableMapper {


    @Autowired
    private RegionMapper regionMapper;

    /**
     * Mapper configuration method
     *
     * @param factory mapper factory
     **/
    @Override
    protected void configure(MapperFactory factory) {
        factory.classMap(CityDto.class, City.class)
                .exclude("address")
                .exclude("region")
                .customize(new CustomMapper<CityDto, City>() {
                    @Override
                    public void mapAtoB(CityDto cityDto, City city, MappingContext context) {
                        super.mapAtoB(cityDto, city, context);
                    }

                    @Override
                    public void mapBtoA(City city, CityDto cityDto, MappingContext context) {
                        super.mapBtoA(city, cityDto, context);
                        if (city.getRegion() != null) {
                            cityDto.setRegion(city.getRegion().getId());
                        }
                    }
                })
                .byDefault()
                .register();
    }
}
