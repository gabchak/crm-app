package xyz.thinkglobal.dto.mappers;

import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import xyz.thinkglobal.dto.LeadDto;
import xyz.thinkglobal.models.City;
import xyz.thinkglobal.models.Lead;
import xyz.thinkglobal.repositories.CityRepository;

@Component
public class LeadMapper extends ConfigurableMapper {

    @Autowired
    private CityRepository cityRepository;

    /**
     * Mapper configuration method
     *
     * @param factory mapper factory
     **/
    @Override
    protected void configure(MapperFactory factory) {
        factory.classMap(LeadDto.class, Lead.class)
                .exclude("city")
                .exclude("contact")
                .exclude("leadEventDtoSet")
                .customize(new CustomMapper<LeadDto, Lead>() {
                    @Override
                    public void mapAtoB(LeadDto leadDto, Lead lead, MappingContext context) {
                        super.mapAtoB(leadDto, lead, context);
                        if (leadDto.getCity() != null) {
                            City city = cityRepository.getOne(leadDto.getCity());
                            lead.setCity(city);
                        }
                    }

                    @Override
                    public void mapBtoA(Lead lead, LeadDto leadDto, MappingContext context) {
                        super.mapBtoA(lead, leadDto, context);

                        if (lead.getCity() != null) {
                            leadDto.setCity(lead.getCity().getId());
                        }
                    }
                })
                .byDefault()
                .register();
    }
}
