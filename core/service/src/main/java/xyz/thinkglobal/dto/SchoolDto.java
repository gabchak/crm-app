package xyz.thinkglobal.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class SchoolDto {

    private Long id;
    private String name;
    private String code;
    private String termOfPartnership;

    private AddressDto address;
    private String status;

    private Set<Long> events;
    private Set<Long> positions;
    private Set<Long> studyClasses;
}
