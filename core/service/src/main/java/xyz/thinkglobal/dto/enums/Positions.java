package xyz.thinkglobal.dto.enums;

import java.util.HashMap;
import java.util.Map;

public enum Positions {

    SYSTEM_ADMINISTRATOR("Системний адміністратор", "Have to be created only within SQL query", UserRole.ROLE_SYSTEM_ADMINISTRATOR),

    GUEST("Гість", "Have to be created only within SQL query", UserRole.ROLE_GUEST),

    SCHOOL_ADMINISTRATOR("Шкільний адміністратор", "", UserRole.ROLE_SCHOOL_ADMINISTRATOR),

    PARTNER_MANAGER("Контакт менеджер", "New user without positions", UserRole.ROLE_PARTNER_MANAGER),

    FAMILY_REPRESENTATIVE("Представник сім'ї", "Contact", UserRole.ROLE_FAMILY_REPRESENTATIVE),

    FAMILY_PARTNER("Фемілі-партнер", "опис", UserRole.ROLE_FAMILY_PARTNER);

    private String name;
    private String description;
    private UserRole role;

    private static final Map<String, Positions> NAMES_VALUES = new HashMap<>();
    private static final Map<String, String> POSITIONS_MAP_FOR_REGISTRATION = new HashMap<>();

    static {
        for (Positions val : values()) {
            NAMES_VALUES.put(val.toString(), val);
        }
    }

    static {
        Positions[] positions = Positions.values();
        for (Positions p : positions) {
            POSITIONS_MAP_FOR_REGISTRATION.put(p.toString(), p.getName());
        }
        POSITIONS_MAP_FOR_REGISTRATION.remove("SYSTEM_ADMINISTRATOR");
//        POSITIONS_MAP_FOR_REGISTRATION.remove("GUEST");
    }

    public static Positions findByName(String name) {
        return NAMES_VALUES.get(name);
    }

    public static Map<String, Positions> getNamesValues() {
        return NAMES_VALUES;
    }

    Positions(String name, String description, UserRole role) {
        this.name = name;
        this.description = description;
        this.role = role;
    }

    public String getName() {
        return name;
    }

    public UserRole getRole() {
        return role;
    }

    public String getDescription() {
        return description;
    }

    public static String getRoleByPositionName(String position) {
        return NAMES_VALUES.get(position).getRole().getFullName();
    }

    public static Map<String, String> getNamesValuesForRegistration() {
        return POSITIONS_MAP_FOR_REGISTRATION;
    }
}
