package xyz.thinkglobal.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Set;

@Getter
@Setter
public class WaitingQueueDto {

    private Long id;
    private String uniqueCode;
    private LocalDate registrationDate;
    private String status;
    private Long studyClass;
    private LocalDate expirationDate;
    private String conditionForBookingOpening;
    private String hasScheduledStudyRoom;

    private Set<Long> enrolleeSet;
}
