package xyz.thinkglobal.dto.mappers;

import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import xyz.thinkglobal.dto.AddressDto;
import xyz.thinkglobal.models.Address;
import xyz.thinkglobal.models.City;
import xyz.thinkglobal.repositories.AddressRepository;
import xyz.thinkglobal.repositories.CityRepository;

import java.util.Optional;

@Component
public class AddressMapper extends ConfigurableMapper {

    private CityRepository cityRepository;
    private AddressRepository addressRepository;

    @Autowired
    public AddressMapper(CityRepository cityRepository, 
                         AddressRepository addressRepository) {
        this.cityRepository = cityRepository;
        this.addressRepository = addressRepository;
    }

    @Override
    protected void configure(MapperFactory factory) {
        factory.classMap(AddressDto.class, Address.class)
                .exclude("city")
                .customize(new CustomMapper<AddressDto, Address>() {
                    @Override
                    public void mapAtoB(AddressDto addressDto, Address address, MappingContext context) {
                        super.mapAtoB(addressDto, address, context);
                        City city = cityRepository.getOne(addressDto.getCity());
                        address.setCity(city);
                    }

                    @Override
                    public void mapBtoA(Address address, AddressDto addressDto, MappingContext context) {
                        super.mapBtoA(address, addressDto, context);
                        addressDto.setCity(address.getCity().getId());
                    }
                })
                .byDefault()
                .register();
    }

    public Address getExistAddressOrCreate(AddressDto addressDto) {
        Address address;
        Optional<Address> optionalRegistrationAddress = addressRepository.findByCity_IdAndStreetAndHouseAndFlat(
                addressDto.getCity(),
                addressDto.getStreet(),
                addressDto.getHouse(),
                addressDto.getFlat()
        );
        if (optionalRegistrationAddress.isPresent()) {
            address = optionalRegistrationAddress.get();
        } else {
            Address mappedAddress = this.map(addressDto, Address.class);
            address = addressRepository.save(mappedAddress);
        }
        return address;
    }
}
