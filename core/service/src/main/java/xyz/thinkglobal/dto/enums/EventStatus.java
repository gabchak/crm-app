package xyz.thinkglobal.dto.enums;


import java.util.HashMap;
import java.util.Map;

public enum EventStatus {

    NEW("Нова", "it's new event"),
    APPROVED("Підтверджена", "event in progress, whatever that means"),
    FINISHED("Завершена", "it's event is over");

    private String name;
    private String description;

    private static final Map<String, EventStatus> NAMES_VALUES = new HashMap<>();

    static {
        for (EventStatus val : values()) {
            NAMES_VALUES.put(val.getName(), val);
        }
    }

    EventStatus(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public static EventStatus findByName(String name){
        return NAMES_VALUES.get(name);
    }

    public static Map<String, EventStatus> getNamesValues() {
        return NAMES_VALUES;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
}
