package xyz.thinkglobal.dto.mappers;

import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import xyz.thinkglobal.dto.PositionDto;
import xyz.thinkglobal.dto.UserDto;
import xyz.thinkglobal.models.Position;
import xyz.thinkglobal.models.Role;
import xyz.thinkglobal.models.User;
import xyz.thinkglobal.repositories.RoleRepository;

import java.util.HashSet;
import java.util.Set;

@Component
public class UserMapper extends ConfigurableMapper {

    @Autowired
    private PositionMapper positionMapper;

    @Override
    protected void configure(MapperFactory factory) {
        factory.classMap(UserDto.class, User.class)
                .exclude("roles")
                .exclude("positions")
                .exclude("password")
                .exclude("email")
                .customize(new CustomMapper<UserDto, User>() {
                    @Override
                    public void mapAtoB(UserDto userDto, User user, MappingContext context) {
                        super.mapAtoB(userDto, user, context);
                        if (userDto.getPositions() != null && !userDto.getPositions().isEmpty()) {
                            Set<Position> positions = positionMapper.mapAsSet(userDto.getPositions(), Position.class);
                            positions.forEach(position -> position.setUser(user));
                            if (user.getPositions() != null) {
                                user.getPositions().addAll(positions);
                            } else {
                                user.setPositions(positions);
                            }
                        }
                    }

                    @Override
                    public void mapBtoA(User user, UserDto userDto, MappingContext context) {
                        super.mapBtoA(user, userDto, context);
                        if (user.getPositions() != null && !user.getPositions().isEmpty()) {
                            userDto.setPositions(positionMapper.mapAsSet(user.getPositions(), PositionDto.class));
                        }
                        userDto.setEmail(user.getEmail());
                    }
                })
                .byDefault()
                .register();
    }
}
