package xyz.thinkglobal.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Set;

@Getter
@Setter
public class StudyGroupDto {

    private Long id;
    private String name;
    private int numberOfSeats;
    private LocalDate openDate;
    private LocalDate closeDate;

    private String status;
    private Long studyClass;
    private Long booking;

    private Set<Long> students;
}
