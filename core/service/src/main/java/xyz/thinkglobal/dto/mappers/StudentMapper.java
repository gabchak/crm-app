package xyz.thinkglobal.dto.mappers;

import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import xyz.thinkglobal.dto.StudentDto;
import xyz.thinkglobal.models.Student;
import xyz.thinkglobal.models.StudyGroup;
import xyz.thinkglobal.repositories.StudyGroupRepository;

import java.util.HashSet;
import java.util.Set;

@Component
public class StudentMapper extends ConfigurableMapper {

    private StudyGroupRepository studyGroupRepository;

    @Autowired
    public StudentMapper(StudyGroupRepository studyGroupRepository) {
        this.studyGroupRepository = studyGroupRepository;
    }

    @Override
    protected void configure(MapperFactory factory) {
        factory.classMap(StudentDto.class, Student.class)
                .exclude("familyRepresentativeSet")
                .exclude("studyGroup")
                .customize(new CustomMapper<StudentDto, Student>() {
                    @Override
                    public void mapAtoB(StudentDto studentDto, Student student, MappingContext context) {
                        super.mapAtoB(studentDto, student, context);
                        StudyGroup studyGroup = studyGroupRepository.getOne(studentDto.getStudyGroup());
                        student.setStudyGroup(studyGroup);
                    }

                    @Override
                    public void mapBtoA(Student student, StudentDto studentDto, MappingContext context) {
                        super.mapBtoA(student, studentDto, context);
                        studentDto.setStudyGroup(student.getStudyGroup().getId());
                    }
                })
                .byDefault()
                .register();
    }

    public Set<Long> mapStudentsAsListOfId(Set<Student> students) {
        Set<Long> result = new HashSet<>();

        for (Student student : students) {
            result.add(student.getId());
        }
        return result;
    }
}
