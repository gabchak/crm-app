package xyz.thinkglobal.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class AuthenticationDto implements Serializable {

    private String username;
    private String password;
}
