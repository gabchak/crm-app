package xyz.thinkglobal.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Set;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class ContactDto extends UserDto {

    private String seriesAndPassportNumber;
    private LocalDate dateOfIssueOfPassport;
    private String whoIssuedPassport;
    private int inn;

    private AddressDto registrationAddress;
    private AddressDto residenceAddress;

    private Set<Long> students;
    private Set<Long> enrolleeSet;
}
