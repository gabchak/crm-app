package xyz.thinkglobal.dto.filters;

import lombok.Data;

@Data
public class LeadFilterDto {

    private String email;
    private String firstName;
    private String lastName;
    private String phone;
}
