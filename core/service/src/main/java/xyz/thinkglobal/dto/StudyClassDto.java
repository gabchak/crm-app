package xyz.thinkglobal.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class StudyClassDto {

    private Long id;
    private Integer name;

    private Long school;

    private Set<Long> studyGroups;
}
