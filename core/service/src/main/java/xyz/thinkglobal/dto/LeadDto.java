package xyz.thinkglobal.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class LeadDto extends PersonDto {

    private String status;
    private Long city;
    private String howYouFindUs;
    private String email;
    private String phone;
    private String additionalPhone;

    private Set<LeadEventDto> leadEventDtoSet;
}
