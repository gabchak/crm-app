package xyz.thinkglobal.dto.mappers;

import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import xyz.thinkglobal.dto.EventDto;
import xyz.thinkglobal.models.Event;
import xyz.thinkglobal.models.School;
import xyz.thinkglobal.repositories.SchoolRepository;

@Component
public class EventMapper extends ConfigurableMapper {

    @Autowired
    private SchoolRepository schoolRepository;

    /**
     * Mapper configuration method
     *
     * @param factory mapper factory
     **/
    @Override
    protected void configure(MapperFactory factory) {
        factory.classMap(EventDto.class, Event.class)
                .exclude("school")
                .exclude("speakers")
                .exclude("leadEventSet")
                .customize(new CustomMapper<EventDto, Event>() {
                    @Override
                    public void mapAtoB(EventDto eventDto, Event event, MappingContext context) {
                        super.mapAtoB(eventDto, event, context);
                        School school = schoolRepository.getOne(eventDto.getSchool());
                        event.setSchool(school);
                    }

                    @Override
                    public void mapBtoA(Event event, EventDto eventDto, MappingContext context) {
                        super.mapBtoA(event, eventDto, context);
                        eventDto.setSchool(event.getSchool().getId());
                    }
                })
                .byDefault()
                .register();
    }
}
