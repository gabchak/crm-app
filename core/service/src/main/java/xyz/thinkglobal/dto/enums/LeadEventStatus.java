package xyz.thinkglobal.dto.enums;

import java.util.HashMap;
import java.util.Map;

public enum LeadEventStatus {

    REGISTERED("registered", "when lead registered for event"),
    VISITED("visited", "when lead was on event"),
    NOT_VISITED("not visited", "when lead wasn't on event");

    private String name;
    private String description;

    private static final Map<String, LeadEventStatus> NAMES_VALUES = new HashMap<>();

    static {
        for (LeadEventStatus val : values()) {
            NAMES_VALUES.put(val.getName(), val);
        }
    }

    public static LeadEventStatus findByName(String name){
        return NAMES_VALUES.get(name);
    }

    public static Map<String, LeadEventStatus> getNamesValues() {
        return NAMES_VALUES;
    }

    LeadEventStatus(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
}
