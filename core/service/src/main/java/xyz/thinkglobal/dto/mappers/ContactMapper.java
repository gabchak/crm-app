package xyz.thinkglobal.dto.mappers;

import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import xyz.thinkglobal.dto.AddressDto;
import xyz.thinkglobal.dto.ContactDto;
import xyz.thinkglobal.models.Address;
import xyz.thinkglobal.models.Contact;
import xyz.thinkglobal.repositories.AddressRepository;

@Component
public class ContactMapper extends ConfigurableMapper {

    private AddressMapper addressMapper;

    @Autowired
    public ContactMapper(AddressMapper addressMapper) {
        this.addressMapper = addressMapper;
    }

    @Override
    protected void configure(MapperFactory factory) {
        factory.classMap(ContactDto.class, Contact.class)
                .exclude("students")
                .exclude("enrolleeSet")
                .exclude("registrationAddress")
                .exclude("residenceAddress")
                .customize(new CustomMapper<ContactDto, Contact>() {
                    @Override
                    public void mapAtoB(ContactDto contactDto, Contact contact, MappingContext context) {
                        super.mapAtoB(contactDto, contact, context);

                        Address registrationAddress =
                                addressMapper.getExistAddressOrCreate(contactDto.getRegistrationAddress());
                        contact.setRegistrationAddress(registrationAddress);

                        Address residenceAddress =
                                addressMapper.getExistAddressOrCreate(contactDto.getResidenceAddress());
                        contact.setResidenceAddress(residenceAddress);
                    }

                    @Override
                    public void mapBtoA(Contact contact, ContactDto contactDto, MappingContext context) {
                        super.mapBtoA(contact, contactDto, context);

                        AddressDto mappedRegistrationAddressDto =
                                addressMapper.map(contact.getRegistrationAddress(), AddressDto.class);
                        contactDto.setRegistrationAddress(mappedRegistrationAddressDto);

                        AddressDto mappedResidenceAddressDto =
                                addressMapper.map(contact.getResidenceAddress(), AddressDto.class);
                        contactDto.setResidenceAddress(mappedResidenceAddressDto);
                    }
                })
                .byDefault()
                .register();
    }

}
