package xyz.thinkglobal.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class EnrolleeDto extends ChildDto {

    private Long bookingDetails;

    private Set<Long> familyRepresentativeSet;
    private Set<Long> waitingQueueEnrolleeSet;
}
