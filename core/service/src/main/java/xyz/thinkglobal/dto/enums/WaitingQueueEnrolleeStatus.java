package xyz.thinkglobal.dto.enums;

public enum WaitingQueueEnrolleeStatus {

    ACTIVE("Активний"),
    INACTIVE("Не активний");

    private String name;

    WaitingQueueEnrolleeStatus(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
