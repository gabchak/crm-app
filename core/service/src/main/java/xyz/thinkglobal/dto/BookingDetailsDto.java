package xyz.thinkglobal.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class BookingDetailsDto {

    private Long id;
    private LocalDate registrationDate;
    private LocalDate expirationDate;

    private Long booking;
}
