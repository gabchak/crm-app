package xyz.thinkglobal.dto.mappers;

import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import xyz.thinkglobal.dto.BookingDto;
import xyz.thinkglobal.models.Booking;
import xyz.thinkglobal.models.StudyGroup;
import xyz.thinkglobal.repositories.StudyGroupRepository;

@Component
public class BookingMapper extends ConfigurableMapper {

    private StudyGroupRepository studyGroupRepository;

    @Autowired
    public BookingMapper(StudyGroupRepository studyGroupRepository) {
        this.studyGroupRepository = studyGroupRepository;
    }

    @Override
    protected void configure(MapperFactory factory) {
        factory.classMap(BookingDto.class, Booking.class)
                .exclude("bookingDetailsSet")
                .exclude("studyGroup")
                .customize(new CustomMapper<BookingDto, Booking>() {
                    @Override
                    public void mapAtoB(BookingDto bookingDto, Booking booking, MappingContext context) {
                        super.mapAtoB(bookingDto, booking, context);
                        StudyGroup studyGroup = studyGroupRepository.getOne(bookingDto.getStudyGroup());
                        booking.setStudyGroup(studyGroup);
                    }

                    @Override
                    public void mapBtoA(Booking booking, BookingDto bookingDto, MappingContext context) {
                        super.mapBtoA(booking, bookingDto, context);
                        bookingDto.setStudyGroup(booking.getStudyGroup().getId());
                    }
                })
                .byDefault()
                .register();
    }
}
