package xyz.thinkglobal.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class UserDto extends PersonDto {

    private String password;
    private String email;
    private String phone;
    private String additionalPhone;

    private Set<PositionDto> positions;
}
