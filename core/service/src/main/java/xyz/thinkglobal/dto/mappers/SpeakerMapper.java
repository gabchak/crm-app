package xyz.thinkglobal.dto.mappers;

import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.stereotype.Component;
import xyz.thinkglobal.dto.SpeakerDto;
import xyz.thinkglobal.models.Speaker;

@Component
public class SpeakerMapper extends ConfigurableMapper {

    @Override
    protected void configure(MapperFactory factory) {
        factory.classMap(SpeakerDto.class, Speaker.class)
                .exclude("events")
                .customize(new CustomMapper<SpeakerDto, Speaker>() {
                    @Override
                    public void mapAtoB(SpeakerDto speakerDto, Speaker speaker, MappingContext context) {
                        super.mapAtoB(speakerDto, speaker, context);
                    }

                    @Override
                    public void mapBtoA(Speaker speaker, SpeakerDto speakerDto, MappingContext context) {
                        super.mapBtoA(speaker, speakerDto, context);
                    }
                })
                .byDefault()
                .register();
    }
}
