package xyz.thinkglobal.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class EventFinishedDto {

    private Long eventId;
    private List<Long> visitedLeads;

}
