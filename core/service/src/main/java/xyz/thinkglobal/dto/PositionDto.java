package xyz.thinkglobal.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PositionDto {

    private Long id;
    private Long school;
    private Long user;
    private String position;
}
