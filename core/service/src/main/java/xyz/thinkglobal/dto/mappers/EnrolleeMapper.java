package xyz.thinkglobal.dto.mappers;

import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import xyz.thinkglobal.dto.EnrolleeDto;
import xyz.thinkglobal.models.BookingDetails;
import xyz.thinkglobal.models.Enrollee;
import xyz.thinkglobal.repositories.BookingDetailsRepository;

@Component
public class EnrolleeMapper extends ConfigurableMapper {

    private BookingDetailsRepository bookingDetailsRepository;

    @Autowired
    public EnrolleeMapper(BookingDetailsRepository bookingDetailsRepository) {
        this.bookingDetailsRepository = bookingDetailsRepository;
    }

    @Override
    protected void configure(MapperFactory factory) {
        factory.classMap(EnrolleeDto.class, Enrollee.class)
                .exclude("waitingQueueEnrolleeSet")
                .exclude("bookingDetails")
                .exclude("contacts")
                .exclude("familyRepresentativeSet")
                .customize(new CustomMapper<EnrolleeDto, Enrollee>() {
                    @Override
                    public void mapAtoB(EnrolleeDto enrolleeDto, Enrollee enrollee, MappingContext context) {
                        super.mapAtoB(enrolleeDto, enrollee, context);

                        if (enrolleeDto.getBookingDetails() != null) {
                            BookingDetails bookingDetails = bookingDetailsRepository.getOne(enrolleeDto.getBookingDetails());
                            enrollee.setBookingDetails(bookingDetails);
                        }
                    }

                    @Override
                    public void mapBtoA(Enrollee enrollee, EnrolleeDto enrolleeDto, MappingContext context) {
                        super.mapBtoA(enrollee, enrolleeDto, context);

                        if (enrollee.getBookingDetails() != null) {
                            enrolleeDto.setBookingDetails(enrollee.getBookingDetails().getId());
                        }
                    }
                })
                .byDefault()
                .register();
    }
}
