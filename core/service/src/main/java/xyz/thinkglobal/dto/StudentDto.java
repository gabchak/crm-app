package xyz.thinkglobal.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class StudentDto extends ChildDto {

    private Long studyGroup;

    private Set<Long> familyRepresentativeSet;

}
