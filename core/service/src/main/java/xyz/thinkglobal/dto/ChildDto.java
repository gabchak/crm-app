package xyz.thinkglobal.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class ChildDto extends PersonDto {

    private LocalDate dateOfBirth;
    private String birthCertificate;

    private Long majorFamilyRepresentative;
}
