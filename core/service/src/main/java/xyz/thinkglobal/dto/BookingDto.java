package xyz.thinkglobal.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Set;

@Getter
@Setter
public class BookingDto {

    private Long id;
    private String name;
    private LocalDate openDate;
    private Integer capacity;
    private Integer freePlaces;
    private Long enrolleeId;

    private String status;
    private Long studyGroup;

    private Set<Long> bookingDetailsSet;
}
