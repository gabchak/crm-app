package xyz.thinkglobal.dto.enums;

import java.util.HashMap;
import java.util.Map;

public enum EventType {

    ENTRY_TO_SCHOOL("Запис в школу"),
    MEETING_OF_TEACHERS("Зустріч вчителів"),
    INDIVIDUAL_MEETING("Індивідуальна зустріч з очільником");

    private String name;

    private static final Map<String, EventType> NAMES_VALUES = new HashMap<>();

    EventType(String name) {
        this.name = name;
    }

    static {
        for (EventType val : values()) {
            NAMES_VALUES.put(val.getName(), val);
        }
    }

    public static EventType findByName(String name){
        return NAMES_VALUES.get(name);
    }

    public static Map<String, EventType> getNamesValues() {
        return NAMES_VALUES;
    }

    public String getName() {
        return name;
    }
}
