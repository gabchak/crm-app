package xyz.thinkglobal.dto.enums;

import java.util.HashMap;
import java.util.Map;

public enum TermOfPartnership {

    OWN("Власна"),
    FRANCHISING("Франчайзі"),
    WHITE_LABEL("White label");

    private String name;

    TermOfPartnership(String name) {
        this.name = name;
    }

    private static final Map<String, TermOfPartnership> NAMES_VALUES = new HashMap<>();

    static {
        for (TermOfPartnership val : values()) {
            NAMES_VALUES.put(val.getName(), val);
        }
    }

    public static TermOfPartnership findByName(String name) {
        return NAMES_VALUES.get(name);
    }

    public static Map<String, TermOfPartnership> getNamesValues() {
        return NAMES_VALUES;
    }

    public String getName() {
        return name;
    }
}
