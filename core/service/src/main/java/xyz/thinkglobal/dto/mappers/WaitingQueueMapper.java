package xyz.thinkglobal.dto.mappers;

import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import xyz.thinkglobal.dto.WaitingQueueDto;
import xyz.thinkglobal.models.StudyClass;
import xyz.thinkglobal.models.WaitingQueue;
import xyz.thinkglobal.repositories.StudyClassRepository;

@Component
public class WaitingQueueMapper extends ConfigurableMapper {

    private StudyClassRepository studyClassRepository;

    @Autowired
    public WaitingQueueMapper(StudyClassRepository studyClassRepository) {
        this.studyClassRepository = studyClassRepository;
    }

    /**
     * Mapper configuration method
     *
     * @param factory mapper factory
     **/
    @Override
    protected void configure(MapperFactory factory) {
        factory.classMap(WaitingQueueDto.class, WaitingQueue.class)
                .exclude("enrolleeSet")
                .exclude("studyClass")
                .customize(new CustomMapper<WaitingQueueDto, WaitingQueue>() {
                    @Override
                    public void mapAtoB(WaitingQueueDto waitingQueueDto, WaitingQueue waitingQueue, MappingContext context) {
                        super.mapAtoB(waitingQueueDto, waitingQueue, context);
                        StudyClass studyClass = studyClassRepository.getOne(waitingQueueDto.getStudyClass());
                        waitingQueue.setStudyClass(studyClass);

                    }

                    @Override
                    public void mapBtoA(WaitingQueue waitingQueue, WaitingQueueDto waitingQueueDto, MappingContext context) {
                        super.mapBtoA(waitingQueue, waitingQueueDto, context);
                        waitingQueueDto.setStudyClass(waitingQueue.getStudyClass().getId());
                    }
                })
                .byDefault()
                .register();
    }
}
