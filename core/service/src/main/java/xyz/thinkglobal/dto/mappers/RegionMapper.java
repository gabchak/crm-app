package xyz.thinkglobal.dto.mappers;

import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.stereotype.Component;
import xyz.thinkglobal.dto.RegionDto;
import xyz.thinkglobal.models.Region;

@Component
public class RegionMapper extends ConfigurableMapper {
    @Override
    protected void configure(MapperFactory factory) {
        factory.classMap(RegionDto.class, Region.class)
                .exclude("cities")
                .customize(new CustomMapper<RegionDto, Region>() {
                    @Override
                    public void mapAtoB(RegionDto regionDto, Region region, MappingContext context) {
                        super.mapAtoB(regionDto, region, context);
                    }

                    @Override
                    public void mapBtoA(Region region, RegionDto regionDto, MappingContext context) {
                        super.mapBtoA(region, regionDto, context);
                    }
                })
                .byDefault()
                .register();
    }
}
