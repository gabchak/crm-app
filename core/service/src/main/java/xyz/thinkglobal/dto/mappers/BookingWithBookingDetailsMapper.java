package xyz.thinkglobal.dto.mappers;

import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import xyz.thinkglobal.dto.BookingDetailsDto;
import xyz.thinkglobal.dto.BookingWithBookingDetailsDto;
import xyz.thinkglobal.models.Booking;
import xyz.thinkglobal.models.BookingDetails;
import xyz.thinkglobal.models.StudyGroup;
import xyz.thinkglobal.repositories.StudyGroupRepository;

import java.util.Optional;
import java.util.Set;

@Component
public class BookingWithBookingDetailsMapper extends ConfigurableMapper {


    private BookingDetailsMapper bookingDetailsMapper;
    private final StudyGroupRepository studyGroupRepository;

    @Autowired
    public BookingWithBookingDetailsMapper(BookingDetailsMapper bookingDetailsMapper,
                                           StudyGroupRepository studyGroupRepository) {
        this.bookingDetailsMapper = bookingDetailsMapper;
        this.studyGroupRepository = studyGroupRepository;
    }

    @Override
    protected void configure(MapperFactory factory) {
        factory.classMap(BookingWithBookingDetailsDto.class, Booking.class)
                .exclude("studyGroup")
                .exclude("bookingDetailsSet")
                .customize(new CustomMapper<BookingWithBookingDetailsDto, Booking>() {
                    @Override
                    public void mapAtoB(BookingWithBookingDetailsDto bookingWithBookingDetailsDto,
                                        Booking booking, MappingContext context) {
                        super.mapAtoB(bookingWithBookingDetailsDto, booking, context);

                        Optional<StudyGroup> optionalStudyGroup = studyGroupRepository
                                .findById(bookingWithBookingDetailsDto.getStudyGroup());
                        optionalStudyGroup.ifPresent(booking::setStudyGroup);

                        if (bookingWithBookingDetailsDto.getBookingDetailsSet() != null) {
                            Set<BookingDetails> bookingDetails = bookingDetailsMapper
                                    .mapAsSet(bookingWithBookingDetailsDto.getBookingDetailsSet(),
                                    BookingDetails.class);
                            booking.setBookingDetailsSet(bookingDetails);
                        }
                    }

                    @Override
                    public void mapBtoA(Booking booking,
                                        BookingWithBookingDetailsDto bookingWithBookingDetailsDto,
                                        MappingContext context) {
                        super.mapBtoA(booking, bookingWithBookingDetailsDto, context);

                        bookingWithBookingDetailsDto.setStudyGroup(booking.getStudyGroup().getId());

                        if (booking.getBookingDetailsSet() != null) {
                            Set<BookingDetailsDto> bookingDetailsDto = bookingDetailsMapper
                                    .mapAsSet(booking.getBookingDetailsSet(),
                                            BookingDetailsDto.class);
                            bookingWithBookingDetailsDto.setBookingDetailsSet(bookingDetailsDto);
                        }
                    }
                })
                .byDefault()
                .register();
    }
}
