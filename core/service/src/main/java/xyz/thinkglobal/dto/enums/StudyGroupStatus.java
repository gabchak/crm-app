package xyz.thinkglobal.dto.enums;

public enum StudyGroupStatus {

    OPEN("open", "when group open"), CLOSED("closed", "when group closed");

    private String name;
    private String description;

    StudyGroupStatus(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
}
