package xyz.thinkglobal.dto.mappers;

import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import xyz.thinkglobal.dto.StudyGroupDto;
import xyz.thinkglobal.models.Booking;
import xyz.thinkglobal.models.StudyClass;
import xyz.thinkglobal.models.StudyGroup;
import xyz.thinkglobal.repositories.BookingRepository;
import xyz.thinkglobal.repositories.StudyClassRepository;

@Component
public class StudyGroupMapper extends ConfigurableMapper {

    private BookingRepository bookingRepository;
    private StudyClassRepository studyClassRepository;

    @Autowired
    public StudyGroupMapper(BookingRepository bookingRepository, StudyClassRepository studyClassRepository) {
        this.bookingRepository = bookingRepository;
        this.studyClassRepository = studyClassRepository;
    }

    @Override
    protected void configure(MapperFactory factory) {
        factory.classMap(StudyGroupDto.class, StudyGroup.class)
                .exclude("booking")
                .exclude("students")
                .exclude("studyClass")
                .customize(new CustomMapper<StudyGroupDto, StudyGroup>() {
                    @Override
                    public void mapAtoB(StudyGroupDto studyGroupDto, StudyGroup studyGroup, MappingContext context) {
                        super.mapAtoB(studyGroupDto, studyGroup, context);
                        if (studyGroupDto.getBooking() != null) {
                            Booking booking = bookingRepository.getOne(studyGroupDto.getBooking());
                            studyGroup.setBooking(booking);
                        }
                        StudyClass studyClass = studyClassRepository.getOne(studyGroupDto.getStudyClass());
                        studyGroup.setStudyClass(studyClass);
                    }

                    @Override
                    public void mapBtoA(StudyGroup studyGroup, StudyGroupDto studyGroupDto, MappingContext context) {
                        super.mapBtoA(studyGroup, studyGroupDto, context);
                        if (studyGroup.getBooking() != null) {
                            studyGroupDto.setBooking(studyGroup.getBooking().getId());
                        }
                        studyGroupDto.setStudyClass(studyGroup.getStudyClass().getId());
                    }
                })
                .byDefault()
                .register();
    }
}
