package xyz.thinkglobal.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class SpeakerDto extends PersonDto {

    private String position;
    private String photo;

    private Set<Long> events;
}
