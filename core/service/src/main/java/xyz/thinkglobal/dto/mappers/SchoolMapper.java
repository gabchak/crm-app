package xyz.thinkglobal.dto.mappers;

import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import xyz.thinkglobal.dto.AddressDto;
import xyz.thinkglobal.dto.SchoolDto;
import xyz.thinkglobal.models.Address;
import xyz.thinkglobal.models.School;
import xyz.thinkglobal.repositories.AddressRepository;

@Component
public class SchoolMapper extends ConfigurableMapper {

    private AddressMapper addressMapper;

    @Autowired
    public SchoolMapper(AddressMapper addressMapper) {
        this.addressMapper = addressMapper;
    }

    @Override
    protected void configure(MapperFactory factory) {
        factory.classMap(SchoolDto.class, School.class)
                .exclude("address")
                .exclude("events")
                .exclude("positions")
                .exclude("studyClasses")
                .customize(new CustomMapper<SchoolDto, School>() {
                    @Override
                    public void mapAtoB(SchoolDto schoolDto, School school, MappingContext context) {
                        super.mapAtoB(schoolDto, school, context);

                        Address address = addressMapper.getExistAddressOrCreate(schoolDto.getAddress());
                        school.setAddress(address);
                    }

                    @Override
                    public void mapBtoA(School school, SchoolDto schoolDto, MappingContext context) {
                        super.mapBtoA(school, schoolDto, context);
                        AddressDto addressDto = addressMapper.map(school.getAddress(), AddressDto.class);
                        schoolDto.setAddress(addressDto);
                    }
                })
                .byDefault()
                .register();
    }
}
