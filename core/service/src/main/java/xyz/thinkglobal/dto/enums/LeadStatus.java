package xyz.thinkglobal.dto.enums;

import java.util.HashMap;
import java.util.Map;

public enum LeadStatus {

    NEW("new", "guy is new"),
    WAITING_FOR_EVENT("waiting for event", "wait for events in his town"),
    REGISTERED_FOR_EVENT("registered for event", "registered on the event"),
    MISSED_CALL("missed call", "his called but we didn't hear"),
    RECALL("recall", "recall the guy"),
    IN_PROGRESS("in progress", "have been not register after visiting"),
    ON_HOLD("on hold", "frosty one"),
    NOT_INTERESTED("not interested", "love passed");

    private String name;
    private String description;

    private static final Map<String, LeadStatus> NAMES_VALUES = new HashMap<>();

    static {
        for (LeadStatus val : values()) {
            NAMES_VALUES.put(val.getName(), val);
        }
    }

    public static LeadStatus findByName(String name){
        return NAMES_VALUES.get(name);
    }

    public static Map<String, LeadStatus> getNamesValues() {
        return NAMES_VALUES;
    }

    LeadStatus(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
}
