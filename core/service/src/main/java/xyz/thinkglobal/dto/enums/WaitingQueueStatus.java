package xyz.thinkglobal.dto.enums;

import java.util.HashMap;
import java.util.Map;

public enum WaitingQueueStatus {
    
    OPEN("open", "when waiting list opened"),
    CLOSE("close", "when waiting list closed");

    private String name;
    private String description;

    private static final Map<String, WaitingQueueStatus> NAMES_VALUES = new HashMap<>();

    static {
        for (WaitingQueueStatus value : NAMES_VALUES.values()) {
            NAMES_VALUES.put(value.getName(), value);
        }
    }

    public static WaitingQueueStatus findByName(String name) {
        return NAMES_VALUES.get(name);
    }

    WaitingQueueStatus(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
}
