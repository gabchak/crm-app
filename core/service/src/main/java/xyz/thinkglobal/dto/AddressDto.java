package xyz.thinkglobal.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AddressDto {

    private String street;
    private String house;
    private String flat;

    private Long city;
}
