package xyz.thinkglobal.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RoleDto {

    private Integer id;
    private String role;
}
