package xyz.thinkglobal.dto.mappers;

import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import xyz.thinkglobal.dto.BookingDetailsDto;
import xyz.thinkglobal.models.Booking;
import xyz.thinkglobal.models.BookingDetails;
import xyz.thinkglobal.repositories.BookingRepository;

@Component
public class BookingDetailsMapper extends ConfigurableMapper {

    private final BookingRepository bookingRepository;

    @Autowired
    public BookingDetailsMapper(BookingRepository bookingRepository) {
        this.bookingRepository = bookingRepository;
    }

    @Override
    protected void configure(MapperFactory factory) {
        factory.classMap(BookingDetailsDto.class, BookingDetails.class)
                .exclude("booking")
                .exclude("enrollee")
                .customize(new CustomMapper<BookingDetailsDto, BookingDetails>() {
                    @Override
                    public void mapAtoB(BookingDetailsDto bookingDetailsDto, BookingDetails bookingDetails, MappingContext context) {
                        super.mapAtoB(bookingDetailsDto, bookingDetails, context);
                        Booking booking = bookingRepository.getOne(bookingDetailsDto.getBooking());
                        bookingDetails.setBooking(booking);
                    }

                    @Override
                    public void mapBtoA(BookingDetails bookingDetails, BookingDetailsDto bookingDetailsDto, MappingContext context) {
                        super.mapBtoA(bookingDetails, bookingDetailsDto, context);
                        bookingDetailsDto.setBooking(bookingDetails.getBooking().getId());
                    }
                })
                .byDefault()
                .register();
    }
}
