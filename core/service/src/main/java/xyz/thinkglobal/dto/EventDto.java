package xyz.thinkglobal.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.Set;

@Getter
@Setter
public class EventDto {

    private Long id;
    private String name;
    private String description;
    private LocalDateTime startDate;

    private String status;
    private Long school;
    private String type;

    private Set<Long> speakers;
    private Set<LeadEventDto> leadToEventSet;
}
