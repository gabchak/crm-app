package xyz.thinkglobal.dto.enums;

import java.util.HashMap;
import java.util.Map;

public enum HowYouFindUs {

    FACEBOOK("facebook"),
    FRIENDS_RECOMENDATIONS("Рекомендації друзів"),
    SITE("Сайт"),
    LOCAL_ADWERTISING("Місцева реклама"),
    OTHERS("Інше");


    private String name;

    HowYouFindUs(String name) {
        this.name = name;
    }

    private static final Map<String, HowYouFindUs> NAMES_VALUES = new HashMap<>();

    static {
        for (HowYouFindUs val : values()) {
            NAMES_VALUES.put(val.getName(), val);
        }
    }

    public static HowYouFindUs findByName(String name){
        return NAMES_VALUES.get(name);
    }

    public String getName() {
        return name;
    }

    public static Map<String, HowYouFindUs> getNamesValuesMap() {
        return NAMES_VALUES;
    }
}
