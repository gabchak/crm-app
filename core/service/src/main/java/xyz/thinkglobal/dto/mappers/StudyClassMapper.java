package xyz.thinkglobal.dto.mappers;

import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import xyz.thinkglobal.dto.StudyClassDto;
import xyz.thinkglobal.models.School;
import xyz.thinkglobal.models.StudyClass;
import xyz.thinkglobal.repositories.SchoolRepository;

@Component
public class StudyClassMapper extends ConfigurableMapper {

    private SchoolRepository schoolRepository;

    @Autowired
    public StudyClassMapper(SchoolRepository schoolRepository) {
        this.schoolRepository = schoolRepository;
    }

    @Override
    protected void configure(MapperFactory factory) {
        factory.classMap(StudyClassDto.class, StudyClass.class)
                .exclude("school")
                .exclude("studyGroups")
                .customize(new CustomMapper<StudyClassDto, StudyClass>() {
                    @Override
                    public void mapAtoB(StudyClassDto studyClassDto, StudyClass studyClass, MappingContext context) {
                        super.mapAtoB(studyClassDto, studyClass, context);
                        School school = schoolRepository.getOne(studyClassDto.getSchool());
                        studyClass.setSchool(school);
                    }

                    @Override
                    public void mapBtoA(StudyClass studyClass, StudyClassDto studyClassDto, MappingContext context) {
                        super.mapBtoA(studyClass, studyClassDto, context);
                        studyClassDto.setSchool(studyClass.getSchool().getId());
                    }
                })
                .byDefault()
                .register();
    }
}
