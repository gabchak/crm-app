package xyz.thinkglobal.util;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import java.io.IOException;
import java.util.Map;

import static xyz.thinkglobal.constatns.TemplatePaths.EMAIL_TEMPLATE_BASE_PACKAGE;

@Component
public class TemplateUtil {

    @Autowired
    private Configuration freemarkerConfig;


    /**
     *
     * @param clazz
     * @param template
     * @param model
     * @return
     * @throws IOException
     * @throws TemplateException
     */
    public String getHtmlMessage(Class clazz, String template, Map<String, Object> model)
            throws IOException, TemplateException {

        freemarkerConfig.setClassForTemplateLoading(clazz, EMAIL_TEMPLATE_BASE_PACKAGE);
        Template t = freemarkerConfig.getTemplate(template);
        return FreeMarkerTemplateUtils.processTemplateIntoString(t, model);
    }
}
