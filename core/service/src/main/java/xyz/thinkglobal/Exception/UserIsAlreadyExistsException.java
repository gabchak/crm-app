package xyz.thinkglobal.Exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class UserIsAlreadyExistsException extends Throwable {
    public UserIsAlreadyExistsException(String message) {
        super(message);
    }

}
