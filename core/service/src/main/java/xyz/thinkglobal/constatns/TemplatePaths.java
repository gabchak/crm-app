package xyz.thinkglobal.constatns;

public final class TemplatePaths {
    private TemplatePaths() {}

    public static final String EMAIL_TEMPLATE_BASE_PACKAGE = "/template/email/";
    public static final String T_LEAD_REGISTRATION_LINK = "lead-registration-for-event-link.ftl";
    public static final String T_LEAD_CONFIRM_EVENT_REGISTRATION = "lead-confirm-event-registration.ftl";
    public static final String T_LEAD_TO_CONTINUE_EVENT = "registration-invite-to-continue-event.ftl";
    public static final String T_LEAD_MISSED_EVENT = "lead-missed-event.ftl";
    public static final String T_CONTACT_TO_CONTINUE_EVENT = "contact-to-continue-event.ftl";
}
