package xyz.thinkglobal.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xyz.thinkglobal.dto.EventDto;
import xyz.thinkglobal.dto.enums.EventStatus;
import xyz.thinkglobal.dto.mappers.EventMapper;
import xyz.thinkglobal.models.Event;
import xyz.thinkglobal.models.Speaker;
import xyz.thinkglobal.repositories.EventRepository;
import xyz.thinkglobal.services.EventService;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class EventServiceImpl implements EventService {

    private EventRepository eventRepository;
    private EventMapper eventMapper;

    @Autowired
    public EventServiceImpl(EventRepository eventRepository,
                            EventMapper eventMapper) {
        this.eventRepository = eventRepository;
        this.eventMapper = eventMapper;
    }

    @Override
    @Transactional
    public Optional<EventDto> findById(Long id) {
        return eventRepository.findById(id)
                .flatMap(this::getEventWithAllFields);
    }

    @Override
    public List<EventDto> findAll() {
        return eventMapper.mapAsList(eventRepository.findAll(), EventDto.class);
    }

    @Override
    public List<EventDto> findAllByStatusAndStartDateBetween(EventStatus status, LocalDateTime fromTime, LocalDateTime toTime) {
        List<Event> events = eventRepository
                .findAllByStatusAndStartDateBetween(status.toString(), fromTime, toTime);
        return eventMapper.mapAsList(events, EventDto.class);
    }

    @Override
    public LocalDateTime findLastNotVisitedEventDate(Long leadId, String leadStatus) {
        return eventRepository.findLastNotVisitedEventDate(leadId, leadStatus);
    }

    @Override
    @Transactional
    public EventDto save(EventDto eventDto) {
        Event event = eventMapper.map(eventDto, Event.class);
        return eventMapper.map(eventRepository.save(event), EventDto.class);
    }

    @Override
    public void deleteById(Long id) {
        eventRepository.deleteById(id);
    }

    @Override
    public boolean validateEvent(EventDto eventDto) {


        //TODO mechanism for event validation
        return false;
    }

    @Override
    public List<EventDto> findAllActive() {
        List<Event> activeEvents = eventRepository.findAllByStartDateGreaterThan(LocalDateTime.now());
        return eventMapper.mapAsList(activeEvents, EventDto.class);
    }

    private Optional<EventDto> getEventWithAllFields(Event event) {
        EventDto eventDto = eventMapper.map(event, EventDto.class);
        if (!event.getSpeakers().isEmpty()) {
            Set<Long> speakers = new HashSet<>();
            for (Speaker speaker : event.getSpeakers()) {
                speakers.add(speaker.getId());
            }
            eventDto.setSpeakers(speakers);
        }
//        if (!event.getLeadEventSet().isEmpty()) {
//            eventDto.setLeadToEventSet(leadEventMapper.mapAsSet(event.getLeadEventSet(), LeadEventDto.class));
//        }
        return Optional.of(eventDto);
    }
}
