package xyz.thinkglobal.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xyz.thinkglobal.dto.SpeakerDto;
import xyz.thinkglobal.dto.mappers.SpeakerMapper;
import xyz.thinkglobal.models.Event;
import xyz.thinkglobal.models.Speaker;
import xyz.thinkglobal.repositories.SpeakerRepository;
import xyz.thinkglobal.services.SpeakerService;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class SpeakerServiceImpl implements SpeakerService {

    private SpeakerRepository speakerRepository;
    private SpeakerMapper speakerMapper;

    @Autowired
    public SpeakerServiceImpl(SpeakerRepository speakerRepository, SpeakerMapper speakerMapper) {
        this.speakerRepository = speakerRepository;
        this.speakerMapper = speakerMapper;
    }

    @Override
    public Optional<SpeakerDto> findById(Long id) {
        return speakerRepository.findById(id)
                .flatMap(this::getSpeakerWithAllFields);
    }

    @Override
    public List<SpeakerDto> findAll() {
        return speakerMapper.mapAsList(speakerRepository.findAll(), SpeakerDto.class);
    }

    @Override
    @Transactional
    public SpeakerDto save(SpeakerDto speakerDto) {
        Speaker speaker = speakerMapper.map(speakerDto, Speaker.class);
        return speakerMapper.map(speakerRepository.save(speaker), SpeakerDto.class);
    }

    @Override
    public void deleteById(Long id) {
        speakerRepository.deleteById(id);
    }

    private Optional<SpeakerDto> getSpeakerWithAllFields(Speaker speaker) {
        SpeakerDto speakerDto = speakerMapper.map(speaker, SpeakerDto.class);
        if (!speaker.getEvents().isEmpty()) {
            Set<Long> events = new HashSet<>();
            for (Event event : speaker.getEvents()) {
                events.add(event.getId());
            }
            speakerDto.setEvents(events);
        }
        return Optional.of(speakerDto);
    }
}
