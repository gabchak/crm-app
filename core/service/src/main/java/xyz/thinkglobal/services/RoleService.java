package xyz.thinkglobal.services;

import xyz.thinkglobal.models.Role;

public interface RoleService {

    Role findByName(String name);
}
