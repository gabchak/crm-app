package xyz.thinkglobal.services;

import xyz.thinkglobal.dto.StudyGroupDto;

import java.util.List;
import java.util.Optional;

public interface StudyGroupService {
    List<StudyGroupDto> findAllBySchoolId(Long id);

    Optional<StudyGroupDto> findById(Long id);

    StudyGroupDto save(StudyGroupDto studyGroupDto);

    void deleteById(Long id);

    StudyGroupDto create(StudyGroupDto studyGroupDto);
}
