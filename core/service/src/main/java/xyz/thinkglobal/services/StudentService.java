package xyz.thinkglobal.services;

import xyz.thinkglobal.dto.StudentDto;

import java.util.List;
import java.util.Optional;

public interface StudentService {

    Optional<StudentDto> findById(Long id);

    List<StudentDto> findAll();

    Optional<StudentDto> findByBirthCertificate(String birthCertificate);

    StudentDto save(StudentDto student);

    void deleteById(Long id);

    List<StudentDto> findAllByContactId(Long id);
}
