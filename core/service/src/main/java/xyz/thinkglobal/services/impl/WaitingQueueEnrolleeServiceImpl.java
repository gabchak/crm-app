package xyz.thinkglobal.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xyz.thinkglobal.dto.WaitingQueueEnrolleeDto;
import xyz.thinkglobal.dto.mappers.WaitingQueueEnrolleeMapper;
import xyz.thinkglobal.models.WaitingQueueEnrollee;
import xyz.thinkglobal.repositories.WaitingQueueEnrolleeRepository;
import xyz.thinkglobal.services.WaitingQueueEnrolleeService;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class WaitingQueueEnrolleeServiceImpl implements WaitingQueueEnrolleeService {

    private WaitingQueueEnrolleeRepository waitingQueueEnrolleeRepository;
    private WaitingQueueEnrolleeMapper waitingQueueEnrolleeMapper;

    @Autowired
    public WaitingQueueEnrolleeServiceImpl(WaitingQueueEnrolleeRepository waitingQueueEnrolleeRepository,
                                           WaitingQueueEnrolleeMapper waitingQueueEnrolleeMapper) {
        this.waitingQueueEnrolleeRepository = waitingQueueEnrolleeRepository;
        this.waitingQueueEnrolleeMapper = waitingQueueEnrolleeMapper;
    }

    @Override
    @Transactional
    public WaitingQueueEnrolleeDto save(WaitingQueueEnrolleeDto waitingQueueEnrolleeDto) {
        WaitingQueueEnrollee waitingQueueEnrollee = waitingQueueEnrolleeMapper
                .map(waitingQueueEnrolleeDto, WaitingQueueEnrollee.class);

        WaitingQueueEnrollee saved = waitingQueueEnrolleeRepository
                .save(waitingQueueEnrollee);
        return waitingQueueEnrolleeMapper.map(saved, WaitingQueueEnrolleeDto.class);
    }

    @Override
    public void deleteById(Long id) {
        waitingQueueEnrolleeRepository.deleteById(id);
    }

    @Override
    public List<WaitingQueueEnrolleeDto> findAllByWaitingQueueId(Long waitingQueueId) {
        List<WaitingQueueEnrollee> waitingQueueEnrolleeList = waitingQueueEnrolleeRepository.findAllByWaitingQueue_Id(waitingQueueId);
        return waitingQueueEnrolleeMapper.mapAsList(waitingQueueEnrolleeList, WaitingQueueEnrolleeDto.class);
    }
}
