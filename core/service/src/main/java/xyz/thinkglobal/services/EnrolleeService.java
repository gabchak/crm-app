package xyz.thinkglobal.services;

import xyz.thinkglobal.dto.EnrolleeDto;

import java.util.List;
import java.util.Optional;

public interface EnrolleeService {

    List<EnrolleeDto> findAllByContactId(Long id);

    Optional<EnrolleeDto> findById(Long id);

    List<EnrolleeDto> findAll();

    EnrolleeDto save(EnrolleeDto enrolleeDto);

    void deleteById(Long id);
}
