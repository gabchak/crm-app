package xyz.thinkglobal.services;

import xyz.thinkglobal.dto.AddressDto;

import java.util.Optional;

public interface AddressService {
    Optional<AddressDto> findById(Long id);

    AddressDto save(AddressDto addressDto);

    void deleteById(Long id);
}
