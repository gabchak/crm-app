package xyz.thinkglobal.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import xyz.thinkglobal.models.User;
import xyz.thinkglobal.models.Role;
import xyz.thinkglobal.repositories.PersonRepository;
import xyz.thinkglobal.repositories.UserRepository;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * The implementation class for
 * {@link org.springframework.security.core.userdetails.UserDetailsService} interface.
 */
@Service
@Primary
public class UserDetailsServiceImpl implements UserDetailsService {

    /**
     * The person repository.
     */
    private UserRepository<User> userRepository;

    /**
     * Instantiates a new user details service impl.
     */
    @Autowired
    public UserDetailsServiceImpl(UserRepository<User> userRepository) {
        super();
        this.userRepository = userRepository;
    }

    /* (non-Javadoc)
     * @see org.springframework.security.core.userdetails.UserDetailsService#loadUserByUsername(java.lang.String)
     */
    @Override
    @Transactional
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        org.springframework.security.core.userdetails.User userDetails = null;

        Optional<User> optionalPerson = userRepository.findByEmail(email);
        User user = null;
        if (optionalPerson.isPresent()) {
            user = optionalPerson.get();
        }

        if (user != null) {
            // defines roles for the user
            Set<GrantedAuthority> roles = new HashSet<>();
            Set<Role> userRoles = new HashSet<>(user.getPositions().size());
            user.getPositions().forEach(position -> userRoles.add(position.getRole()));
            for (Role role : userRoles) {
                roles.add(new SimpleGrantedAuthority(role.getRole()));
            }

            // create UserDetails instance
            userDetails = new org.springframework.security.core.userdetails.User(email, user.getPassword(), roles);
        }
        if (userDetails == null) {
            throw new UsernameNotFoundException(email);
        }
        return userDetails;
    }

}
