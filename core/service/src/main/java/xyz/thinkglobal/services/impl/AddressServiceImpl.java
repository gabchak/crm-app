package xyz.thinkglobal.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xyz.thinkglobal.dto.AddressDto;
import xyz.thinkglobal.dto.mappers.AddressMapper;
import xyz.thinkglobal.models.Address;
import xyz.thinkglobal.repositories.AddressRepository;
import xyz.thinkglobal.services.AddressService;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class AddressServiceImpl implements AddressService {

    private AddressRepository addressRepository;
    private AddressMapper addressMapper;

    @Autowired
    public AddressServiceImpl(AddressRepository addressRepository, AddressMapper addressMapper) {
        this.addressRepository = addressRepository;
        this.addressMapper = addressMapper;
    }

    @Override
    public Optional<AddressDto> findById(Long id) {
        return addressRepository.findById(id)
                .map(address -> addressMapper.map(address, AddressDto.class));
    }

    @Override
    @Transactional
    public AddressDto save(AddressDto addressDto) {
        Address address = addressMapper.map(addressDto, Address.class);
        Address saved = addressRepository.save(address);
        return addressMapper.map(saved, AddressDto.class);
    }

    @Override
    public void deleteById(Long id) {
        addressRepository.deleteById(id);
    }
}
