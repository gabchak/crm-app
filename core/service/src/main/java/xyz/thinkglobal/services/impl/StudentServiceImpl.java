package xyz.thinkglobal.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xyz.thinkglobal.dto.StudentDto;
import xyz.thinkglobal.dto.mappers.StudentMapper;
import xyz.thinkglobal.models.Contact;
import xyz.thinkglobal.models.Student;
import xyz.thinkglobal.repositories.ContactRepository;
import xyz.thinkglobal.repositories.StudentRepository;
import xyz.thinkglobal.services.StudentService;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class StudentServiceImpl implements StudentService {

    private StudentRepository studentRepository;
    private StudentMapper studentMapper;
    private ContactRepository contactRepository;

    @Autowired
    public StudentServiceImpl(StudentRepository studentRepository, StudentMapper studentMapper, ContactRepository contactRepository) {
        this.studentRepository = studentRepository;
        this.studentMapper = studentMapper;
        this.contactRepository = contactRepository;
    }

    @Override
    public Optional<StudentDto> findById(Long id) {
        return studentRepository.findById(id)
                .map(this::getStudentDtoWithAllFields);
    }

    @Override
    public List<StudentDto> findAll() {
        return studentRepository.findAll()
                .stream()
                .map(student -> studentMapper.map(student, StudentDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public Optional<StudentDto> findByBirthCertificate(String birthCertificate) {
        return studentRepository.findByBirthCertificate(birthCertificate)
                .map(student -> studentMapper.map(student, StudentDto.class));
    }

    @Override
    @Transactional
    public StudentDto save(StudentDto studentDto) {
        Student student = getStudentWithAllFields(studentDto);
        Student savedStudent = studentRepository.save(student);
        return getStudentDtoWithAllFields(savedStudent);
    }

    @Override
    public void deleteById(Long id) {
        studentRepository.deleteById(id);
    }

    @Override
    public List<StudentDto> findAllByContactId(Long id) {
        return studentMapper.mapAsList(
                studentRepository.findAllByFamilyRepresentativeSet_Id(id),
                StudentDto.class);
    }

    private StudentDto getStudentDtoWithAllFields(Student student) {
        StudentDto studentDto = studentMapper.map(student, StudentDto.class);
        if (!student.getFamilyRepresentativeSet().isEmpty()) {
            Set<Long> studentSet = new HashSet<>();
            for (Contact contact : student.getFamilyRepresentativeSet()) {
                studentSet.add(contact.getId());
            }
            studentDto.setFamilyRepresentativeSet(studentSet);
        }
        return studentDto;
    }

    private Student getStudentWithAllFields(StudentDto studentDto) {
        Student student = studentMapper.map(studentDto, Student.class);
        if (!studentDto.getFamilyRepresentativeSet().isEmpty()) {
            Set<Contact> contactSet = contactRepository.findAllByIdIn(studentDto.getFamilyRepresentativeSet());
            student.setFamilyRepresentativeSet(contactSet);
        }
        return student;
    }
}
