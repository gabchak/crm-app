package xyz.thinkglobal.services;

import xyz.thinkglobal.dto.UserDto;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface UserService {
    List<UserDto> findAll();

    Optional<UserDto> findById(Long id);

    UserDto save(UserDto userDto);

    void deleteById(Long id);
}
