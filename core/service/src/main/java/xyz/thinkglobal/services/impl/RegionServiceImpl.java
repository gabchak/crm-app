package xyz.thinkglobal.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xyz.thinkglobal.dto.RegionDto;
import xyz.thinkglobal.dto.mappers.RegionMapper;
import xyz.thinkglobal.repositories.RegionRepository;
import xyz.thinkglobal.services.RegionService;

import java.util.List;
import java.util.Optional;

@Service
public class RegionServiceImpl implements RegionService {

    private RegionRepository regionRepository;
    private RegionMapper regionMapper;

    @Autowired
    public RegionServiceImpl(RegionRepository regionRepository, RegionMapper regionMapper) {
        this.regionRepository = regionRepository;
        this.regionMapper = regionMapper;
    }


    @Override
    public List<RegionDto> findAll() {
        return regionMapper.mapAsList(regionRepository.findAll(), RegionDto.class);
    }

    @Override
    public Optional<RegionDto> findById(Long id) {
        return regionRepository.findById(id).map(region -> regionMapper.map(region, RegionDto.class));
    }
}
