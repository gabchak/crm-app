package xyz.thinkglobal.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xyz.thinkglobal.dto.CityDto;
import xyz.thinkglobal.dto.mappers.CityMapper;
import xyz.thinkglobal.repositories.CityRepository;
import xyz.thinkglobal.services.CityService;

import java.util.List;
import java.util.Optional;

@Service
public class CityServiceImpl implements CityService {

    private CityRepository cityRepository;
    private CityMapper cityMapper;

    @Autowired
    public CityServiceImpl(CityRepository cityRepository, CityMapper cityMapper) {
        this.cityRepository = cityRepository;
        this.cityMapper = cityMapper;
    }

    @Override
    public List<CityDto> findAllByRegionId(Long id) {
        return cityMapper.mapAsList(cityRepository.findAllByRegion_Id(id), CityDto.class);
    }

    @Override
    public Optional<CityDto> findById(Long id) {
        return cityRepository.findById(id).map(city -> cityMapper.map(city, CityDto.class));
    }
}
