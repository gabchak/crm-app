package xyz.thinkglobal.services.pdf.impl;

import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xyz.thinkglobal.services.pdf.PdfGeneratorService;
import xyz.thinkglobal.util.TemplateUtil;

import java.io.ByteArrayInputStream;
import java.io.StringWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Map;
import java.util.logging.Logger;

@Service
public class PdfGeneratorServiceImpl implements PdfGeneratorService {


    @Autowired
    private TemplateUtil templateUtil;


    /** This method works only for Ubuntu OS with wkhtml preinstalled
     *
     * @param map
     * @param templateName
     * @return
     * @throws IOException
     * @throws TemplateException
     * @throws InterruptedException
     */
    @Override
    public synchronized File getPdfFileFromTemplate(Map<String, Object> map, String templateName)
            throws IOException, TemplateException, InterruptedException {

        Logger log = Logger.getLogger("thinkglobal.pdf.wkhtml");
        String htmlText = templateUtil.getHtmlMessage(this.getClass(), templateName, map);
//        OutputStream outputStream = new FileOutputStream("file.pdf");
        File destinationFile = File.createTempFile("contract", ".pdf");
        Process wkhtml; // Create uninitialized process

        final String[] command = {
            "/usr/bin/xvfb-run",
            "-a",
            "--server-args=-screen 0, 1024x768x24",
            "wkhtmltopdf", "-q",
            "-L", "8mm", "-R", "8mm", "-T", "7mm", "-B", "7mm",
            "-", destinationFile.getPath(),
        };

        wkhtml = Runtime.getRuntime().exec(command); // Start process

        try {
            wkhtml.getOutputStream().write(htmlText.getBytes());
            wkhtml.getOutputStream().flush();
            wkhtml.getOutputStream().close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        for(;;) {
            try {
                // TODO(pc) add timeout
                final int wkstatus = wkhtml.waitFor();
                if(wkstatus != 0) {
                    log.severe("Error running wkhtml, code: " + wkstatus);
                    // TODO(pc) make more specific exception
                    throw new RuntimeException(
                        "Process wkthmltopdf exited with bad status code: " +
                        wkstatus);
                }
            } catch (InterruptedException e) {
                continue;
            }
            break;
        }
        // TODO(pc) read wkhtmltopdf's stderr

        return destinationFile;
    }

}
