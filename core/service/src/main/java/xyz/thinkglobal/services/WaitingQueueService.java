package xyz.thinkglobal.services;

import xyz.thinkglobal.dto.WaitingQueueDto;

import java.util.List;
import java.util.Optional;

public interface WaitingQueueService {

    Optional<WaitingQueueDto> findById(Long waitingQueueId);

    WaitingQueueDto save(WaitingQueueDto waitingQueueDto);

    Optional<WaitingQueueDto> findByClassId(Long classId);

    List<WaitingQueueDto> findAllBySchoolId(Long schoolId);

    List<WaitingQueueDto> findAll();

    List<WaitingQueueDto> findAllByPositionId(Long positionId) throws IllegalAccessException;

    boolean hasAccessToStudyClass(Long positionId, Long studyClassId) throws IllegalAccessException;

    boolean hasAccessToWaitingQueue(Long positionId, Long waitingQueueId) throws IllegalAccessException;
}
