package xyz.thinkglobal.services;

import org.springframework.data.domain.Pageable;
import xyz.thinkglobal.dto.ChildDto;
import xyz.thinkglobal.dto.StudyClassDto;
import xyz.thinkglobal.dto.filters.ChildrenFilterDto;

import java.util.List;
import java.util.Optional;

public interface StudyClassService {
    List<StudyClassDto> findAllBySchoolId(Long id);

    Optional<StudyClassDto> findById(Long id);

    StudyClassDto save(StudyClassDto studyClassDto);

    void deleteById(Long id);

    List<StudyClassDto> findAllBySchoolIdAndWaitingQueueExists(Long id);

    List<ChildDto> findChildrenByPositionIn(ChildrenFilterDto childrenFilterDto, Pageable pageable);
}
