package xyz.thinkglobal.services;

import xyz.thinkglobal.dto.WaitingQueueEnrolleeDto;

import java.util.List;

public interface WaitingQueueEnrolleeService {

    WaitingQueueEnrolleeDto save(WaitingQueueEnrolleeDto waitingQueueEnrolleeDto);

    void deleteById(Long id);

    List<WaitingQueueEnrolleeDto> findAllByWaitingQueueId(Long waitingQueueId);
}
