package xyz.thinkglobal.services;

import xyz.thinkglobal.dto.CityDto;

import java.util.List;
import java.util.Optional;

public interface CityService {
    List<CityDto> findAllByRegionId(Long id);

    Optional<CityDto> findById(Long id);
}
