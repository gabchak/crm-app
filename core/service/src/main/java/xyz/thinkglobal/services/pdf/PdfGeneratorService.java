package xyz.thinkglobal.services.pdf;

import freemarker.template.TemplateException;

import java.io.File;
import java.io.IOException;
import java.util.Map;

public interface PdfGeneratorService {

    File getPdfFileFromTemplate(Map<String, Object> map, String templateName) throws IOException, TemplateException, InterruptedException;
}
