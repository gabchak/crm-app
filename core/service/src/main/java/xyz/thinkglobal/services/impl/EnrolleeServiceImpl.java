package xyz.thinkglobal.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xyz.thinkglobal.dto.EnrolleeDto;
import xyz.thinkglobal.dto.mappers.EnrolleeMapper;
import xyz.thinkglobal.models.Contact;
import xyz.thinkglobal.models.Enrollee;
import xyz.thinkglobal.models.WaitingQueueEnrollee;
import xyz.thinkglobal.repositories.ContactRepository;
import xyz.thinkglobal.repositories.EnrolleeRepository;
import xyz.thinkglobal.repositories.WaitingQueueEnrolleeRepository;
import xyz.thinkglobal.services.EnrolleeService;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class EnrolleeServiceImpl implements EnrolleeService {

    private EnrolleeRepository enrolleeRepository;
    private EnrolleeMapper enrolleeMapper;
    private WaitingQueueEnrolleeRepository waitingQueueEnrolleeRepository;
    private ContactRepository contactRepository;

    @Autowired
    public EnrolleeServiceImpl(EnrolleeRepository enrolleeRepository, EnrolleeMapper enrolleeMapper, WaitingQueueEnrolleeRepository waitingQueueEnrolleeRepository, ContactRepository contactRepository) {
        this.enrolleeRepository = enrolleeRepository;
        this.enrolleeMapper = enrolleeMapper;
        this.waitingQueueEnrolleeRepository = waitingQueueEnrolleeRepository;
        this.contactRepository = contactRepository;
    }

    @Override
    public List<EnrolleeDto> findAllByContactId(Long id) {
        List<Enrollee> allByFamilyRepresentativeSet_id = enrolleeRepository.findAllByFamilyRepresentativeSet_Id(id);
        return enrolleeMapper.mapAsList(allByFamilyRepresentativeSet_id,
                EnrolleeDto.class);
    }

    @Override
    public Optional<EnrolleeDto> findById(Long id) {
        return enrolleeRepository.findById(id)
                .map(this::getEnrolleeDtoWithAllFields);
    }

    @Override
    public List<EnrolleeDto> findAll() {
        return enrolleeMapper.mapAsList(enrolleeRepository.findAll(), EnrolleeDto.class);
    }

    @Override
    @Transactional
    public EnrolleeDto save(EnrolleeDto enrolleeDto) {
        Enrollee enrollee = getEnrolleeWithAllFields(enrolleeDto);
        Enrollee savedEnrollee = enrolleeRepository.save(enrollee);
        return getEnrolleeDtoWithAllFields(savedEnrollee);
    }

    @Override
    public void deleteById(Long id) {
        enrolleeRepository.deleteById(id);
    }

    private EnrolleeDto getEnrolleeDtoWithAllFields(Enrollee enrollee) {
        EnrolleeDto enrolleeDto = enrolleeMapper.map(enrollee, EnrolleeDto.class);
        if (enrollee.getFamilyRepresentativeSet() != null) {
            Set<Long> enrolleeSet = new HashSet<>();
            for (Contact contact : enrollee.getFamilyRepresentativeSet()) {
                enrolleeSet.add(contact.getId());
            }
            enrolleeDto.setFamilyRepresentativeSet(enrolleeSet);
        }
        if (enrollee.getWaitingQueueEnrolleeSet() != null) {
            Set<Long> waitingQueueEnrolleeSet = new HashSet<>();
            for (WaitingQueueEnrollee waitingQueueEnrollee : enrollee.getWaitingQueueEnrolleeSet()) {
                waitingQueueEnrolleeSet.add(waitingQueueEnrollee.getId());
            }
            enrolleeDto.setWaitingQueueEnrolleeSet(waitingQueueEnrolleeSet);
        }
        return enrolleeDto;
    }

    private Enrollee getEnrolleeWithAllFields(EnrolleeDto enrolleeDto) {
        Enrollee enrollee = enrolleeMapper.map(enrolleeDto, Enrollee.class);
        if (enrolleeDto.getWaitingQueueEnrolleeSet() != null) {
            Set<WaitingQueueEnrollee> waitingQueueEnrolleeSet = waitingQueueEnrolleeRepository
                    .findAllByIdIn(enrolleeDto.getWaitingQueueEnrolleeSet());
            enrollee.setWaitingQueueEnrolleeSet(waitingQueueEnrolleeSet);
        }
        if (enrolleeDto.getFamilyRepresentativeSet() != null) {
            Set<Contact> contactSet = contactRepository.findAllByIdIn(enrolleeDto.getFamilyRepresentativeSet());
            enrollee.setFamilyRepresentativeSet(contactSet);
        }
        return enrollee;
    }
}
