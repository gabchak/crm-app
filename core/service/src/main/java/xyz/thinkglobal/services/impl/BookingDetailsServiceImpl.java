package xyz.thinkglobal.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import xyz.thinkglobal.dto.BookingDetailsDto;
import xyz.thinkglobal.dto.enums.WaitingQueueEnrolleeStatus;
import xyz.thinkglobal.dto.mappers.BookingDetailsMapper;
import xyz.thinkglobal.models.Booking;
import xyz.thinkglobal.models.BookingDetails;
import xyz.thinkglobal.models.Enrollee;
import xyz.thinkglobal.models.WaitingQueue;
import xyz.thinkglobal.models.WaitingQueueEnrollee;
import xyz.thinkglobal.repositories.BookingDetailsRepository;
import xyz.thinkglobal.repositories.BookingRepository;
import xyz.thinkglobal.repositories.WaitingQueueEnrolleeRepository;
import xyz.thinkglobal.services.BookingDetailsService;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * The type Booking details service.
 */
@Service
public class BookingDetailsServiceImpl implements BookingDetailsService {

    private BookingDetailsRepository bookingDetailsRepository;
    private BookingDetailsMapper bookingDetailsMapper;

    /**
     * Instantiates a new Booking details service.
     *
     * @param bookingDetailsRepository       the booking details repository
     * @param bookingDetailsMapper           the booking details mapper
     */
    @Autowired
    public BookingDetailsServiceImpl(BookingDetailsRepository bookingDetailsRepository,
                                     BookingDetailsMapper bookingDetailsMapper) {
        this.bookingDetailsRepository = bookingDetailsRepository;
        this.bookingDetailsMapper = bookingDetailsMapper;
    }

    @Override
    public BookingDetailsDto save(BookingDetailsDto bookingDetailsDto) {
        BookingDetails bookingDetails = bookingDetailsMapper.map(bookingDetailsDto, BookingDetails.class);
        BookingDetails savedBookingDetails = bookingDetailsRepository.save(bookingDetails);
        return bookingDetailsMapper.map(savedBookingDetails, BookingDetailsDto.class);
    }

    @Override
    public Optional<BookingDetailsDto> findById(Long id) {
        return bookingDetailsRepository.findById(id)
                .map(bookingDetails -> bookingDetailsMapper
                        .map(bookingDetails, BookingDetailsDto.class));
    }

    @Override
    public List<BookingDetailsDto> findAll() {
        return bookingDetailsMapper
                .mapAsList(bookingDetailsRepository.findAll(), BookingDetailsDto.class);
    }

    @Override
    public void deleteById(Long id) {
        bookingDetailsRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void deleteByEnrolleeId(Long enrolleeId) {
        bookingDetailsRepository.deleteByEnrollee_Id(enrolleeId);
    }
}
