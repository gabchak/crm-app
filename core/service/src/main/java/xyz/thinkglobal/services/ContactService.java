package xyz.thinkglobal.services;

import xyz.thinkglobal.dto.ContactDto;

import java.util.List;
import java.util.Optional;

public interface ContactService {

    List<ContactDto> findAll();

    Optional<ContactDto> findById(Long id);

    Optional<ContactDto> findByEmail(String email);

    ContactDto save(ContactDto contactDto);

    void deleteById(Long id);
}
