package xyz.thinkglobal.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xyz.thinkglobal.dto.PositionDto;
import xyz.thinkglobal.dto.mappers.PositionMapper;
import xyz.thinkglobal.models.Position;
import xyz.thinkglobal.repositories.PositionRepository;
import xyz.thinkglobal.services.PositionService;

import java.util.List;
import java.util.Set;

@Service
public class PositionServiceImpl implements PositionService {

    private PositionRepository positionRepository;
    private PositionMapper positionMapper;

    @Autowired
    public PositionServiceImpl(PositionRepository positionRepository, PositionMapper positionMapper) {
        this.positionRepository = positionRepository;
        this.positionMapper = positionMapper;
    }


    @Override
    public Set<PositionDto> getAllByUserEmail(String email) {
        return positionMapper.mapAsSet(positionRepository.findAllByUser_Email(email), PositionDto.class);
    }

    @Override
    public PositionDto getById(Long id) {
        Position position = positionRepository.getOne(id);
        return position != null ? positionMapper.map(position, PositionDto.class) : null;
    }

    @Override
    public void deleteById(Long id) {
        positionRepository.deleteById(id);
    }

    /** This method have to return all positions where userId and position is the same as position with the positionId.
     *
     * @param positionId
     * @return
     */
    @Override
    public List<PositionDto> getAllRelatedPositionsById(Long positionId) {
        List<Position> positionList = positionRepository.findAllRelatedByPositionId(positionId);
        return positionMapper.mapAsList(positionList, PositionDto.class);
    }
}
