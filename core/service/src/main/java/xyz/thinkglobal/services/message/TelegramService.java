package xyz.thinkglobal.services.message;

public interface TelegramService {

    void sendMessage(String phone, String subject, String text);
}
