package xyz.thinkglobal.services.impl;

import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import xyz.thinkglobal.Exception.EventDoesNotExist;
import xyz.thinkglobal.Exception.EventFinishedException;
import xyz.thinkglobal.Exception.LeadNotExistException;
import xyz.thinkglobal.dto.LeadDto;
import xyz.thinkglobal.dto.LeadEventDto;
import xyz.thinkglobal.dto.enums.LeadEventStatus;
import xyz.thinkglobal.dto.enums.LeadStatus;
import xyz.thinkglobal.dto.mappers.LeadEventMapper;
import xyz.thinkglobal.dto.mappers.LeadMapper;
import xyz.thinkglobal.messaging.EmailService;
import xyz.thinkglobal.models.Event;
import xyz.thinkglobal.models.Lead;
import xyz.thinkglobal.models.LeadEvent;
import xyz.thinkglobal.repositories.*;
import xyz.thinkglobal.services.LeadEventService;
import xyz.thinkglobal.util.TemplateUtil;

import javax.mail.MessagingException;
import javax.transaction.Transactional;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static xyz.thinkglobal.constatns.TemplatePaths.T_LEAD_CONFIRM_EVENT_REGISTRATION;

@Service
public class LeadEventServiceImpl implements LeadEventService {

    private final LeadEventRepository leadEventRepository;
    private final LeadRepository leadRepository;
    private final EventRepository eventRepository;
    private final LeadMapper leadMapper;
    private final EmailService emailService;
    private final TemplateUtil templateUtil;
    private final Environment environment;

    @Autowired
    public LeadEventServiceImpl(LeadEventRepository leadEventRepository,
                                LeadRepository leadRepository,
                                EventRepository eventRepository,
                                LeadMapper leadMapper,
                                EmailService emailService,
                                TemplateUtil templateUtil,
                                Environment environment) {
        this.leadEventRepository = leadEventRepository;
        this.leadRepository = leadRepository;
        this.eventRepository = eventRepository;
        this.leadMapper = leadMapper;
        this.emailService = emailService;
        this.templateUtil = templateUtil;
        this.environment = environment;
    }


    @Override
    @Transactional
    public LeadDto bindLeadToEvent(Long leadId, Long eventId)
            throws LeadNotExistException, EventDoesNotExist, EventFinishedException {
        Optional<Lead> optionalLead = leadRepository.findById(leadId);
        if (!optionalLead.isPresent()) {
            throw new LeadNotExistException();
        }
        LeadEvent leadEvent = bindLeadToEvent(optionalLead.get(), eventId);
        return leadMapper.map(leadEvent.getLead(), LeadDto.class);
    }

    @Override
    @Transactional
    public LeadDto registerLead(LeadDto leadDto, Long eventId)
            throws MessagingException, IOException, TemplateException,
            EventDoesNotExist, EventFinishedException {

        Lead lead = registerLead(leadDto);
        LeadEvent leadEvent = bindLeadToEvent(lead, eventId);
        sendEmailConfirmRegistrationForEvent(leadEvent);

        return leadMapper.map(lead, LeadDto.class);
    }

    private LeadEvent bindLeadToEvent(Lead lead, Long eventId)
            throws EventDoesNotExist, EventFinishedException {

        Optional<Event> optionalEvent = eventRepository.findById(eventId);

        if (!optionalEvent.isPresent()) {
            throw new EventDoesNotExist();
        }

        if (ChronoUnit.MINUTES.between(LocalDateTime.now(), optionalEvent.get().getStartDate()) < 0) {
            throw new EventFinishedException();
        }

        LeadEvent leadEvent = new LeadEvent();

        lead.setStatus(LeadStatus.IN_PROGRESS.toString());

        leadEvent.setLead(lead);
        leadEvent.setLeadId(lead.getId());
        leadEvent.setEvent(optionalEvent.get());
        leadEvent.setEventId(optionalEvent.get().getId());
        leadEvent.setStatus(LeadEventStatus.REGISTERED.toString());

        return leadEventRepository.save(leadEvent);
    }

    private Lead registerLead(LeadDto leadDto) {
        Lead lead;
        Optional<Lead> optionalLead = leadRepository.findByEmail(leadDto.getEmail());

        if (optionalLead.isPresent()) {
            lead = optionalLead.get();
            updatePhone(lead, leadDto.getPhone());
        } else {
            lead = leadMapper.map(leadDto, Lead.class);
            lead.setStatus(LeadStatus.NEW.toString());
            lead.setHowYouFindUs(leadDto.getHowYouFindUs());
        }
        return leadRepository.save(lead);
    }

    @Override
    @Transactional
    public LeadEventDto save(LeadEventDto leadEventDto) {
        LeadEvent leadEvent = leadMapper.map(leadEventDto, LeadEvent.class);
        LeadEvent savedLTE = leadEventRepository.save(leadEvent);
        return leadMapper.map(savedLTE, LeadEventDto.class);
    }

    @Override
    public Optional<LeadEventDto> findById(Long eventId, Long leadId) {
        LeadEvent.LeadEventId leadEventId = new LeadEvent.LeadEventId(leadId, eventId);
        leadEventId.setEventId(eventId);
        leadEventId.setLeadId(leadId);

        return leadEventRepository.findById(leadEventId).map(leadEvent -> leadMapper.map(leadEvent, LeadEventDto.class));
    }

    private void updatePhone(Lead lead, String phone) {
        String curPhone = lead.getPhone();
        if (!curPhone.equals(phone)) {
            lead.setPhone(phone);
            lead.setAdditionalPhone(curPhone);
        }
    }

    private void sendEmailConfirmRegistrationForEvent(LeadEvent leadEvent)
            throws IOException, MessagingException, TemplateException {

        Map<String, Object> model = new HashMap<>();
        model.put("leadEvent", leadEvent);
        model.put("hotline", environment.getProperty("hotline.phone-number"));
        model.put("facebookLink", environment.getProperty("facebook.group-link"));

        String text = templateUtil.getHtmlMessage(this.getClass(),
                T_LEAD_CONFIRM_EVENT_REGISTRATION, model);

        String subjectTemplate = environment.getProperty("mail.subject.lead-confirm-event-registration");
        String subject = subjectTemplate.replace("event.startDate", leadEvent.getEvent().getFormattedStartDate());
        emailService.sendSimpleMessage(leadEvent.getLead().getEmail(),
                subject, text);
    }
}