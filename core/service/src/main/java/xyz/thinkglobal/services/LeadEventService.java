package xyz.thinkglobal.services;

import freemarker.template.TemplateException;
import xyz.thinkglobal.Exception.EventDoesNotExist;
import xyz.thinkglobal.Exception.EventFinishedException;
import xyz.thinkglobal.Exception.LeadNotExistException;
import xyz.thinkglobal.dto.LeadDto;
import xyz.thinkglobal.dto.LeadEventDto;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.Optional;

public interface LeadEventService {

    LeadEventDto save(LeadEventDto leadEventDto);

    Optional<LeadEventDto> findById(Long eventId, Long leadId);

    LeadDto registerLead(LeadDto leadDto, Long eventId)
            throws MessagingException, IOException, TemplateException,
            EventDoesNotExist, EventFinishedException;

    LeadDto bindLeadToEvent(Long leadId, Long eventId)
            throws LeadNotExistException, EventDoesNotExist, EventFinishedException;
}
