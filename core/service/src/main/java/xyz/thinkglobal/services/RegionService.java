package xyz.thinkglobal.services;

import xyz.thinkglobal.dto.RegionDto;

import java.util.List;
import java.util.Optional;

public interface RegionService {

    List<RegionDto> findAll();

    Optional<RegionDto> findById(Long id);
}
