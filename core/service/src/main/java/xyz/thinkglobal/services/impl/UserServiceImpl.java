package xyz.thinkglobal.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import xyz.thinkglobal.dto.UserDto;
import xyz.thinkglobal.dto.mappers.UserMapper;
import xyz.thinkglobal.models.User;
import xyz.thinkglobal.repositories.RoleRepository;
import xyz.thinkglobal.repositories.UserRepository;
import xyz.thinkglobal.services.UserService;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository<User> userRepository;
    private UserMapper userMapper;

    @Autowired
    public UserServiceImpl(UserRepository<User> userRepository,
                           UserMapper userMapper) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
    }

    @Override
    public List<UserDto> findAll() {
        return userMapper.mapAsList(userRepository.findAll(), UserDto.class);
    }

    @Override
    public Optional<UserDto> findById(Long id) {
        return userRepository.findById(id)
                .map(user -> userMapper.map(user, UserDto.class));
    }

    @Override
    @Transactional
    public UserDto save(UserDto userDto) {
        User user = createUserByDtoAndRepositoryIfExists(userDto);

        User savedUser = userRepository.save(user);
        return userMapper.map(savedUser, UserDto.class);
    }

    private User createUserByDtoAndRepositoryIfExists(UserDto userDto) {
        User user = null;
        Optional<User> optional;
        boolean exists = false;
        if (userDto.getId() != null) {
            optional = userRepository.findById(userDto.getId());
            if (optional.isPresent()) {
                user = optional.get();
                userMapper.map(userDto, user);
                exists = true;
            }
        }
        if (!exists){
            user = userMapper.map(userDto, User.class);
            user.setEmail(userDto.getEmail());
            user.setPassword(new BCryptPasswordEncoder().encode(userDto.getPassword()));
        }
        return user;
    }

    @Override
    public void deleteById(Long id) {
        userRepository.deleteById(id);
    }
}
