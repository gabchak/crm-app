package xyz.thinkglobal.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import xyz.thinkglobal.dto.ChildDto;
import xyz.thinkglobal.dto.StudyClassDto;
import xyz.thinkglobal.dto.filters.ChildrenFilterDto;
import xyz.thinkglobal.dto.mappers.ChildMapper;
import xyz.thinkglobal.dto.mappers.StudyClassMapper;
import xyz.thinkglobal.models.StudyClass;
import xyz.thinkglobal.models.StudyGroup;
import xyz.thinkglobal.repositories.StudyClassRepository;
import xyz.thinkglobal.services.StudyClassService;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class StudyClassServiceImpl implements StudyClassService {

    private StudyClassRepository studyClassRepository;
    private StudyClassMapper studyClassMapper;
    private ChildMapper childMapper;

    @Autowired
    public StudyClassServiceImpl(StudyClassRepository studyClassRepository, StudyClassMapper studyClassMapper) {
        this.studyClassRepository = studyClassRepository;
        this.studyClassMapper = studyClassMapper;
    }

    @Override
    public List<StudyClassDto> findAllBySchoolId(Long id) {
        return studyClassMapper.mapAsList(studyClassRepository.findAllBySchool_Id(id), StudyClassDto.class);
    }

    @Override
    public Optional<StudyClassDto> findById(Long id) {
        return studyClassRepository.findById(id)
                .flatMap(this::getStudyClassWithAllFields);
    }

    @Override
    @Transactional
    public StudyClassDto save(StudyClassDto studyClassDto) {
        StudyClass studyClass = studyClassMapper.map(studyClassDto, StudyClass.class);
        return studyClassMapper.map(studyClassRepository.save(studyClass), StudyClassDto.class);
    }

    @Override
    public void deleteById(Long id) {
        studyClassRepository.deleteById(id);
    }

    @Override
    public List<StudyClassDto> findAllBySchoolIdAndWaitingQueueExists(Long id) {
        return studyClassMapper
                .mapAsList(studyClassRepository.findAllBySchool_IdAndWaitingQueueNotNull(id),
                StudyClassDto.class);
    }

    @Override
    public List<ChildDto> findChildrenByPositionIn(ChildrenFilterDto childrenFilterDto, Pageable pageable) {

        return null;
    }

    private Optional<StudyClassDto> getStudyClassWithAllFields(StudyClass studyClass) {
        StudyClassDto studyClassDto = studyClassMapper.map(studyClass, StudyClassDto.class);
        if (!studyClass.getStudyGroups().isEmpty()) {
            Set<Long> studyGroups = new HashSet<>();
            for (StudyGroup studyGroup : studyClass.getStudyGroups()) {
                studyGroups.add(studyGroup.getId());
            }
            studyClassDto.setStudyGroups(studyGroups);
        }
        return Optional.of(studyClassDto);
    }
}
