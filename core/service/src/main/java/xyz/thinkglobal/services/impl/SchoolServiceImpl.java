package xyz.thinkglobal.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xyz.thinkglobal.dto.SchoolDto;
import xyz.thinkglobal.dto.mappers.SchoolMapper;
import xyz.thinkglobal.models.Event;
import xyz.thinkglobal.models.Position;
import xyz.thinkglobal.models.School;
import xyz.thinkglobal.models.StudyClass;
import xyz.thinkglobal.repositories.SchoolRepository;
import xyz.thinkglobal.services.SchoolService;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class SchoolServiceImpl implements SchoolService {

    private SchoolRepository schoolRepository;
    private SchoolMapper schoolMapper;

    @Autowired
    public SchoolServiceImpl(SchoolRepository schoolRepository, SchoolMapper schoolMapper) {
        this.schoolRepository = schoolRepository;
        this.schoolMapper = schoolMapper;
    }

    @Override
    public Optional<SchoolDto> findById(Long id) {
        return schoolRepository.findById(id)
                .flatMap(this::getSchoolDtoWithAllFields);
    }


    @Override
    public List<SchoolDto> findAll() {
        return schoolMapper.mapAsList(schoolRepository.findAll(), SchoolDto.class);
    }

    @Override
    @Transactional
    public SchoolDto save(SchoolDto schoolDto) {
        School school = schoolMapper.map(schoolDto, School.class);
        School save = schoolRepository.save(school);
        return schoolMapper.map(save, SchoolDto.class);
    }

    @Override
    public void deleteById(Long id) {
        schoolRepository.deleteById(id);
    }

    @Override
    public List<SchoolDto> findAllByCityId(Long id) {
        return schoolMapper.mapAsList(schoolRepository.findAllByAddress_City_Id(id), SchoolDto.class);
    }

    private Optional<SchoolDto> getSchoolDtoWithAllFields(School school) {
        SchoolDto schoolDto = schoolMapper.map(school, SchoolDto.class);
        if (!school.getEvents().isEmpty()) {
            Set<Long> events = new HashSet<>();
            for (Event event : school.getEvents()) {
                events.add(event.getId());
            }
            schoolDto.setEvents(events);
        }
        if (!school.getPositions().isEmpty()) {
            HashSet<Long> positions = new HashSet<>();
            for (Position position : school.getPositions()) {
                positions.add(position.getId());
            }
            schoolDto.setPositions(positions);
        }
        if (!school.getStudyClasses().isEmpty()) {
            Set<Long> studyClasses = new HashSet<>();
            for (StudyClass studyClass : school.getStudyClasses()) {
                studyClasses.add(studyClass.getId());
            }
            schoolDto.setStudyClasses(studyClasses);
        }
        return Optional.of(schoolDto);
    }
}
