package xyz.thinkglobal.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xyz.thinkglobal.dto.ContactDto;
import xyz.thinkglobal.dto.mappers.ContactMapper;
import xyz.thinkglobal.dto.mappers.StudentMapper;
import xyz.thinkglobal.models.Contact;
import xyz.thinkglobal.models.Enrollee;
import xyz.thinkglobal.models.Student;
import xyz.thinkglobal.repositories.ContactRepository;
import xyz.thinkglobal.repositories.EnrolleeRepository;
import xyz.thinkglobal.repositories.StudentRepository;
import xyz.thinkglobal.services.ContactService;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class ContactServiceImpl implements ContactService {

    private ContactRepository contactRepository;
    private ContactMapper contactMapper;
    private EnrolleeRepository enrolleeRepository;
    private StudentRepository studentRepository;
    private final StudentMapper studentMapper;


    @Autowired
    public ContactServiceImpl(ContactRepository contactRepository,
                              ContactMapper contactMapper,
                              EnrolleeRepository enrolleeRepository,
                              StudentRepository studentRepository,
                              StudentMapper studentMapper) {
        this.contactRepository = contactRepository;
        this.contactMapper = contactMapper;
        this.enrolleeRepository = enrolleeRepository;
        this.studentRepository = studentRepository;
        this.studentMapper = studentMapper;
    }

    @Override
    public Optional<ContactDto> findById(Long id) {
        Optional<Contact> optionalContact = contactRepository.findById(id);

        return optionalContact.map(this::getContactDtoWithAllFields);
    }

    @Override
    public Optional<ContactDto> findByEmail(String email) {
        return contactRepository.findByEmail(email)
                .map(this::getContactDtoWithAllFields);
    }

    @Override
    public List<ContactDto> findAll() {
        List<Contact> contacts = contactRepository.findAll();
        return contacts.stream()
                .map(contact -> contactMapper.map(contact, ContactDto.class))
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public ContactDto save(ContactDto contactDto) {
        Contact contact = getContactWithAllFields(contactDto);
        Contact savedContact = contactRepository.save(contact);
        return getContactDtoWithAllFields(savedContact);
    }

    @Override
    public void deleteById(Long id) {
        contactRepository.deleteById(id);
    }

    private ContactDto getContactDtoWithAllFields(Contact contact) {
        ContactDto contactDto = contactMapper.map(contact, ContactDto.class);
        if (!contact.getEnrolleeSet().isEmpty()) {
            Set<Long> enrolleeSet = new HashSet<>();
            for (Enrollee enrollee : contact.getEnrolleeSet()) {
                enrolleeSet.add(enrollee.getId());
            }
            contactDto.setEnrolleeSet(enrolleeSet);
        }
        if (contact.getStudents() != null) {
            Set<Long> studentsIds = studentMapper.mapStudentsAsListOfId(contact.getStudents());
            contactDto.setStudents(studentsIds);
        }
        return contactDto;
    }

    private Contact getContactWithAllFields(ContactDto contactDto) {
        Contact contact = contactMapper.map(contactDto, Contact.class);
        if (!contactDto.getEnrolleeSet().isEmpty()) {
            Set<Enrollee> enrollees = enrolleeRepository.findAllByIdIn(contactDto.getEnrolleeSet());
            contact.setEnrolleeSet(enrollees);
        }
        if (!contactDto.getStudents().isEmpty()) {
            Set<Student> students = studentRepository.findAllByIdIn(contactDto.getStudents());
            contact.setStudents(students);
        }
        return contact;
    }
}
