package xyz.thinkglobal.services;

import xyz.thinkglobal.dto.EventDto;
import xyz.thinkglobal.dto.enums.EventStatus;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface EventService {

    Optional<EventDto> findById(Long id);

    List<EventDto> findAll();

    LocalDateTime findLastNotVisitedEventDate(Long leadId, String leadStatus);

    List<EventDto> findAllByStatusAndStartDateBetween(EventStatus statusId, LocalDateTime fromTime, LocalDateTime toTime);

    EventDto save(EventDto eventDto);

    void deleteById(Long id);

    boolean validateEvent(EventDto eventDto);

    List<EventDto> findAllActive();
}
