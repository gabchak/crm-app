package xyz.thinkglobal.services;

import xyz.thinkglobal.dto.PositionDto;

import java.util.List;
import java.util.Set;

public interface PositionService {

    Set<PositionDto> getAllByUserEmail(String email);

    PositionDto getById(Long id);

    void deleteById(Long id);

    List<PositionDto> getAllRelatedPositionsById(Long positionId);
}
