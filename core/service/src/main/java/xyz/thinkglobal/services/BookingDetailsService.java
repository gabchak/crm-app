package xyz.thinkglobal.services;

import xyz.thinkglobal.dto.BookingDetailsDto;

import java.util.List;
import java.util.Optional;

public interface BookingDetailsService {

    BookingDetailsDto save(BookingDetailsDto bookingDetailsDto);

    Optional<BookingDetailsDto> findById(Long id);

    List<BookingDetailsDto> findAll();

    void deleteById(Long id);

    void deleteByEnrolleeId(Long enrolleeId);
}
