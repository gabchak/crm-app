package xyz.thinkglobal.services;

import xyz.thinkglobal.dto.BookingDto;
import xyz.thinkglobal.dto.BookingWithBookingDetailsDto;

import java.util.List;
import java.util.Optional;

public interface BookingService {

    Optional<BookingDto> findById(Long id);

    List<BookingDto> findAll();

    BookingDto save(BookingDto bookingDto);

    void deleteById(Long id);

    BookingWithBookingDetailsDto create(BookingDto bookingDto);

    void importEnrolleesToAllBookings();

}
