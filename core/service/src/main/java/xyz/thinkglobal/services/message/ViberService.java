package xyz.thinkglobal.services.message;

public interface ViberService {

    void sendMessage(String phone, String subject, String text);
}
