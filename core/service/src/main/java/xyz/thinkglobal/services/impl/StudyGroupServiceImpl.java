package xyz.thinkglobal.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xyz.thinkglobal.dto.StudyGroupDto;
import xyz.thinkglobal.dto.mappers.StudentMapper;
import xyz.thinkglobal.dto.mappers.StudyGroupMapper;
import xyz.thinkglobal.models.Student;
import xyz.thinkglobal.models.StudyClass;
import xyz.thinkglobal.models.StudyGroup;
import xyz.thinkglobal.repositories.StudyClassRepository;
import xyz.thinkglobal.repositories.StudyGroupRepository;
import xyz.thinkglobal.services.StudyGroupService;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class StudyGroupServiceImpl implements StudyGroupService {

    private StudyGroupRepository studyGroupRepository;
    private StudyGroupMapper studyGroupMapper;
    private StudentMapper studentMapper;
    private StudyClassRepository studyClassRepository;


    @Autowired
    public StudyGroupServiceImpl(StudyGroupRepository studyGroupRepository,
                                 StudyGroupMapper studyGroupMapper,
                                 StudentMapper studentMapper,
                                 StudyClassRepository studyClassRepository) {
        this.studyGroupRepository = studyGroupRepository;
        this.studyGroupMapper = studyGroupMapper;
        this.studentMapper = studentMapper;
        this.studyClassRepository = studyClassRepository;
    }

    @Override
    public List<StudyGroupDto> findAllBySchoolId(Long id) {
        return studyGroupMapper.mapAsList(
                studyGroupRepository.findAllByStudyClass_School_Id(id), StudyGroupDto.class);
    }

    @Override
    public Optional<StudyGroupDto> findById(Long id) {
        return studyGroupRepository.findById(id)
                .flatMap(this::getStudyGroupWithAllFields);
    }

    @Override
    @Transactional
    public StudyGroupDto save(StudyGroupDto studyGroupDto) {
        StudyGroup studyGroup = studyGroupMapper.map(studyGroupDto, StudyGroup.class);
        return studyGroupMapper.map(studyGroupRepository.save(studyGroup), StudyGroupDto.class);
    }

    @Override
    public void deleteById(Long id) {
        studyGroupRepository.deleteById(id);
    }

    @Override
    @Transactional
    public StudyGroupDto create(StudyGroupDto studyGroupDto) {
        StudyGroup studyGroup = studyGroupMapper.map(studyGroupDto, StudyGroup.class);
        Optional<StudyClass> optionalStudyClass = studyClassRepository.findById(studyGroupDto.getStudyClass());
        optionalStudyClass.ifPresent(studyClass -> {
            studyGroup.setStudyClass(studyClass);
            getGeneratedName(studyGroup, studyClass);
        });
        StudyGroup savedStudyGroup = studyGroupRepository.save(studyGroup);
        return studyGroupMapper.map(studyGroupRepository.save(savedStudyGroup), StudyGroupDto.class);
    }

    private void getGeneratedName(StudyGroup studyGroup, StudyClass studyClass) {
        String inputName = studyGroup.getName();
        studyGroup.setName(studyClass.getName() + inputName);
    }

    private Optional<StudyGroupDto> getStudyGroupWithAllFields(StudyGroup studyGroup) {
        StudyGroupDto studyGroupDto = studyGroupMapper.map(studyGroup, StudyGroupDto.class);

        if (studyGroup.getStudents() != null) {
            Set<Long> studentsIds = studentMapper.mapStudentsAsListOfId(studyGroup.getStudents());
            studyGroupDto.setStudents(studentsIds);
        }
        return Optional.of(studyGroupDto);
    }
}
