package xyz.thinkglobal.services;

import xyz.thinkglobal.dto.SpeakerDto;

import java.util.List;
import java.util.Optional;

public interface SpeakerService {

    Optional<SpeakerDto> findById(Long id);

    List<SpeakerDto> findAll();

    SpeakerDto save(SpeakerDto speakerDto);

    void deleteById(Long id);
}
