package xyz.thinkglobal.services;

import xyz.thinkglobal.dto.SchoolDto;

import java.util.List;
import java.util.Optional;

public interface SchoolService {

    Optional<SchoolDto> findById(Long id);

    List<SchoolDto> findAll();

    SchoolDto save(SchoolDto schoolDto);

    void deleteById(Long id);

    List<SchoolDto> findAllByCityId(Long id);
}
