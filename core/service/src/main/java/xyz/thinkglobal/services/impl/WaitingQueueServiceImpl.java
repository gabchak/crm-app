package xyz.thinkglobal.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xyz.thinkglobal.constatns.TGConstants;
import xyz.thinkglobal.dto.WaitingQueueDto;
import xyz.thinkglobal.dto.enums.WaitingQueueStatus;
import xyz.thinkglobal.dto.mappers.WaitingQueueMapper;
import xyz.thinkglobal.models.Position;
import xyz.thinkglobal.models.StudyClass;
import xyz.thinkglobal.models.WaitingQueue;
import xyz.thinkglobal.models.WaitingQueueEnrollee;
import xyz.thinkglobal.repositories.PositionRepository;
import xyz.thinkglobal.repositories.StudyClassRepository;
import xyz.thinkglobal.repositories.WaitingQueueRepository;
import xyz.thinkglobal.services.WaitingQueueService;

import javax.transaction.Transactional;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class WaitingQueueServiceImpl implements WaitingQueueService {

    private final WaitingQueueRepository waitingQueueRepository;
    private final WaitingQueueMapper waitingQueueMapper;
    private final PositionRepository positionRepository;
    private final StudyClassRepository studyClassRepository;


    @Autowired
    public WaitingQueueServiceImpl(WaitingQueueRepository waitingQueueRepository,
                                   WaitingQueueMapper waitingQueueMapper,
                                   PositionRepository positionRepository,
                                   StudyClassRepository studyClassRepository) {
        this.waitingQueueRepository = waitingQueueRepository;
        this.waitingQueueMapper = waitingQueueMapper;
        this.positionRepository = positionRepository;
        this.studyClassRepository = studyClassRepository;
    }

    @Override
    public Optional<WaitingQueueDto> findById(Long waitingQueueId) {
        return waitingQueueRepository.findById(waitingQueueId)
                .flatMap(this::getWaitingQueueWithAllFields);
    }

    @Override
    @Transactional
    public WaitingQueueDto save(WaitingQueueDto waitingQueueDto) {
        WaitingQueue waitingQueue = waitingQueueMapper.map(waitingQueueDto, WaitingQueue.class);
        if (waitingQueue.getUniqueCode() == null || waitingQueue.getUniqueCode().length() > 0) {
            waitingQueue.setUniqueCode(getUniqueCodeForWaitingQueue(waitingQueue));
            waitingQueue.setStatus(WaitingQueueStatus.OPEN.toString());
        }
        WaitingQueue savedSWQ = waitingQueueRepository.save(waitingQueue);
        return waitingQueueMapper.map(savedSWQ, WaitingQueueDto.class);
    }

    @Override
    public Optional<WaitingQueueDto> findByClassId(Long classId) {
        Optional<WaitingQueue> waitingQueueOptional = waitingQueueRepository.findByStudyClass_Id(classId);
        return waitingQueueOptional
                .map(w -> waitingQueueMapper.map(w, WaitingQueueDto.class));
    }

    @Override
    public List<WaitingQueueDto> findAllBySchoolId(Long schoolId) {
        return waitingQueueMapper.mapAsList(waitingQueueRepository.findAllByStudyClass_School_Id(schoolId),
                WaitingQueueDto.class);
    }

    @Override
    public List<WaitingQueueDto> findAll() {
        return waitingQueueMapper.mapAsList(waitingQueueRepository.findAll(), WaitingQueueDto.class);
    }

    @Override
    public List<WaitingQueueDto> findAllByPositionId(Long positionId) throws IllegalAccessException {
        List<Position> allUserPositions = getAllUserPositions(positionId);

        List<Long> schoolIds = new ArrayList<>();
        allUserPositions.forEach(position -> schoolIds.add(position.getSchool().getId()));
        return waitingQueueMapper.mapAsList(
                waitingQueueRepository.findAllByStudyClass_School_IdIn(schoolIds),
                WaitingQueueDto.class);
    }

    @Override
    public boolean hasAccessToStudyClass(Long positionId, Long studyClassId) throws IllegalAccessException {
        List<Position> allUserPositions = getAllUserPositions(positionId);
        Optional<StudyClass> optionalStudyClass = studyClassRepository.findById(studyClassId);
        boolean result = false;

        if (optionalStudyClass.isPresent()) {
            StudyClass studyClass = optionalStudyClass.get();
            for (Position position : allUserPositions) {
                if (position.getSchool().equals(studyClass.getSchool())) {
                    result = true;
                }
            }
        }

        return result;
    }


    public boolean hasAccessToWaitingQueue(Long positionId, Long waitingQueueId) throws IllegalAccessException {
        List<Position> allUserPositions = getAllUserPositions(positionId);
        Optional<WaitingQueue> optionalWaitingQueue = waitingQueueRepository.findById(waitingQueueId);

        if (optionalWaitingQueue.isPresent()) {
            WaitingQueue waitingQueue = optionalWaitingQueue.get();
            for (Position position : allUserPositions) {
                if (position.getSchool().getStudyClasses().contains(waitingQueue.getStudyClass())) {
                    return true;
                }
            }
        }
        return false;
    }

    private List<Position> getAllUserPositions(Long positionId) throws IllegalAccessException {
        Optional<Position> optionalPosition = positionRepository.findById(positionId);
        if (!optionalPosition.isPresent()) {
            throw new IllegalAccessException();
        }
        return positionRepository
                .findAllByUser_IdAndIdIn(optionalPosition.get().getUser().getId(), positionId);
    }

    private String getUniqueCodeForWaitingQueue(WaitingQueue waitingQueue) {
        StringBuilder sb = new StringBuilder();
        sb.append(waitingQueue.getStudyClass().getSchool().getCode())
          .append(TGConstants.WAITING_QUEUE_CODE)
          .append(waitingQueue.getExpirationDate().format(DateTimeFormatter.ofPattern("yy")))
          .append(waitingQueue.getStudyClass().getName());
        return sb.toString();
    }

    private Optional<WaitingQueueDto> getWaitingQueueWithAllFields(WaitingQueue waitingQueue) {
        WaitingQueueDto waitingQueueDto = waitingQueueMapper.map(waitingQueue, WaitingQueueDto.class);
        if (!waitingQueue.getWaitingQueueEnrolleeList().isEmpty()) {
            Set<Long> enrolees = new HashSet<>();
            for (WaitingQueueEnrollee waitingQueueEnrollee : waitingQueue.getWaitingQueueEnrolleeList()) {
                enrolees.add(waitingQueueEnrollee.getEnrollee().getId());
            }
            waitingQueueDto.setEnrolleeSet(enrolees);
        }
        return Optional.of(waitingQueueDto);
    }

}
