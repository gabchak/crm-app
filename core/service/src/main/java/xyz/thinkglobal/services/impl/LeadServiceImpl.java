package xyz.thinkglobal.services.impl;

import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import xyz.thinkglobal.constatns.TemplatePaths;
import xyz.thinkglobal.dto.LeadDto;
import xyz.thinkglobal.dto.enums.LeadStatus;
import xyz.thinkglobal.dto.filters.LeadFilterDto;
import xyz.thinkglobal.dto.mappers.LeadMapper;
import xyz.thinkglobal.messaging.EmailService;
import xyz.thinkglobal.models.Event;
import xyz.thinkglobal.models.Lead;
import xyz.thinkglobal.models.filters.LeadFilter;
import xyz.thinkglobal.repositories.EventRepository;
import xyz.thinkglobal.repositories.LeadRepository;
import xyz.thinkglobal.services.LeadService;
import xyz.thinkglobal.services.message.TelegramService;
import xyz.thinkglobal.services.message.ViberService;
import xyz.thinkglobal.util.TemplateUtil;

import javax.mail.MessagingException;
import javax.transaction.Transactional;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class LeadServiceImpl implements LeadService {

    private LeadRepository leadRepository;
    private LeadMapper leadMapper;
    private EmailService emailService;
    private ViberService viberService;
    private TelegramService telegramService;
    private TemplateUtil templateUtil;
    private Environment environment;
    private EventRepository eventRepository;

    @Autowired
    public LeadServiceImpl(LeadRepository leadRepository,
                           LeadMapper leadMapper,
                           EmailService emailService,
                           ViberService viberService,
                           TelegramService telegramService,
                           TemplateUtil templateUtil,
                           Environment environment,
                           EventRepository eventRepository) {
        this.leadRepository = leadRepository;
        this.leadMapper = leadMapper;
        this.emailService = emailService;
        this.viberService = viberService;
        this.telegramService = telegramService;
        this.templateUtil = templateUtil;
        this.environment = environment;
        this.eventRepository = eventRepository;
    }

    @Override
    @Transactional
    public Optional<LeadDto> findById(Long id) {
        Optional<Lead> optionalLead = leadRepository.findById(id);
        return optionalLead.flatMap(this::getLeadDtoWithLeadEventSet);
    }

    @Override
    public Optional<LeadDto> findByEmail(String email) {
        Optional<Lead> optionalLead = leadRepository.findByEmail(email);
        return optionalLead.flatMap(this::getLeadDtoWithLeadEventSet);
    }

    @Override
    public List<LeadDto> findAll() {
        List<Lead> leads = leadRepository.findAll();

        return leads.stream().map(lead -> leadMapper.map(lead, LeadDto.class)).collect(Collectors.toList());
    }

    @Override
    public List<LeadDto> findAllByStatus(LeadStatus status) {
        List<Lead> leads = leadRepository.findLeadsByStatus(status.toString());

        return leads.stream().map(lead -> leadMapper.map(lead, LeadDto.class)).collect(Collectors.toList());

    }

    @Override
    @Transactional
    public LeadDto save(LeadDto leadDto) {
        Lead lead = leadMapper.map(leadDto, Lead.class);

        Lead savedLead = leadRepository.save(lead);

        return leadMapper.map(savedLead, LeadDto.class);
    }

    @Override
    public void deleteById(Long id) {
        leadRepository.deleteById(id);
    }

    @Override
    public List<LeadDto> findBy(LeadFilterDto leadFilterDto) {
        LeadFilter leadFilter = leadMapper.map(leadFilterDto, LeadFilter.class);

        List<Lead> leads = leadRepository.findAll(leadFilter);

        return leads.stream().map(lead -> leadMapper.map(lead, LeadDto.class)).collect(Collectors.toList());
    }

    @Override
    public void sendEmailWithRegistrationLink(String email, Long eventId)
            throws IOException, MessagingException, TemplateException {
        Map<String, Object> model = new HashMap<>();

        Event event = eventRepository.findById(eventId).get();
        model.put("event", event);
        model.put("eventLink", formRegistrationForEventLink(eventId));

        String subject = environment.getProperty("mail.subject.lead_registration_link");

        String text = templateUtil.getHtmlMessage(this.getClass(), TemplatePaths.T_LEAD_REGISTRATION_LINK, model);
        emailService.sendSimpleMessage(email, subject, text);
    }

    @Override
    public void sendMessageWithRegistrationLink(String phone, Long eventId) {
        String subject = environment.getProperty("mail.subject.lead_registration_link");
        String link = formRegistrationForEventLink(eventId);

        //TODO: add template to text message. Replace link to text with link.
        viberService.sendMessage(phone, subject, link);
        telegramService.sendMessage(phone, subject, link);
    }

    private String formRegistrationForEventLink(Long eventId) {
        String link = environment.getProperty("front-end.lead-registration-url");
        if (eventId != null) {
            link += "?eventId=" + eventId;
        }
        return link;
    }

    private Optional<LeadDto> getLeadDtoWithLeadEventSet(Lead lead) {
        LeadDto leadDto = leadMapper.map(lead, LeadDto.class);
//        Set<LeadEventDto> leadEventDtos = leadEventMapper.mapAsSet(lead.getLeadEventSet(), LeadEventDto.class);
//        leadDto.setLeadEventDtoSet(leadEventDtos);
        return Optional.of(leadDto);
    }
}