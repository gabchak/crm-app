package xyz.thinkglobal.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import xyz.thinkglobal.dto.BookingDto;
import xyz.thinkglobal.dto.BookingWithBookingDetailsDto;
import xyz.thinkglobal.dto.enums.WaitingQueueEnrolleeStatus;
import xyz.thinkglobal.dto.mappers.BookingMapper;
import xyz.thinkglobal.dto.mappers.BookingWithBookingDetailsMapper;
import xyz.thinkglobal.models.Booking;
import xyz.thinkglobal.models.BookingDetails;
import xyz.thinkglobal.models.Enrollee;
import xyz.thinkglobal.models.School;
import xyz.thinkglobal.models.StudyGroup;
import xyz.thinkglobal.models.WaitingQueue;
import xyz.thinkglobal.models.WaitingQueueEnrollee;
import xyz.thinkglobal.repositories.BookingDetailsRepository;
import xyz.thinkglobal.repositories.BookingRepository;
import xyz.thinkglobal.repositories.SchoolRepository;
import xyz.thinkglobal.repositories.WaitingQueueEnrolleeRepository;
import xyz.thinkglobal.services.BookingService;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class BookingServiceImpl implements BookingService {

    private BookingRepository bookingRepository;
    private BookingMapper bookingMapper;
    private SchoolRepository schoolRepository;
    private final BookingDetailsRepository bookingDetailsRepository;
    private Environment environment;
    private WaitingQueueEnrolleeRepository waitingQueueEnrolleeRepository;
    private BookingWithBookingDetailsMapper bookingWithBookingDetailsMapper;


    @Autowired
    public BookingServiceImpl(BookingMapper bookingMapper,
                              BookingRepository bookingRepository,
                              BookingDetailsRepository bookingDetailsRepository,
                              BookingWithBookingDetailsMapper bookingWithBookingDetailsMapper,
                              Environment environment,
                              SchoolRepository schoolRepository,
                              WaitingQueueEnrolleeRepository waitingQueueEnrolleeRepository) {
        this.bookingDetailsRepository = bookingDetailsRepository;
        this.bookingMapper = bookingMapper;
        this.bookingRepository = bookingRepository;
        this.bookingWithBookingDetailsMapper = bookingWithBookingDetailsMapper;
        this.environment = environment;
        this.schoolRepository = schoolRepository;
        this.waitingQueueEnrolleeRepository = waitingQueueEnrolleeRepository;
    }

    @Override
    @Transactional
    public BookingWithBookingDetailsDto create(BookingDto bookingDto) {
        Booking booking = bookingMapper.map(bookingDto, Booking.class);
        booking.setOpenDate(LocalDate.now());
        String generatedName = getGeneratedName(booking.getStudyGroup());
        booking.setName(generatedName);
        booking.setCapacity(booking.getStudyGroup().getNumberOfSeats());
        Booking savedBooking = bookingRepository.save(booking);
        List<BookingDetails> bookingDetails = importEnrolleesToOneBooking(savedBooking);
        Set<BookingDetails> bookingDetailsSet = new LinkedHashSet<>(bookingDetails);
        savedBooking.setBookingDetailsSet(bookingDetailsSet);

        return bookingWithBookingDetailsMapper.map(savedBooking, BookingWithBookingDetailsDto.class);
    }

    @Override
    @Transactional
    public BookingDto save(BookingDto bookingDto) {
        Booking booking = bookingMapper.map(bookingDto, Booking.class);
        Booking savedBooking = bookingRepository.save(booking);
        return bookingMapper.map(savedBooking, BookingDto.class);
    }

    @Override
    public Optional<BookingDto> findById(Long id) {
        return bookingRepository.findById(id)
                .flatMap(this::getBookingDtoWithBookingDetails);
    }

    @Override
    public List<BookingDto> findAll() {
        return bookingMapper.mapAsList(bookingRepository.findAll(), BookingDto.class);
    }

    @Override
    public void deleteById(Long id) {
        bookingRepository.deleteById(id);
    }

    private Optional<BookingDto> getBookingDtoWithBookingDetails(Booking booking) {
        BookingDto bookingDto = bookingMapper.map(booking, BookingDto.class);

        if (!booking.getBookingDetailsSet().isEmpty()) {
            Set<Long> bookingDetailsSet = new HashSet<>();
            for (BookingDetails bookingDetails : booking.getBookingDetailsSet()) {
                bookingDetailsSet.add(bookingDetails.getId());
            }
            bookingDto.setBookingDetailsSet(bookingDetailsSet);
        }
        return Optional.of(bookingDto);
    }

    /**
     * Find all not filling bookings and
     * import N enrollees in each booking.
     * Where N is quantity of free places in booking.
     */
    @Override
    @Transactional
    public void importEnrolleesToAllBookings() {

        List<Booking> bookingList = bookingRepository.findAllByFreePlacesGreaterThan(0);

        List<BookingDetails> resultBookingDetailsList = new ArrayList<>();
        List<WaitingQueueEnrollee> resultWaitingQueueEnrolleeList = new ArrayList<>();

        for (Booking booking : bookingList) {
            importEnrolleesToBooking(booking, resultBookingDetailsList, resultWaitingQueueEnrolleeList);
        }
        bookingDetailsRepository.saveAll(resultBookingDetailsList);
        waitingQueueEnrolleeRepository.saveAll(resultWaitingQueueEnrolleeList);
    }


    /**
     * Find booking by bookingId.
     * Import N enrollees from Waiting Queue to Booking.
     * Where N is quantity of free places in booking.
     *
     * @param booking - booking for import enrollees from WQ
     */
    private List<BookingDetails> importEnrolleesToOneBooking(Booking booking) {

        List<BookingDetails> resultBookingDetailsList = new ArrayList<>();
        List<WaitingQueueEnrollee> resultWaitingQueueEnrolleeList = new ArrayList<>();

        importEnrolleesToBooking(booking, resultBookingDetailsList, resultWaitingQueueEnrolleeList);

        List<BookingDetails> bookingDetails = bookingDetailsRepository.saveAll(resultBookingDetailsList);
        waitingQueueEnrolleeRepository.saveAll(resultWaitingQueueEnrolleeList);

        return bookingDetails;
    }

    /**
     * Iterate by enrollees in waiting queue.
     * Change enrollee status if enrollee add in booking.
     * Create new BookingDetails for each enrollee.
     * Change quantity of free places in booking when import is over.
     *
     * @param booking   - booking for processing.
     * @param resultBD  - result booking details list witch contains all booking details lists. ResultBD will save with all changes.
     * @param resultWQE - result waiting queue enrollee list
     *                  witch contains all waiting queue enrollee lists. ResultWQE will save.
     */
    private void importEnrolleesToBooking(Booking booking,
                                          List<BookingDetails> resultBD,
                                          List<WaitingQueueEnrollee> resultWQE) {

        WaitingQueue waitingQueue = booking.getStudyGroup().getStudyClass().getWaitingQueue();
        List<WaitingQueueEnrollee> waitingQueueEnrolleeList = waitingQueue.getWaitingQueueEnrolleeList();

        int enrolleesQuantity = booking.getFreePlaces();
        int freePlaces = booking.getFreePlaces();

        for (int i = 0; i < enrolleesQuantity && i < waitingQueueEnrolleeList.size(); i++) {
            WaitingQueueEnrollee waitingQueueEnrollee = waitingQueueEnrolleeList.get(i);
            if (!waitingQueueEnrollee.getStatus().equals(WaitingQueueEnrolleeStatus.INACTIVE.toString())) {
                waitingQueueEnrollee.setStatus(WaitingQueueEnrolleeStatus.INACTIVE.toString());
                resultWQE.add(waitingQueueEnrollee);

                Enrollee enrollee = waitingQueueEnrollee.getEnrollee();

                BookingDetails bookingDetails = createBookingDetails(booking, resultBD, enrollee);
                resultBD.add(bookingDetails);
                freePlaces--;

            } else {
                enrolleesQuantity++;
            }
        }
        booking.setFreePlaces(freePlaces);
    }

    /**
     * Add new BookingDetails to result list for save.
     *
     * @param booking  - current booking. It will set into new booking details
     * @param resultBD - result booking details list witch contains all booking details lists.
     *                 ResultBD will save with all changes.
     * @param enrollee - current enrollee. It will set into booking details.
     */
    private BookingDetails createBookingDetails(Booking booking, List<BookingDetails> resultBD, Enrollee enrollee) {
        BookingDetails bookingDetails = new BookingDetails();
        bookingDetails.setBooking(booking);
        bookingDetails.setEnrollee(enrollee);
        setRegistrationAndExpirationDates(bookingDetails);
        return bookingDetails;
    }

    /**
     * Set registration and expiration dates when contact add enrollee in booking.
     * Expiration date = registration date + booking term.
     *
     * @param bookingDetails - current booking details
     */
    private void setRegistrationAndExpirationDates(BookingDetails bookingDetails) {
        LocalDate registrationDate = LocalDate.now();
        bookingDetails.setRegistrationDate(registrationDate);
        Integer bookingTerm = environment.getProperty("booking.term.enrollee.week", Integer.class);
        bookingDetails.setExpirationDate(registrationDate.plusWeeks(bookingTerm));
    }

    private String getGeneratedName(StudyGroup studyGroup) {
        School school = schoolRepository.findByStudyClasses_StudyGroups_Id(studyGroup.getId());

        StringBuilder codeBuilder = new StringBuilder();
        codeBuilder.append(school.getCode());
        codeBuilder.append("B");
        codeBuilder.append(studyGroup.getOpenDate().format(DateTimeFormatter.ofPattern("yy")));
        codeBuilder.append(studyGroup.getStudyClass().getName());
        codeBuilder.append(studyGroup.getName());

        return codeBuilder.toString();
    }
}
