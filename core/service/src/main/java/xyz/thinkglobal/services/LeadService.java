package xyz.thinkglobal.services;

import freemarker.template.TemplateException;
import xyz.thinkglobal.dto.LeadDto;
import xyz.thinkglobal.dto.filters.LeadFilterDto;
import xyz.thinkglobal.dto.enums.LeadStatus;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

public interface LeadService {

    Optional<LeadDto> findById(Long id);

    List<LeadDto> findAll();

    List<LeadDto> findAllByStatus(LeadStatus statusType);

    Optional<LeadDto> findByEmail(String email);

    LeadDto save(LeadDto lead);

    void deleteById(Long id);

    List<LeadDto> findBy(LeadFilterDto leadFilterDto);

    void sendEmailWithRegistrationLink(String email, Long eventId) throws IOException, MessagingException, TemplateException;

    void sendMessageWithRegistrationLink(String phone, Long eventId);
}
