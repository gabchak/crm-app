package xyz.thinkglobal.business.service;


import freemarker.template.TemplateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import xyz.thinkglobal.Exception.EventDoesNotExist;
import xyz.thinkglobal.Exception.EventFinishedException;
import xyz.thinkglobal.Exception.UserIsAlreadyExistsException;
import xyz.thinkglobal.dto.ContactDto;
import xyz.thinkglobal.dto.enums.EventStatus;
import xyz.thinkglobal.dto.enums.EventType;
import xyz.thinkglobal.dto.enums.LeadEventStatus;
import xyz.thinkglobal.dto.enums.LeadStatus;
import xyz.thinkglobal.dto.enums.Positions;
import xyz.thinkglobal.dto.mappers.ContactMapper;
import xyz.thinkglobal.messaging.EmailService;
import xyz.thinkglobal.models.Contact;
import xyz.thinkglobal.models.Event;
import xyz.thinkglobal.models.Lead;
import xyz.thinkglobal.models.LeadEvent;
import xyz.thinkglobal.models.Position;
import xyz.thinkglobal.models.User;
import xyz.thinkglobal.repositories.ContactRepository;
import xyz.thinkglobal.repositories.EventRepository;
import xyz.thinkglobal.repositories.LeadEventRepository;
import xyz.thinkglobal.repositories.LeadRepository;
import xyz.thinkglobal.repositories.PositionRepository;
import xyz.thinkglobal.util.TemplateUtil;

import javax.mail.MessagingException;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static xyz.thinkglobal.constatns.TemplatePaths.T_CONTACT_TO_CONTINUE_EVENT;
import static xyz.thinkglobal.constatns.TemplatePaths.T_LEAD_MISSED_EVENT;
import static xyz.thinkglobal.constatns.TemplatePaths.T_LEAD_TO_CONTINUE_EVENT;


@Service
public class LeadToContactConversionService {

    private static final int DAYS_AFTER_ON_HOLD_TO_RECALL = 3;

    private static final Logger LOGGER = LoggerFactory.getLogger(LeadToContactConversionService.class);

    private LeadRepository leadRepository;
    private LeadEventRepository leadEventRepository;
    private EventRepository eventRepository;
    private ContactRepository contactRepository;
    private ContactMapper contactMapper;
    private EmailService emailService;
    private TemplateUtil templateUtil;
    private Environment environment;
    private PositionRepository positionRepository;

    @Autowired
    public LeadToContactConversionService(LeadRepository leadRepository,
                                          LeadEventRepository leadEventRepository,
                                          EventRepository eventRepository,
                                          ContactRepository contactRepository,
                                          ContactMapper contactMapper,
                                          EmailService emailService,
                                          TemplateUtil templateUtil,
                                          Environment environment,
                                          PositionRepository positionRepository) {
        this.leadRepository = leadRepository;
        this.leadEventRepository = leadEventRepository;
        this.eventRepository = eventRepository;
        this.contactRepository = contactRepository;
        this.contactMapper = contactMapper;
        this.emailService = emailService;
        this.templateUtil = templateUtil;
        this.environment = environment;
        this.positionRepository = positionRepository;
    }

    @Transactional
    public void sendSmsReminderWhenEventStart() {
        LocalDateTime startDate = LocalDate.now().atTime(LocalTime.MIN).plusDays(1);
        LocalDateTime endDate = LocalDate.now().atTime(LocalTime.MAX).plusDays(1);

        List<Event> events = eventRepository
                .findAllByStatusAndStartDateBetween(EventStatus.APPROVED.toString(), startDate, endDate);

        events.forEach(event ->
                event.getLeadEventSet().forEach(leadEvent -> {
                    Optional<Lead> optionalLead = leadRepository.findById(leadEvent.getLead().getId());
                    optionalLead.ifPresent(lead -> sendSms(lead.getPhone(),
                            environment.getProperty("message.subject.reminder-about-event")));//TODO: need to use individual template for each event
                }));
    }

    public void updateLeadToRecall() {
        //TODO: logic may be not exactly right
        List<Lead> leads = leadRepository.findLeadsByStatus(LeadStatus.ON_HOLD.toString());
        leads.forEach(lead -> {
            //TODO: every for each do request to DB! Fix it! please :)
            LocalDateTime time = eventRepository.findLastNotVisitedEventDate(lead.getId(), LeadStatus.ON_HOLD.toString());
            if (ChronoUnit.DAYS.between(time, LocalDateTime.now()) >= DAYS_AFTER_ON_HOLD_TO_RECALL) {
                lead.setStatus(LeadStatus.RECALL.toString());
                leadRepository.save(lead);
            }
        });
    }


    @Transactional
    public void closeEventAndMarkLeads(Long eventId, List<Long> visitedLeads)
            throws IOException, TemplateException, MessagingException,
            EventFinishedException, EventDoesNotExist {

        Optional<Event> optionalEvent = eventRepository.findById(eventId);

        if (!optionalEvent.isPresent()) {
            throw new EventDoesNotExist();
        }

        Event event = optionalEvent.get();

        if (event.getType().equals(EventType.INDIVIDUAL_MEETING.toString())) {
            markLeads(eventId, visitedLeads);
        }

        if (event.getStatus().equals(EventStatus.FINISHED.toString())) {
            throw new EventFinishedException();
        } else {

            event.setStatus(EventStatus.FINISHED.toString());
            eventRepository.save(event);

            List<LeadEvent> leadToEvents = markLeads(eventId, visitedLeads);

            leadEventRepository.saveAll(leadToEvents);
        }


    }

    private List<LeadEvent> markLeads(Long eventId, List<Long> visitedLeads) throws TemplateException, IOException, MessagingException {
        List<LeadEvent> leadToEvents = leadEventRepository.findAllByEventId(eventId);

        for (LeadEvent leadToEvent : leadToEvents) {

            if (visitedLeads.contains(leadToEvent.getLeadId())) {
                markVisitedLeads(leadToEvent);
            } else {
                markNotVisitedLeads(leadToEvent);
                changeLeadsStatusToOnHold(leadToEvent.getLead());
            }
        }
        return leadToEvents;
    }

    private void changeLeadsStatusToOnHold(Lead lead) {
        boolean isOnHold = lead.getLeadEventSet()
                .stream()
                .filter(leadEvent ->
                        !leadEvent.getStatus().equals(LeadEventStatus.NOT_VISITED.toString()))
                .collect(Collectors.toList())
                .isEmpty();
        if (isOnHold) {
            lead.setStatus(LeadStatus.ON_HOLD.toString());
        }
    }

    private void markVisitedLeads(LeadEvent leadEvent)
            throws TemplateException, IOException, MessagingException {
        leadEvent.setStatus(LeadEventStatus.VISITED.toString());
        Optional<Contact> optionalContact = contactRepository.findById(leadEvent.getLeadId());
        if (optionalContact.isPresent()) {
            sendEmailToContactToContinueEvent(leadEvent);
        }
        sendEmailToLeadToContinueEvent(leadEvent);
    }

    private void markNotVisitedLeads(LeadEvent leadEvent)
            throws TemplateException, IOException, MessagingException {
        leadEvent.setStatus(LeadEventStatus.NOT_VISITED.toString());
        sendEmailMissedEvent(leadEvent);
    }

    public Long registerContact(ContactDto contactDto)
            throws UserIsAlreadyExistsException {
        Optional<Contact> optionalExistingContact = contactRepository.findByEmail(contactDto.getEmail());

        if (!optionalExistingContact.isPresent()) {

            Optional<Lead> optionalLead = leadRepository.findByEmail(contactDto.getEmail());

            if (optionalLead.isPresent()) {
                Contact contact = contactMapper.map(contactDto, Contact.class);
                contact.setLead(optionalLead.get().getId());
                contactRepository.save(contact);
            }

        } else {
            LOGGER.error("User with " + contactDto.getEmail() + " email is already exists.");
            throw new UserIsAlreadyExistsException();
        }
        return contactDto.getId();
    }

    private void sendEmailToLeadToContinueEvent(LeadEvent leadEvent)
            throws TemplateException, IOException, MessagingException {
        Map<String, Object> model = new HashMap<>();
        model.put("leadEvent", leadEvent);

        String hash = new BCryptPasswordEncoder()
                .encode(leadEvent.getLeadId() + leadEvent.getLead().getEmail());

        String contactRegistrationLinkWithHash = environment.getProperty("front-end.contact-registration-url") +
                "?email=" + leadEvent.getLead().getEmail() +
                "&em=" + hash;

        model.put("contactRegistrationLink", contactRegistrationLinkWithHash);

        Optional<Position> optionalPosition = positionRepository
                .findBySchool_idAndPosition(leadEvent.getEvent().getSchool().getId(),
                        Positions.FAMILY_PARTNER.toString());
        User familyPartner = optionalPosition.get().getUser();
        model.put("familyPartner", familyPartner);

        sendEmail(leadEvent.getLead().getEmail(), model,
                environment.getProperty("mail.subject.lead-to-continue-event"), T_LEAD_TO_CONTINUE_EVENT);
    }

    private void sendEmailMissedEvent(LeadEvent leadEvent)
            throws TemplateException, IOException, MessagingException {
        Map<String, Object> model = new HashMap<>();
        model.put("leadEvent", leadEvent);

        Long cityId = leadEvent.getEvent().getSchool().getAddress().getCity().getId();
        Set<Event> upcomingEvents = eventRepository.findAllBySchool_Address_City_Id(cityId);
        model.put("events", upcomingEvents);
        model.put("hotline", environment.getProperty("hotline.phone-number"));

        sendEmail(leadEvent.getLead().getEmail(), model,
                environment.getProperty("mail.subject.lead-missed-event"), T_LEAD_MISSED_EVENT);
    }

    private void sendEmailToContactToContinueEvent(LeadEvent leadEvent)
            throws TemplateException, IOException, MessagingException {

        Map<String, Object> model = new HashMap<>();
        model.put("leadEvent", leadEvent);
        model.put("accountLink", environment.getProperty("front-end.account"));

        Optional<Position> optionalPosition = positionRepository
                .findBySchool_idAndPosition(leadEvent.getEvent().getSchool().getId(),
                        Positions.FAMILY_PARTNER.toString());
        User familyPartner = optionalPosition.get().getUser();
        model.put("familyPartner", familyPartner);

        sendEmail(leadEvent.getLead().getEmail(), model,
                environment.getProperty("mail.subject.contact-to-continue-event"), T_CONTACT_TO_CONTINUE_EVENT);
    }


    private void sendEmail(String email, Map<String, Object> model,
                           String subject, String templatePath)
            throws IOException, TemplateException, MessagingException {

        String text = templateUtil.getHtmlMessage(this.getClass(), templatePath, model);
        emailService.sendSimpleMessage(email, subject, text);
    }

    private void sendSms(String phone, String message) {
        LOGGER.info("@sendSms: {}", phone + message);
    }

}
