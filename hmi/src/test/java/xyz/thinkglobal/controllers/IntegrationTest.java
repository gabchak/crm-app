package xyz.thinkglobal.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.RestAssured;
import lombok.extern.slf4j.Slf4j;
import io.restassured.http.ContentType;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import static io.restassured.RestAssured.given;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
@Slf4j
public class IntegrationTest {

    @LocalServerPort
    private int port;

    @Autowired
    ObjectMapper objectMapper;

    @Before
    public void setup() {
        RestAssured.port = this.port;
        String token = given()
                .contentType(ContentType.JSON)
                .body("{\"username\":\"username\",\"password\":\"test\"")
                .when().post("/login")
                .andReturn().jsonPath().getString("token");
        log.debug("Got token:" + token);
    }

    @Test
    public void testFindLeadByWithAuth() throws Exception {
//
//        //@formatter:off
//        given()
//                .header("Authorization", "Bearer "+token)
//                .contentType(ContentType.JSON)
//                .body("{\"email\":\"some@email.com\"}")
//
//                .when()
//                .get("/leads/find-by")
//
//                .then()
//                .statusCode(201);
//
//        //@formatter:on
    }
}
