package xyz.thinkglobal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@Configuration
@EnableAutoConfiguration
@ComponentScan
@PropertySource(value = "classpath:application-test.properties")
public class TGApplicationTests {

    @Test
    public void contextLoads() {
    }

}

