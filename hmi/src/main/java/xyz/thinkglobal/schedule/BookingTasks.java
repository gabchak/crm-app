package xyz.thinkglobal.schedule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import xyz.thinkglobal.business.service.LeadToContactConversionService;
import xyz.thinkglobal.services.BookingService;

@Component
public class BookingTasks {

    private static final Logger LOGGER = LoggerFactory.getLogger(LeadToContactConversionService.class);

    private BookingService bookingService;

    @Autowired
    public BookingTasks(BookingService bookingDetailsService) {
        this.bookingService = bookingDetailsService;
    }

    @Scheduled(cron = "${schedule.import-enrollee-from-wq-to-booking.cron}")
    public void importEnrolleesFromWQToBookings() {
        LOGGER.info("BookingTasks @importEnrolleesFromWQToBooking");

        bookingService.importEnrolleesToAllBookings();
    }
}
