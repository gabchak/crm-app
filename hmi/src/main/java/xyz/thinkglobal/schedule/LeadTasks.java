package xyz.thinkglobal.schedule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import xyz.thinkglobal.business.service.LeadToContactConversionService;


@Component
public class LeadTasks {

    private static final Logger LOGGER = LoggerFactory.getLogger(LeadToContactConversionService.class);

    private LeadToContactConversionService leadToContactConversionService;

    @Autowired
    public LeadTasks(LeadToContactConversionService leadToContactConversionService) {
        this.leadToContactConversionService = leadToContactConversionService;
    }


    @Scheduled(cron = "${schedule.notification.cron}")
    public void sendEventEmailNotifications() {
        LOGGER.info("LeadTasks @sendSmsReminderWhenEventStart");

        leadToContactConversionService.sendSmsReminderWhenEventStart();
    }

    @Scheduled(cron = "${schedule.notification.cron}")
    public void updateOnHoldStatus() {
        LOGGER.info("LeadTasks @updateOnHoldStatus");

        leadToContactConversionService.updateLeadToRecall();
    }
}
