package xyz.thinkglobal.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import xyz.thinkglobal.dto.PositionDto;
import xyz.thinkglobal.dto.enums.Positions;
import xyz.thinkglobal.services.PositionService;

import javax.servlet.http.HttpServletRequest;

@Component
public class AuthorisationUtil {


    private final PositionService positionService;
    private final Environment environment;

    @Autowired
    public AuthorisationUtil(PositionService positionService,
                             Environment environment) {
        this.positionService = positionService;
        this.environment = environment;
    }

    public String getCurrentRole(HttpServletRequest request) {
        Long currentPosition = getCurrentPosition(request);
        PositionDto positionDto = positionService.getById(currentPosition);
        return positionDto != null ? Positions.getRoleByPositionName(positionDto.getPosition()) : "null";
    }

    public Long getCurrentPosition(HttpServletRequest request) {
        return Long.valueOf(request
                .getHeader(environment.getProperty("request.header.position")));
    }

    public String hasAccess(Authentication authentication,
                             HttpServletRequest request) {
        String currentRole = getCurrentRole(request);
        return authentication.getAuthorities().contains(new SimpleGrantedAuthority(currentRole))
                ? currentRole : null;
    }
}
