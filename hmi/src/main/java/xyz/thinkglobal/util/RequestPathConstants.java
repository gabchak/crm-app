package xyz.thinkglobal.util;

public class RequestPathConstants {

    public final static String ADDRESS_CRUD = "/addresses";

    public final static String BOOKING_CRUD = "/booking";
    public final static String BOOKING_DETAILS_CRUD = "/booking-details";
    public final static String BOOKING_DETAILS_DELETE_BY_ENROLLEE = "/booking-details/enrollee";

    public final static String CONTACT_CRUD = "/contacts";
    public final static String CONTACT_REGISTER = "/contacts/register";

    public final static String ENROLLEE_CRUD = "/enrollees";
    public final static String ENROLLEE_FIND_BY_CONTACT_ID = "/enrollees/contact";

    public final static String EVENT_CRUD = "/events";
    public final static String EVENT_FINISHED = "/events/finished";
    public final static String EVENT_ACTIVE = "/events/active";
    public final static String EVENT_EXISTS = "/events/exists";
    public final static String EVENT_GET_STATUSES = "/events/statuses";
    public final static String EVENT_GET_TYPES = "/events/types";

    public final static String LEAD_CRUD = "/leads";
    public final static String LEAD_REGISTER = "/leads/register";
    public final static String LEAD_FIND_BY = "/leads/find-by";
    public final static String LEAD_BIND_FOR_EVENT = "/leads/bind-lead-to-event";
    public final static String LEAD_SEND_REGISTRATION_LINK = "/leads/register-link";
    public final static String LEAD_HOW_YOU_FIND_US_SOURCES = "/leads/how-you-find-us";
    public final static String LEAD_GET_STATUSES = "/leads/statuses";
    public final static String LEAD_EVENT_GET_STATUSES = "/leads/events/statuses";

    public final static String SCHOOL_CRUD = "/schools";
    public final static String SCHOOL_FIND_BY_CITY = "/schools/city";
    public final static String SCHOOL_GET_STATUSES = "/schools/statuses";
    public final static String SCHOOL_GET_TERM_OF_PARTNERSHIPS = "/schools/term-of-partnerships";

    public final static String STUDENT_BOOKING = "/students/add-student-to-booking";
    public final static String STUDENT_CRUD = "/students";
    public final static String STUDENT_GET_ALL_BY_CONTACT_ID = "/students/contact";
    public final static String STUDENT_REGISTER = "/students/register";
    public final static String STUDENT_WQ = "/students/add-student-to-waiting-queue";

    public final static String USER_CRUD = "/users";
    public final static String USER_GET_POSITIONS = "/users/positions";
    public final static String USER_DELETE_POSITION_BY_ID = "/users/positions";
    public final static String GET_ROLES = "/get/roles";

    public static final String WAITING_QUEUE_CRUD = "/waiting-list";
    public static final String WAITING_QUEUE_GET_BY_CLASS = "/waiting-list/by-class";

    public static final String WAITING_QUEUE_ENROLLEE_CRUD = "/waiting-list-enrollees";
    public static final String WAITING_QUEUE_ENROLLEE_FIND_BY_WAITING_QUEUE_ID = "/waiting-list-enrollees/waiting-queue";

    public final static String LOGIN = "/login";

    public final static String SWAGGER = "/swagger-ui.html";

    public final static String REGION_CRUD = "/regions";

    public final static String CITY_CRUD = "/cities";

    public final static String STUDY_CLASS_CRUD = "/study-classes";
    public final static String STUDY_CLASS_FIND_BY_SCHOOL = "/study-classes/school";
    public final static String GET_ALL_CHILDREN_BY_STUDY_CLASS_ID = "/study-classes/get-children-by-class/";

    public final static String STUDY_GROUP_CRUD = "/study-groups";

    public final static String SPEAKER_CRUD = "/speakers";

    public static final String CREATE_CONTRACT_BY_TEMPLATE_AND_DATA = "/create-pdf/additional-service-contract";
    public static final String GET_CONTRACT_TYPES = "/get-contract-types";

    public final static String[] PERMIT_ALL_URL = {LOGIN, LEAD_REGISTER, CONTACT_REGISTER};
    public final static String[] SYSTEM_ADMIN_URL = {SCHOOL_CRUD, USER_CRUD, SWAGGER,
            REGION_CRUD, CITY_CRUD, STUDY_CLASS_CRUD, STUDY_CLASS_FIND_BY_SCHOOL, GET_ROLES
    };
    public final static String[] SYSTEM_ADMIN_POST_PUT_DELETE_URL = {SCHOOL_CRUD, USER_CRUD, SWAGGER};
    public final static String[] AUXILARY_GET_URL = {SCHOOL_CRUD, USER_CRUD, SWAGGER};
}
