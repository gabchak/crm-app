package xyz.thinkglobal.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Properties;

@Configuration
@EnableSwagger2
//@EnableWebMvc
@EnableAutoConfiguration
@PropertySources({ @PropertySource(value = "classpath:application.properties", encoding = "UTF-8"),
                   @PropertySource(value = "classpath:properties/message.properties", encoding = "UTF-8"),
                   @PropertySource(value = "classpath:properties/business-process.properties", encoding = "UTF-8"),})
public class ApplicationConfiguration {

    @Autowired
    private Environment environment;

    @Bean
    public JavaMailSender getJavaMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();

        mailSender.setHost(environment.getProperty("spring.mail.host"));
        mailSender.setPort(Integer.valueOf(environment.getProperty("spring.mail.port")));
        mailSender.setDefaultEncoding(environment.getProperty("spring.mail.default-encoding"));
        mailSender.setUsername(environment.getProperty("spring.mail.username"));
        mailSender.setPassword(environment.getProperty("spring.mail.password"));

        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", environment.getProperty("spring.mail.protocol"));
        props.put("mail.smtp.auth", environment.getProperty("spring.mail.properties.mail.smtp.auth"));
        props.put("mail.smtp.starttls.enable", environment.getProperty("spring.mail.properties.mail.smtp.starttls.enable"));
        props.put("mail.debug", environment.getProperty("debug"));
        String socketPort = environment.getProperty("spring.mail.properties.mail.smtp.socketFactory.port");
        String socketFactory = environment.getProperty("spring.mail.properties.mail.smtp.socketFactory.class");
        if (socketPort != null &&
                socketFactory != null) {
            props.put("mail.smtp.socketFactory.port", socketPort);
            props.put("mail.smtp.socketFactory.class", socketFactory);
        }

        return mailSender;
    }

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build();
    }
}
