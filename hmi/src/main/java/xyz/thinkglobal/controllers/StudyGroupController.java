package xyz.thinkglobal.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import xyz.thinkglobal.dto.StudyGroupDto;
import xyz.thinkglobal.services.StudyGroupService;

import java.util.List;
import java.util.Optional;

import static xyz.thinkglobal.util.RequestPathConstants.STUDY_GROUP_CRUD;

@RestController
public class StudyGroupController {

    private StudyGroupService studyGroupService;

    @Autowired
    public StudyGroupController(StudyGroupService studyGroupService) {
        this.studyGroupService = studyGroupService;
    }

    @GetMapping(STUDY_GROUP_CRUD)
    public ResponseEntity<List<StudyGroupDto>> getAllBySchoolId(Long id) {
        return Optional.of(studyGroupService.findAllBySchoolId(id))
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @GetMapping(STUDY_GROUP_CRUD + "{id}")
    public ResponseEntity<StudyGroupDto> getById(@PathVariable Long id) {
        return studyGroupService.findById(id)
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @PostMapping(STUDY_GROUP_CRUD)
    public ResponseEntity<StudyGroupDto> insert(@RequestBody StudyGroupDto studyGroupDto) {
        return Optional.of(studyGroupService.create(studyGroupDto))
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @PutMapping(STUDY_GROUP_CRUD)
    public ResponseEntity<StudyGroupDto> update(@RequestBody StudyGroupDto studyGroupDto) {
        return Optional.of(studyGroupService.save(studyGroupDto))
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @DeleteMapping(STUDY_GROUP_CRUD + "{id}")
    public void deleteById(@PathVariable Long id) {
        studyGroupService.deleteById(id);
    }

}
