package xyz.thinkglobal.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import xyz.thinkglobal.dto.CityDto;
import xyz.thinkglobal.services.CityService;

import java.util.List;
import java.util.Optional;

import static xyz.thinkglobal.util.RequestPathConstants.CITY_CRUD;

@RestController
public class CityController {

    private final CityService cityService;

    @Autowired
    public CityController(CityService cityService) {
        this.cityService = cityService;
    }

    @GetMapping(CITY_CRUD)
    public ResponseEntity<List<CityDto>> getAllByRegionId(@RequestParam("regionId") Long id) {
        return Optional.of(cityService.findAllByRegionId(id))
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @GetMapping(CITY_CRUD + "{id}")
    public ResponseEntity<CityDto> getById(@PathVariable Long id) {
        return cityService.findById(id)
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }
}
