package xyz.thinkglobal.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import xyz.thinkglobal.dto.AuthenticationDto;
import xyz.thinkglobal.dto.PositionDto;
import xyz.thinkglobal.models.Position;
import xyz.thinkglobal.services.PositionService;
import xyz.thinkglobal.services.impl.UserServiceImpl;
import xyz.thinkglobal.util.springUtil.JwtTokenProvider;
import xyz.thinkglobal.services.impl.UserDetailsServiceImpl;
import xyz.thinkglobal.util.RequestPathConstants;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
public class AuthenticationController {

    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider jwtTokenProvider;
    private final UserDetailsServiceImpl userDetailsService;
    private final PositionService positionService;

    @Autowired
    public AuthenticationController(AuthenticationManager authenticationManager,
                                    JwtTokenProvider jwtTokenProvider,
                                    UserDetailsServiceImpl userDetailsService,
                                    PositionService positionService) {
        this.authenticationManager = authenticationManager;
        this.jwtTokenProvider = jwtTokenProvider;
        this.userDetailsService = userDetailsService;
        this.positionService = positionService;
    }

    @PostMapping(RequestPathConstants.LOGIN)
    public ResponseEntity login(@RequestBody AuthenticationDto authenticationDto) {
        String username = authenticationDto.getUsername();
        String password = authenticationDto.getPassword();
        try {
            Model model = new ExtendedModelMap();
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
            Collection<? extends GrantedAuthority> roles =
                    userDetailsService.loadUserByUsername(username).getAuthorities();
            String token = jwtTokenProvider.createToken(username, roles.stream().map(GrantedAuthority::getAuthority)
                    .collect(Collectors.toList()));
            Set<PositionDto> positionDto = positionService.getAllByUserEmail(username);
            model.addAttribute("username", username)
                    .addAttribute("token", token)
                    .addAttribute("positions", positionDto);
            return ResponseEntity.ok(model);
        } catch (UsernameNotFoundException e) {
            throw new BadCredentialsException("No user with name \"" + username + "\" was found.");
        } catch (AuthenticationException e) {
            throw new BadCredentialsException("Invalid username or password supplied.");
        }
    }

}
