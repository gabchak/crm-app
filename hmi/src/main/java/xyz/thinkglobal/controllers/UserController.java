package xyz.thinkglobal.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import xyz.thinkglobal.dto.UserDto;
import xyz.thinkglobal.dto.enums.Positions;
import xyz.thinkglobal.dto.enums.UserRole;
import xyz.thinkglobal.services.PositionService;
import xyz.thinkglobal.services.UserService;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import static xyz.thinkglobal.util.RequestPathConstants.GET_ROLES;
import static xyz.thinkglobal.util.RequestPathConstants.USER_CRUD;
import static xyz.thinkglobal.util.RequestPathConstants.USER_DELETE_POSITION_BY_ID;
import static xyz.thinkglobal.util.RequestPathConstants.USER_GET_POSITIONS;

@RestController
@PreAuthorize("hasRole('ROLE_SYSTEM_ADMINISTRATOR')")
public class UserController {

    private UserService userService;
    private final PositionService positionService;

    @Autowired
    public UserController(UserService userService, PositionService positionService) {
        this.userService = userService;
        this.positionService = positionService;
    }

    @GetMapping(USER_CRUD)
    public ResponseEntity<List<UserDto>> getAll() {
        return Optional.of(userService.findAll())
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @GetMapping(USER_CRUD + "/{id}")
    public ResponseEntity<UserDto> getById(@PathVariable Long id) {
        return userService.findById(id)
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @PostMapping(USER_CRUD)
    public ResponseEntity<UserDto> insert(@RequestBody UserDto userDto) {
        return Optional.of(userService.save(userDto))
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @PutMapping(USER_CRUD)
    public ResponseEntity<UserDto> update(@RequestBody UserDto userDto) {
        return Optional.of(userService.save(userDto))
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @DeleteMapping(USER_CRUD)
    public void deleteById(Long id) {
        userService.deleteById(id);
    }

    @GetMapping(GET_ROLES)
    public ResponseEntity<UserRole[]> getRoles() {
        return ResponseEntity.ok(UserRole.values());
    }

    @GetMapping(USER_GET_POSITIONS)
    public ResponseEntity<Map<String, String>> getPositions() {
        return ResponseEntity.ok(Positions.getNamesValuesForRegistration());
    }

    @DeleteMapping(USER_DELETE_POSITION_BY_ID)
    public void deletePositionById(@RequestParam Long positionId) {
        positionService.deleteById(positionId);
    }

}
