package xyz.thinkglobal.controllers;

import freemarker.template.TemplateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import xyz.thinkglobal.Exception.EventDoesNotExist;
import xyz.thinkglobal.Exception.EventFinishedException;
import xyz.thinkglobal.business.service.LeadToContactConversionService;
import xyz.thinkglobal.dto.EventDto;
import xyz.thinkglobal.dto.EventFinishedDto;
import xyz.thinkglobal.dto.enums.EventStatus;
import xyz.thinkglobal.dto.enums.EventType;
import xyz.thinkglobal.services.EventService;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static xyz.thinkglobal.util.RequestPathConstants.*;

@RestController
public class EventController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ContactController.class);

    private EventService eventService;
    private LeadToContactConversionService leadToContactConversionService;

    @Autowired
    public EventController(EventService eventService,
                           LeadToContactConversionService leadToContactConversionService) {
        this.eventService = eventService;
        this.leadToContactConversionService = leadToContactConversionService;
    }

    @PostMapping(EVENT_FINISHED)
    public ResponseEntity closeEventAndMarkVisitedLeads(@RequestBody EventFinishedDto eventFinishedDto) {
        try {
            leadToContactConversionService
                    .closeEventAndMarkLeads(eventFinishedDto.getEventId(), eventFinishedDto.getVisitedLeads());
            return ResponseEntity.ok().build();
        } catch (TemplateException | IOException e) {
            return ResponseEntity.status(400).body(e.getMessage());
        } catch (EventFinishedException e) {
            LOGGER.error("Error. Tried bind leads to finished event id=" + eventFinishedDto.getEventId());
            return ResponseEntity.status(400).body("Error. You can't bind leads to finished event.");
        } catch (EventDoesNotExist e) {
            LOGGER.error("Error. Tried bind leads to nonexistent event id=" + eventFinishedDto.getEventId());
            return ResponseEntity.status(400).body("Error. Event doesn't exist! You can't bind leads to nonexistent event.");
        } catch (MessagingException e) {
            LOGGER.error("Error. Message didn't send.");
            return ResponseEntity.status(400).body("Error. Message didn't send.");
        }
    }

    @GetMapping(EVENT_CRUD)
    public ResponseEntity<List<EventDto>> getAll() {  // TODO get with pagination
        return Optional.of(eventService.findAll())
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @GetMapping(EVENT_ACTIVE)
    public ResponseEntity<List<EventDto>> getAllActive() {
        return Optional.of(eventService.findAllActive())
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @GetMapping(EVENT_CRUD + "/{id}")
    public ResponseEntity<EventDto> getById(@PathVariable Long id) {
        return eventService.findById(id)
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @PostMapping(EVENT_CRUD)
    public ResponseEntity<EventDto> insert(@RequestBody EventDto eventDto) {
        return Optional.of(eventService.save(eventDto))
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @PutMapping(EVENT_CRUD)
    public ResponseEntity<EventDto> update(@RequestBody EventDto eventDto) {
        return Optional.of(eventService.save(eventDto))
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @DeleteMapping(EVENT_CRUD + "/{id}")
    public void deleteById(@PathVariable Long id) {
        eventService.deleteById(id);
    }

    @GetMapping(EVENT_EXISTS + "{id}")
    public boolean isEventExist(@PathVariable Long id) {
        return eventService.findById(id).isPresent();
    }

    @GetMapping(EVENT_GET_STATUSES)
    public Map<String, EventStatus> getEventsStatuses() {
        return EventStatus.getNamesValues();
    }

    @GetMapping(EVENT_GET_TYPES)
    public Map<String, EventType> getEventsTypes() {
        return EventType.getNamesValues();
    }
}
