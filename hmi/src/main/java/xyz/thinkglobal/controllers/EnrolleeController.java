package xyz.thinkglobal.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import xyz.thinkglobal.dto.EnrolleeDto;
import xyz.thinkglobal.services.EnrolleeService;

import java.util.List;
import java.util.Optional;

import static xyz.thinkglobal.util.RequestPathConstants.ENROLLEE_CRUD;
import static xyz.thinkglobal.util.RequestPathConstants.ENROLLEE_FIND_BY_CONTACT_ID;

@RestController
public class EnrolleeController {

    private final EnrolleeService enrolleeService;

    @Autowired
    public EnrolleeController(EnrolleeService enrolleeService) {
        this.enrolleeService = enrolleeService;
    }

    @GetMapping(ENROLLEE_FIND_BY_CONTACT_ID + "/{id}")
    public ResponseEntity<List<EnrolleeDto>> getEnrolleesByContactId(@PathVariable Long id) {
        return Optional.of(enrolleeService.findAllByContactId(id))
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @GetMapping(ENROLLEE_CRUD + "/{id}")
    public ResponseEntity<EnrolleeDto> getById(@PathVariable Long id) {
        return enrolleeService.findById(id)
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @GetMapping(ENROLLEE_CRUD)
    public ResponseEntity<List<EnrolleeDto>> getAll() {
        return Optional.of(enrolleeService.findAll())
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @PostMapping(ENROLLEE_CRUD)
    public ResponseEntity<EnrolleeDto> insert(@RequestBody EnrolleeDto enrolleeDto) {
        return Optional.of(enrolleeService.save(enrolleeDto))
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @PutMapping(ENROLLEE_CRUD)
    public ResponseEntity<EnrolleeDto> update(@RequestBody EnrolleeDto enrolleeDto) {
        return Optional.of(enrolleeService.save(enrolleeDto))
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @DeleteMapping(ENROLLEE_CRUD)
    public void deleteById(Long id) {
        enrolleeService.deleteById(id);
    }

}
