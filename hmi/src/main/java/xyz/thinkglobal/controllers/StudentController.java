package xyz.thinkglobal.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import xyz.thinkglobal.dto.StudentDto;
import xyz.thinkglobal.services.StudentService;

import java.util.List;
import java.util.Optional;

import static xyz.thinkglobal.util.RequestPathConstants.STUDENT_CRUD;
import static xyz.thinkglobal.util.RequestPathConstants.STUDENT_GET_ALL_BY_CONTACT_ID;

@RestController
public class StudentController {

    private StudentService studentService;

    @Autowired
    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @PostMapping(STUDENT_CRUD)
    public ResponseEntity<StudentDto> insert(@RequestBody StudentDto studentDto) {
        return Optional.of(studentService.save(studentDto))
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @GetMapping(STUDENT_CRUD)
    public ResponseEntity<List<StudentDto>> getAll() {
        return Optional.of(studentService.findAll())
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @GetMapping(STUDENT_CRUD + "/{id}")
    public ResponseEntity<StudentDto> getById(@PathVariable Long id) {
        return studentService.findById(id)
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @PutMapping(STUDENT_CRUD)
    public ResponseEntity<StudentDto> update(@RequestBody StudentDto studentDto) {
        return Optional.of(studentService.save(studentDto))
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @DeleteMapping(STUDENT_CRUD + "/{id}")
    public void deleteById(@PathVariable Long id) {
        studentService.deleteById(id);
    }

    @GetMapping(STUDENT_GET_ALL_BY_CONTACT_ID + "/{id}")
    public ResponseEntity<List<StudentDto>> getStudentsByContactId(@PathVariable Long id) {
        return Optional.of(studentService.findAllByContactId(id))
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

}
