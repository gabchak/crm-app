package xyz.thinkglobal.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import xyz.thinkglobal.dto.AddressDto;
import xyz.thinkglobal.services.AddressService;

import java.util.Optional;

import static xyz.thinkglobal.util.RequestPathConstants.ADDRESS_CRUD;

@RestController
public class AddressController {

    private final AddressService addressService;

    @Autowired
    public AddressController(AddressService addressService) {
        this.addressService = addressService;
    }

    @GetMapping(ADDRESS_CRUD + "{id}")
    public ResponseEntity<AddressDto> getById(@PathVariable Long id) {
        return addressService.findById(id)
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @PostMapping(ADDRESS_CRUD)
    public ResponseEntity<AddressDto> insert(@RequestBody AddressDto addressDto) {
        return Optional.of(addressService.save(addressDto))
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @PutMapping(ADDRESS_CRUD)
    public ResponseEntity<AddressDto> update(@RequestBody AddressDto addressDto) {
        return Optional.of(addressService.save(addressDto))
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @DeleteMapping(ADDRESS_CRUD)
    public void deleteById(Long id) {
        addressService.deleteById(id);
    }

}
