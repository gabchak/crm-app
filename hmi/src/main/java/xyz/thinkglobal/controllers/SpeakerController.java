package xyz.thinkglobal.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import xyz.thinkglobal.dto.SpeakerDto;
import xyz.thinkglobal.services.SpeakerService;

import java.util.List;
import java.util.Optional;

import static xyz.thinkglobal.util.RequestPathConstants.SPEAKER_CRUD;

//@RestController
public class SpeakerController {

    private final SpeakerService speakerService;

    @Autowired
    public SpeakerController(SpeakerService speakerService) {
        this.speakerService = speakerService;
    }

    @GetMapping(SPEAKER_CRUD + "/{id}")
    public ResponseEntity<SpeakerDto> getById(@PathVariable Long id) {
        return speakerService.findById(id)
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @GetMapping(SPEAKER_CRUD)
    public ResponseEntity<List<SpeakerDto>> getAll() {
        return Optional.of(speakerService.findAll())
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @PostMapping(SPEAKER_CRUD)
    public ResponseEntity<SpeakerDto> insert(@RequestBody SpeakerDto speakerDto) {
        return Optional.of(speakerService.save(speakerDto))
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @PutMapping(SPEAKER_CRUD)
    public ResponseEntity<SpeakerDto> update(@RequestBody SpeakerDto speakerDto) {
        return Optional.of(speakerService.save(speakerDto))
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @DeleteMapping(SPEAKER_CRUD)
    public void deleteById(Long id) {
        speakerService.deleteById(id);
    }
}
