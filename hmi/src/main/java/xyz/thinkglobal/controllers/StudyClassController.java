package xyz.thinkglobal.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import xyz.thinkglobal.dto.ChildDto;
import xyz.thinkglobal.dto.StudyClassDto;
import xyz.thinkglobal.dto.filters.ChildrenFilterDto;
import xyz.thinkglobal.services.StudyClassService;
import xyz.thinkglobal.util.AuthorisationUtil;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

import static xyz.thinkglobal.util.RequestPathConstants.GET_ALL_CHILDREN_BY_STUDY_CLASS_ID;
import static xyz.thinkglobal.util.RequestPathConstants.STUDY_CLASS_CRUD;
import static xyz.thinkglobal.util.RequestPathConstants.STUDY_CLASS_FIND_BY_SCHOOL;

@RestController
public class StudyClassController {

    private StudyClassService studyClassService;

    @Autowired
    private AuthorisationUtil authorisationUtil;

    @Autowired
    public StudyClassController(StudyClassService studyClassService) {
        this.studyClassService = studyClassService;
    }

    @GetMapping(STUDY_CLASS_CRUD)
    public ResponseEntity<List<StudyClassDto>> getAllBySchoolId(@RequestParam("schoolId") Long id) {
        return Optional.of(studyClassService.findAllBySchoolId(id))
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @GetMapping(STUDY_CLASS_CRUD + "{id}")
    public ResponseEntity<StudyClassDto> getById(@PathVariable Long id) {
        return studyClassService.findById(id)
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @PostMapping(STUDY_CLASS_CRUD)
    public ResponseEntity<StudyClassDto> insert(@RequestBody StudyClassDto studyClassDto) {
        return Optional.of(studyClassService.save(studyClassDto))
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @PutMapping(STUDY_CLASS_CRUD)
    public ResponseEntity<StudyClassDto> update(@RequestBody StudyClassDto studyClassDto) {
        return Optional.of(studyClassService.save(studyClassDto))
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @DeleteMapping(STUDY_CLASS_CRUD + "{id}")
    public void deleteById(@PathVariable Long id) {
        studyClassService.deleteById(id);
    }

    @GetMapping(STUDY_CLASS_FIND_BY_SCHOOL + "{id}")
    public ResponseEntity<List<StudyClassDto>> getClassListWhichHaveWaitingQueueBySchool(@PathVariable Long id) {
        return Optional.of(studyClassService.findAllBySchoolIdAndWaitingQueueExists(id))
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @GetMapping(GET_ALL_CHILDREN_BY_STUDY_CLASS_ID + "{id}")
    public ResponseEntity getChildrenByClassId(@PathVariable Long id, @NotNull Pageable pageable,
                                               ChildrenFilterDto childrenFilterDto,
                                               Authentication authentication,
                                               HttpServletRequest request) {
        Long positionId = authorisationUtil.getCurrentPosition(request);
        childrenFilterDto.setPositionId(positionId);
        List<ChildDto> childList =
                studyClassService.findChildrenByPositionIn(childrenFilterDto, pageable);
        return ResponseEntity.ok(childList);
    }

}
