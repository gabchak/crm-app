package xyz.thinkglobal.controllers;

import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import xyz.thinkglobal.dto.enums.ContractType;
import xyz.thinkglobal.dto.pdf.ContractDto;
import xyz.thinkglobal.services.pdf.PdfGeneratorService;
import xyz.thinkglobal.util.MediaTypeUtils;
import xyz.thinkglobal.util.RequestPathConstants;

import javax.servlet.ServletContext;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@RestController
@PreAuthorize("permitAll()")
public class PdfController {
    private final PdfGeneratorService pdfGeneratorService;
    private final ServletContext servletContext;

    @Autowired
    public PdfController(PdfGeneratorService pdfGeneratorService,
                         ServletContext servletContext) {
        this.pdfGeneratorService = pdfGeneratorService;
        this.servletContext = servletContext;
    }


    @PostMapping(RequestPathConstants.CREATE_CONTRACT_BY_TEMPLATE_AND_DATA)
    public ResponseEntity<Object> getAdditionalServicePdf(@RequestBody ContractDto contractDto, int contractType) {
        Map<String, Object> map = new HashMap<>(1);
        map.put("contractDto", contractDto);
        File file;
        try {
            file = pdfGeneratorService.getPdfFileFromTemplate(map, ContractType.values()[contractType].getTemplate());
        } catch (IOException e) {
            return ResponseEntity.status(500).body("IOException");
        } catch (TemplateException e) {
            return ResponseEntity.status(500).body("TemplateException");
        } catch (InterruptedException e) {
            return ResponseEntity.status(500).body("InterruptedException");
        }
        MediaType mediaType = MediaTypeUtils.getMediaTypeForFileName(this.servletContext.getMimeType(file.getName()));
        InputStreamResource inputStreamResource = null;
        try {
            inputStreamResource = new InputStreamResource(new FileInputStream(file));
        } catch (FileNotFoundException e) {
            return ResponseEntity.status(500).body("FileNotFoundException");
        }
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getName() + "\"")
                .contentType(mediaType).contentLength(file.length()).body(inputStreamResource);
    }

    @GetMapping(RequestPathConstants.GET_CONTRACT_TYPES)
    public ResponseEntity<Map<Integer, String>> getContractTypes() {
        return ResponseEntity.ok().body(ContractType.getAllWithDescription());
    }
}
