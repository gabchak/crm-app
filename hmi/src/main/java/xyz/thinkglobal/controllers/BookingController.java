package xyz.thinkglobal.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import xyz.thinkglobal.dto.BookingDto;
import xyz.thinkglobal.dto.BookingWithBookingDetailsDto;
import xyz.thinkglobal.services.BookingService;

import java.util.List;
import java.util.Optional;

import static xyz.thinkglobal.util.RequestPathConstants.BOOKING_CRUD;

@RestController
public class BookingController {

    private BookingService bookingService;

    @Autowired
    public BookingController(BookingService bookingService) {
        this.bookingService = bookingService;
    }

    @GetMapping(BOOKING_CRUD + "/{id}")
    public ResponseEntity<BookingDto> getById(@PathVariable Long id) {
        return bookingService.findById(id)
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @GetMapping(BOOKING_CRUD)
    public ResponseEntity<List<BookingDto>> getAll() {
        return Optional.of(bookingService.findAll())
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @PostMapping(BOOKING_CRUD)
    public ResponseEntity<BookingWithBookingDetailsDto> insert(@RequestBody BookingDto bookingDto) {
        BookingWithBookingDetailsDto savedBookingDto = bookingService.create(bookingDto);
        return Optional.of(savedBookingDto)
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.badRequest()::build);
    }

    @PutMapping(BOOKING_CRUD)
    public ResponseEntity<BookingDto> update(@RequestBody BookingDto bookingDto) {
        return Optional.of(bookingService.save(bookingDto))
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.badRequest()::build);
    }

    @DeleteMapping(BOOKING_CRUD)
    public void deleteById(Long id) {
        bookingService.deleteById(id);
    }
}
