package xyz.thinkglobal.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import xyz.thinkglobal.dto.SchoolDto;
import xyz.thinkglobal.dto.enums.SchoolStatus;
import xyz.thinkglobal.dto.enums.TermOfPartnership;
import xyz.thinkglobal.services.SchoolService;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import static xyz.thinkglobal.util.RequestPathConstants.*;

@RestController
public class SchoolController {

    private final SchoolService schoolService;

    @Autowired
    public SchoolController(SchoolService schoolService) {
        this.schoolService = schoolService;
    }

    @GetMapping(SCHOOL_CRUD + "/{id}")
    public ResponseEntity<SchoolDto> getById(@PathVariable Long id) {
        return schoolService.findById(id)
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @GetMapping(SCHOOL_CRUD)
    public ResponseEntity<List<SchoolDto>> getAll() {
        return Optional.of(schoolService.findAll())
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @PostMapping(SCHOOL_CRUD)
    public ResponseEntity<SchoolDto> insert(@RequestBody SchoolDto schoolDto) {
        return Optional.of(schoolService.save(schoolDto))
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @PutMapping(SCHOOL_CRUD)
    public ResponseEntity<SchoolDto> update(@RequestBody SchoolDto schoolDto) {
        return Optional.of(schoolService.save(schoolDto))
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @DeleteMapping(SCHOOL_CRUD + "/{id}")
    public void deleteById(@PathVariable Long id) {
        schoolService.deleteById(id);
    }

    @GetMapping(SCHOOL_FIND_BY_CITY + "/{id}")
    public ResponseEntity<List<SchoolDto>> getSchoolListByCity(@PathVariable Long id) {
        return Optional.of(schoolService.findAllByCityId(id))
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @GetMapping(SCHOOL_GET_STATUSES)
    public Map<String, SchoolStatus> getAllSchoolsStatuses() {
        return SchoolStatus.getNamesValues();
    }

    @GetMapping(SCHOOL_GET_TERM_OF_PARTNERSHIPS)
    public Map<String, TermOfPartnership> getSchoolsTermOfPartnerships() {
        return TermOfPartnership.getNamesValues();
    }
}
