package xyz.thinkglobal.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import xyz.thinkglobal.Exception.UserIsAlreadyExistsException;
import xyz.thinkglobal.business.service.LeadToContactConversionService;
import xyz.thinkglobal.dto.ContactDto;
import xyz.thinkglobal.dto.LeadDto;
import xyz.thinkglobal.services.ContactService;
import xyz.thinkglobal.services.LeadService;
import xyz.thinkglobal.util.AuthorisationUtil;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

import static xyz.thinkglobal.util.RequestPathConstants.CONTACT_CRUD;
import static xyz.thinkglobal.util.RequestPathConstants.CONTACT_REGISTER;

@RestController
public class ContactController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ContactController.class);

    private final ContactService contactService;
    private final LeadService leadService;
    private final LeadToContactConversionService leadToContactConversionService;
    private final AuthorisationUtil authorisationUtil;


    @Autowired
    public ContactController(ContactService contactService,
                             LeadService leadService,
                             LeadToContactConversionService leadToContactConversionService,
                             AuthorisationUtil authorisationUtil) {
        this.contactService = contactService;
        this.leadService = leadService;
        this.leadToContactConversionService = leadToContactConversionService;
        this.authorisationUtil = authorisationUtil;
    }

    @PostMapping(CONTACT_REGISTER)
    public ResponseEntity register(@RequestBody ContactDto contactDto, HttpServletRequest request,
                                   Authentication authentication, @RequestParam String em) {

        Optional<LeadDto> leadDto = leadService.findByEmail(contactDto.getEmail());

        if (!leadDto.isPresent()) {
            LOGGER.error("Attempt to register with not existing lead: " + contactDto.getEmail());
            return ResponseEntity.status(400).body("error=wrong url" + contactDto.getEmail());
        } else if (contactService.findByEmail(contactDto.getEmail()).isPresent()) {
            LOGGER.error("Contact with email \"" + contactDto.getEmail() + "\" already exists");
            return ResponseEntity.status(400).body("error=contact exists");
        }

        if (!hasAccess(leadDto.get(), em, authentication, request)) {
            return ResponseEntity.status(400).body("error=wrong url");
        }
        try {
            return ResponseEntity.ok(leadToContactConversionService.registerContact(contactDto));
        } catch (UserIsAlreadyExistsException e) {
            LOGGER.error("Attempt to register with existing email: " + contactDto.getEmail());
            return ResponseEntity.status(400).body(e.getMessage());
        }
    }

    private boolean hasAccess(LeadDto leadDto, String em, Authentication authentication,
                             HttpServletRequest request) {
        String currentRole = authorisationUtil.getCurrentRole(request);
        Long leadId = leadDto.getId();
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        boolean hasRequiredAuthorities =
                authentication.getAuthorities().contains(new SimpleGrantedAuthority(currentRole));
        boolean isValidHash = encoder.matches(leadId + leadDto.getEmail(), em);
        return hasRequiredAuthorities && isValidHash;
    }


    @PostMapping(CONTACT_CRUD)
    @PreAuthorize("hasAnyRole('ROLE_SCHOOL_ADMINISTRATOR', 'ROLE_SYSTEM_ADMINISTRATOR')")
    public ResponseEntity<ContactDto> insert(@RequestBody ContactDto contactDto) {
        return Optional.of(contactService.save(contactDto))
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @GetMapping(CONTACT_CRUD)
    public ResponseEntity<List<ContactDto>> getAll() {
        return Optional.of(contactService.findAll())
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @GetMapping(CONTACT_CRUD + "/{id}")
    public ResponseEntity<ContactDto> getById(@PathVariable Long id) {
        return contactService.findById(id)
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @PutMapping(CONTACT_CRUD)
    public ResponseEntity<ContactDto> update(@RequestBody ContactDto contactDto) {
        return Optional.of(contactService.save(contactDto))
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @DeleteMapping(CONTACT_CRUD + "/{id}")
    public void deleteById(@PathVariable Long id) {
        contactService.deleteById(id);
    }

}
