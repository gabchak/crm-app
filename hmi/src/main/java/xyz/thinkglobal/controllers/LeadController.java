package xyz.thinkglobal.controllers;

import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import xyz.thinkglobal.Exception.EventDoesNotExist;
import xyz.thinkglobal.Exception.EventFinishedException;
import xyz.thinkglobal.Exception.LeadNotExistException;
import xyz.thinkglobal.dto.LeadDto;
import xyz.thinkglobal.dto.enums.HowYouFindUs;
import xyz.thinkglobal.dto.enums.LeadEventStatus;
import xyz.thinkglobal.dto.enums.LeadStatus;
import xyz.thinkglobal.dto.filters.LeadFilterDto;
import xyz.thinkglobal.services.LeadEventService;
import xyz.thinkglobal.services.LeadService;

import javax.mail.MessagingException;
import javax.websocket.server.PathParam;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static xyz.thinkglobal.util.RequestPathConstants.LEAD_BIND_FOR_EVENT;
import static xyz.thinkglobal.util.RequestPathConstants.LEAD_CRUD;
import static xyz.thinkglobal.util.RequestPathConstants.LEAD_EVENT_GET_STATUSES;
import static xyz.thinkglobal.util.RequestPathConstants.LEAD_FIND_BY;
import static xyz.thinkglobal.util.RequestPathConstants.LEAD_GET_STATUSES;
import static xyz.thinkglobal.util.RequestPathConstants.LEAD_HOW_YOU_FIND_US_SOURCES;
import static xyz.thinkglobal.util.RequestPathConstants.LEAD_REGISTER;
import static xyz.thinkglobal.util.RequestPathConstants.LEAD_SEND_REGISTRATION_LINK;

@RestController
public class LeadController {

    private LeadService leadService;
    private LeadEventService leadEventService;

    @Autowired
    public LeadController(LeadService leadService, LeadEventService leadEventService) {
        this.leadService = leadService;
        this.leadEventService = leadEventService;
    }


    @PostMapping(LEAD_REGISTER)
    public ResponseEntity register(@RequestBody LeadDto leadDto, @RequestParam Long eventId) {
        try {
            return ResponseEntity.ok(leadEventService.registerLead(leadDto, eventId));

        } catch (MessagingException | TemplateException | IOException  e) {
            return ResponseEntity.status(400).body(e.getMessage());
        } catch (LeadNotExistException e) {
            return ResponseEntity.status(400).body("Error. Lead doesn't exists!");
        } catch (EventDoesNotExist e) {
            return ResponseEntity.status(400).body("Error. Event doesn't exists!");
        } catch (EventFinishedException e) {
            return ResponseEntity.status(400).body("Error. Event finished already!");
        }
    }

    @PostMapping(LEAD_BIND_FOR_EVENT)
    public ResponseEntity bindLeadForEvent(@RequestParam Long leadId, @RequestParam Long eventId) {

        try {
            return ResponseEntity.ok(leadEventService.bindLeadToEvent(leadId, eventId));
        } catch (LeadNotExistException e) {
            return ResponseEntity.status(400).body("Error. Lead doesn't exists!");
        } catch (EventDoesNotExist e) {
            return ResponseEntity.status(400).body("Error. Event doesn't exists!");
        } catch (EventFinishedException e) {
            return ResponseEntity.status(400).body("Error. Event finished already!");
        }

    }

    @GetMapping(LEAD_FIND_BY)
    public ResponseEntity<List<LeadDto>> leadFindBy(@PathParam("email") String email,
                                                    @PathParam("firstName") String firstName,
                                                    @PathParam("lastName") String lastName,
                                                    @PathParam("phone") String phone) {
        LeadFilterDto leadFilterDto = new LeadFilterDto();
        leadFilterDto.setEmail(email);
        leadFilterDto.setFirstName(firstName);
        leadFilterDto.setLastName(lastName);
        leadFilterDto.setPhone(phone);
        return Optional.of(leadService.findBy(leadFilterDto))
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @GetMapping(LEAD_SEND_REGISTRATION_LINK)
    public void sendRegistrationLink(@PathParam("email") String email,
                                     @PathParam("phone") String phone,
                                     @PathParam("eventId") Long eventId) {
        if (!email.isEmpty()) {
            try {
                leadService.sendEmailWithRegistrationLink(email, eventId);
            } catch (TemplateException | IOException | MessagingException e) {
                e.printStackTrace();
            }
        } else {
            leadService.sendMessageWithRegistrationLink(phone, eventId);
        }
    }

    @GetMapping(LEAD_CRUD)
    public ResponseEntity<List<LeadDto>> getAll() {
        return Optional.of(leadService.findAll())
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @GetMapping(LEAD_CRUD + "/{id}")
    public ResponseEntity<LeadDto> getById(@PathVariable Long id) {
        return leadService.findById(id)
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @PostMapping(LEAD_CRUD)
    public ResponseEntity<LeadDto> insert(@RequestBody LeadDto leadDto) {
        return Optional.of(leadService.save(leadDto))
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @PutMapping(LEAD_CRUD)
    public ResponseEntity<LeadDto> update(@RequestBody LeadDto lead) {
        return Optional.of(leadService.save(lead))
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @DeleteMapping(LEAD_CRUD + "/{id}")
    public void deleteById(@PathVariable Long id) {
        leadService.deleteById(id);
    }

    @GetMapping(LEAD_HOW_YOU_FIND_US_SOURCES)
    public Map<String, HowYouFindUs> getAllHowYouFindUsSources() {
        return HowYouFindUs.getNamesValuesMap();
    }

    @GetMapping(LEAD_GET_STATUSES)
    public Map<String, LeadStatus> getAllLeadStatuses() {
        return LeadStatus.getNamesValues();
    }

    @GetMapping(LEAD_EVENT_GET_STATUSES)
    public Map<String, LeadEventStatus> getLeadEventStatuses() {
        return LeadEventStatus.getNamesValues();
    }

}
