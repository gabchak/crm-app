package xyz.thinkglobal.controllers.advice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.ControllerAdvice;
import xyz.thinkglobal.dto.PositionDto;
import xyz.thinkglobal.services.PositionService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@ControllerAdvice
public class GlobalControllerAdvice {

    private final PositionService positionService;
    private final Environment environment;

    @Autowired
    public GlobalControllerAdvice(PositionService positionService,
                             Environment environment) {
        this.positionService = positionService;
        this.environment = environment;
    }

//    @ModelAttribute
    public void setPositionDateByHeaderPosition(HttpServletRequest request) {
        String positionString = request.getHeader(environment.getProperty("request.header.position"));
        if (positionString != null &&
           (request.getAttribute("position") == null ||
            request.getAttribute("positionList") == null)) {
        Long positionId = Long.valueOf(positionString);
            List<PositionDto> positionDtoList = positionService.getAllRelatedPositionsById(positionId);
            request.setAttribute("position", positionId);
            request.setAttribute("positionList", positionDtoList);
        }
    }
}
