package xyz.thinkglobal.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import xyz.thinkglobal.dto.BookingDetailsDto;
import xyz.thinkglobal.services.BookingDetailsService;

import java.util.List;
import java.util.Optional;

import static xyz.thinkglobal.util.RequestPathConstants.BOOKING_DETAILS_CRUD;
import static xyz.thinkglobal.util.RequestPathConstants.BOOKING_DETAILS_DELETE_BY_ENROLLEE;

@RestController
public class BookingDetailsController {

    private BookingDetailsService bookingDetailsService;

    @Autowired
    public BookingDetailsController(BookingDetailsService bookingDetailsService) {
        this.bookingDetailsService = bookingDetailsService;
    }

    @PostMapping(BOOKING_DETAILS_CRUD)
    public ResponseEntity<BookingDetailsDto> insert(@RequestBody BookingDetailsDto bookingDetailsDto) {
        return Optional.of(bookingDetailsService.save(bookingDetailsDto))
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.badRequest()::build);
    }

    @PutMapping(BOOKING_DETAILS_CRUD)
    public ResponseEntity<BookingDetailsDto> update(@RequestBody BookingDetailsDto bookingDetailsDto) {
        return Optional.of(bookingDetailsService.save(bookingDetailsDto))
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.badRequest()::build);
    }

    @GetMapping(BOOKING_DETAILS_CRUD + "/{id}")
    public ResponseEntity<BookingDetailsDto> getById(@PathVariable Long id) {
        return bookingDetailsService.findById(id)
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @GetMapping(BOOKING_DETAILS_CRUD)
    public ResponseEntity<List<BookingDetailsDto>> getAll() {
        return Optional.of(bookingDetailsService.findAll())
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @DeleteMapping(BOOKING_DETAILS_CRUD)
    public void deleteById(@RequestParam Long id) {
        bookingDetailsService.deleteById(id);
    }

    @DeleteMapping(BOOKING_DETAILS_DELETE_BY_ENROLLEE)
    public void deleteByEnrolleeId(@RequestParam Long enrolleeId) {
        bookingDetailsService.deleteByEnrolleeId(enrolleeId);
    }
}
