package xyz.thinkglobal.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import xyz.thinkglobal.dto.WaitingQueueDto;
import xyz.thinkglobal.dto.enums.UserRole;
import xyz.thinkglobal.services.WaitingQueueService;
import xyz.thinkglobal.util.AuthorisationUtil;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

import static xyz.thinkglobal.util.RequestPathConstants.WAITING_QUEUE_CRUD;
import static xyz.thinkglobal.util.RequestPathConstants.WAITING_QUEUE_GET_BY_CLASS;

@RestController
public class WaitingQueueController {

    private final WaitingQueueService waitingQueueService;
    private final AuthorisationUtil authorisationUtil;


    @Autowired
    public WaitingQueueController(WaitingQueueService waitingQueueService,
                                  AuthorisationUtil authorisationUtil) {
        this.waitingQueueService = waitingQueueService;
        this.authorisationUtil = authorisationUtil;
    }

    @PreAuthorize("hasRole('ROLE_SYSTEM_ADMINISTRATOR')")
    @PostMapping(WAITING_QUEUE_CRUD)
    public ResponseEntity<WaitingQueueDto> insertWaitingList(@RequestBody WaitingQueueDto waitingQueueDto) {
        WaitingQueueDto resultWaitingQueue = waitingQueueService.save(waitingQueueDto);
        return ResponseEntity.ok(resultWaitingQueue);
    }

    @PreAuthorize("hasAnyRole('ROLE_SYSTEM_ADMINISTRATOR', 'ROLE_SCHOOL_ADMINISTRATOR')")
    @GetMapping(WAITING_QUEUE_CRUD + "/{id}")
    public ResponseEntity getById(@PathVariable Long id,
                                                   HttpServletRequest request) {

        Long currentPosition = authorisationUtil.getCurrentPosition(request);
        try {
            if (waitingQueueService.hasAccessToWaitingQueue(currentPosition, id)) {
                return waitingQueueService.findById(id)
                        .map(ResponseEntity::ok)
                        .orElseGet(ResponseEntity.notFound()::build);
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            return ResponseEntity.status(500).body("Position didn't find");
        }
        return ResponseEntity.notFound().build();
    }


    @PreAuthorize("hasAnyRole('ROLE_SYSTEM_ADMINISTRATOR', 'ROLE_SCHOOL_ADMINISTRATOR')")
    @GetMapping(WAITING_QUEUE_GET_BY_CLASS + "/{classId}")
    public ResponseEntity getWaitingListByClassId(@PathVariable Long classId,
                                                  HttpServletRequest request) {
        Long currentPosition = authorisationUtil.getCurrentPosition(request);

        try {
            if (waitingQueueService.hasAccessToStudyClass(currentPosition, classId)) {
                Optional<WaitingQueueDto> waitingQueueDto = waitingQueueService.findByClassId(classId);
                return ResponseEntity.ok(waitingQueueDto);
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            return ResponseEntity.status(500).body("Position didn't find.");
        }
        return ResponseEntity.notFound().build();
    }

    @PreAuthorize("hasAnyRole('ROLE_SYSTEM_ADMINISTRATOR', 'ROLE_SCHOOL_ADMINISTRATOR')")
    @GetMapping(WAITING_QUEUE_CRUD)
    public ResponseEntity getAll(Authentication authentication,
                                 HttpServletRequest request) {
        List<WaitingQueueDto> result;

        String hasAccess = authorisationUtil.hasAccess(authentication, request);
        if (hasAccess == null) {
            return ResponseEntity.status(403).body("Wrong position");
        }

        switch (UserRole.valueOf(hasAccess)) {
            case ROLE_SCHOOL_ADMINISTRATOR:
                Long currentPosition = authorisationUtil.getCurrentPosition(request);
                try {
                    result = waitingQueueService.findAllByPositionId(currentPosition);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                    return ResponseEntity.status(500).body("Position didn't find");
                }
                break;
            case ROLE_SYSTEM_ADMINISTRATOR:
                result = waitingQueueService.findAll();
                break;
            default:
                return ResponseEntity.notFound().build();
        }
        return Optional.ofNullable(result)
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    private String hasAccess(Authentication authentication,
                             HttpServletRequest request) {
        String currentRole = authorisationUtil.getCurrentRole(request);
        return authentication.getAuthorities().contains(new SimpleGrantedAuthority(currentRole))
                ? currentRole : null;
    }
    //TODO create get by id
}
