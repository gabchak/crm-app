package xyz.thinkglobal.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import xyz.thinkglobal.dto.WaitingQueueEnrolleeDto;
import xyz.thinkglobal.services.WaitingQueueEnrolleeService;

import java.util.List;
import java.util.Optional;

import static xyz.thinkglobal.util.RequestPathConstants.WAITING_QUEUE_ENROLLEE_CRUD;
import static xyz.thinkglobal.util.RequestPathConstants.WAITING_QUEUE_ENROLLEE_FIND_BY_WAITING_QUEUE_ID;

@RestController
public class WaitingQueueEnrolleeController {

    private WaitingQueueEnrolleeService waitingQueueEnrolleeService;

    @Autowired
    public WaitingQueueEnrolleeController(WaitingQueueEnrolleeService waitingQueueEnrolleeService) {
        this.waitingQueueEnrolleeService = waitingQueueEnrolleeService;
    }

    @PostMapping(WAITING_QUEUE_ENROLLEE_CRUD)
    public ResponseEntity<WaitingQueueEnrolleeDto> insert(@RequestBody WaitingQueueEnrolleeDto waitingQueueEnrolleeDto) {
        return Optional.of(waitingQueueEnrolleeService.save(waitingQueueEnrolleeDto))
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.badRequest()::build);
    }

    @DeleteMapping(WAITING_QUEUE_ENROLLEE_CRUD)
    public void deleteById(@RequestParam Long id) {
        waitingQueueEnrolleeService.deleteById(id);
    }

    @GetMapping(WAITING_QUEUE_ENROLLEE_FIND_BY_WAITING_QUEUE_ID + "/{waitingQueueId}")
    public ResponseEntity<List<WaitingQueueEnrolleeDto>> getByWaitingQueueId(@PathVariable Long waitingQueueId) {
        return Optional.of(waitingQueueEnrolleeService.findAllByWaitingQueueId(waitingQueueId))
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }
}
