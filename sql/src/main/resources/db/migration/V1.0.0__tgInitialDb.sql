-- DROP SCHEMA IF EXISTS public CASCADE;
-- CREATE SCHEMA public;

CREATE SEQUENCE hibernate_sequence START 1 INCREMENT 1;
CREATE TABLE address (
  id         BIGSERIAL NOT NULL,
  flat       VARCHAR(255),
  house      VARCHAR(255),
  street     VARCHAR(255),
  fk_city_id INT8,
  PRIMARY KEY (id)
);
CREATE TABLE booking (
  id                BIGSERIAL NOT NULL,
  capacity          INT4,
  name              VARCHAR(255),
  open_date         DATE,
  status            VARCHAR(255),
  students_quantity INT4,
  study_group_id    INT8,
  PRIMARY KEY (id)
);
CREATE TABLE booking_details (
  id                BIGSERIAL NOT NULL,
  date_to           DATE,
  expiration_date   DATE,
  registration_date DATE,
  booking_id        INT8,
  PRIMARY KEY (id)
);
CREATE TABLE child (
  birth_certificate          VARCHAR(255),
  date_of_birth              DATE,
  lead_family_representative INT8,
  id                         INT8 NOT NULL,
  PRIMARY KEY (id)
);
CREATE TABLE city (
  id           BIGSERIAL NOT NULL,
  name         VARCHAR(255),
  fk_region_id INT8,
  PRIMARY KEY (id)
);
CREATE TABLE contact (
  date_of_issue_of_passport  DATE,
  inn                        INT4 NOT NULL,
  lead                       INT8,
  series_and_passport_number VARCHAR(255),
  who_issued_passport        VARCHAR(255),
  id                         INT8 NOT NULL,
  registration_address_id    INT8,
  residence_address_id       INT8,
  PRIMARY KEY (id)
);
CREATE TABLE contact_students (
  family_representative_set_id INT8 NOT NULL,
  students_id                  INT8 NOT NULL,
  PRIMARY KEY (family_representative_set_id, students_id)
);
CREATE TABLE enrollee (
  primery_queue      INT8,
  id                 INT8 NOT NULL,
  booking_details_id INT8,
  fk_study_group_id  INT8,
  PRIMARY KEY (id)
);
CREATE TABLE event (
  id           BIGSERIAL NOT NULL,
  description  VARCHAR(255),
  end_date     TIMESTAMP,
  name         VARCHAR(255),
  start_date   TIMESTAMP,
  status       VARCHAR(255),
  type         VARCHAR(255),
  fk_school_id INT8,
  PRIMARY KEY (id)
);
CREATE TABLE event_speaker (
  events_id  INT8 NOT NULL,
  speaker_id INT8 NOT NULL,
  PRIMARY KEY (events_id, speaker_id)
);
CREATE TABLE lead (
  status     VARCHAR(255),
  id         INT8 NOT NULL,
  fk_city_id INT8,
  PRIMARY KEY (id)
);
CREATE TABLE lead_event (
  event_id INT8 NOT NULL,
  lead_id  INT8 NOT NULL,
  status   VARCHAR(255),
  PRIMARY KEY (event_id, lead_id)
);
CREATE TABLE person (
  id               BIGSERIAL NOT NULL,
  additional_phone VARCHAR(255),
  email            VARCHAR(255),
  first_name       VARCHAR(255),
  last_name        VARCHAR(255),
  middle_name      VARCHAR(255),
  phone            VARCHAR(255),
  PRIMARY KEY (id)
);
CREATE TABLE region (
  id   BIGSERIAL NOT NULL,
  name VARCHAR(255),
  PRIMARY KEY (id)
);
CREATE TABLE registered_user (
  password VARCHAR(255),
  id       INT8 NOT NULL,
  PRIMARY KEY (id)
);
CREATE TABLE role (
  id   SERIAL NOT NULL,
  role VARCHAR(255),
  PRIMARY KEY (id)
);
CREATE TABLE school (
  id                  BIGSERIAL NOT NULL,
  code                VARCHAR(255),
  name                VARCHAR(255),
  status              VARCHAR(255),
  term_of_partnership VARCHAR(255),
  address_id          INT8,
  PRIMARY KEY (id)
);
CREATE TABLE speaker (
  photo    VARCHAR(255),
  position VARCHAR(255),
  id       INT8 NOT NULL,
  PRIMARY KEY (id)
);
CREATE TABLE student (
  id                INT8 NOT NULL,
  fk_study_group_id INT8,
  PRIMARY KEY (id)
);
CREATE TABLE study_class (
  id               BIGSERIAL NOT NULL,
  name             INT4      NOT NULL,
  fk_school_id     INT8,
  waiting_queue_id INT8,
  PRIMARY KEY (id)
);
CREATE TABLE study_class_study_groups (
  study_class_id  INT8 NOT NULL,
  study_groups_id INT8 NOT NULL,
  PRIMARY KEY (study_class_id, study_groups_id)
);
CREATE TABLE study_group (
  id              BIGSERIAL NOT NULL,
  close_date      DATE,
  name            VARCHAR(255),
  number_of_seats INT4      NOT NULL,
  open_date       DATE,
  status          VARCHAR(255),
  booking_id      INT8,
  study_class_id  INT8,
  PRIMARY KEY (id)
);
CREATE TABLE user_role (
  user_id INT8 NOT NULL,
  role_id INT4 NOT NULL,
  PRIMARY KEY (user_id, role_id)
);
CREATE TABLE waiting_queue (
  id                BIGSERIAL NOT NULL,
  registration_date DATE,
  status            VARCHAR(255),
  enrollee_id       INT8,
  study_class_id    INT8,
  PRIMARY KEY (id)
);
ALTER TABLE IF EXISTS child
  ADD CONSTRAINT UKn4bh2q0x9r6e1xi7xa6s7n4wb UNIQUE (birth_certificate);
ALTER TABLE IF EXISTS person
  ADD CONSTRAINT UKfwmwi44u55bo4rvwsv0cln012 UNIQUE (email);
ALTER TABLE IF EXISTS study_class_study_groups
  ADD CONSTRAINT UK_pmd56x3d3vies21kk74ctevlo UNIQUE (study_groups_id);
ALTER TABLE IF EXISTS address
  ADD CONSTRAINT FKcax0w5jqky9lsr8lywueyyyg3 FOREIGN KEY (fk_city_id) REFERENCES city;
ALTER TABLE IF EXISTS booking
  ADD CONSTRAINT FKmyldphjag9sivr1nxerhjnwvc FOREIGN KEY (study_group_id) REFERENCES study_group;
ALTER TABLE IF EXISTS booking_details
  ADD CONSTRAINT FKdi4hhcv3pwr6b14qfhf9gahex FOREIGN KEY (booking_id) REFERENCES booking;
ALTER TABLE IF EXISTS child
  ADD CONSTRAINT FK9ehroodol82gd0cewu3lkxcet FOREIGN KEY (id) REFERENCES registered_user;
ALTER TABLE IF EXISTS city
  ADD CONSTRAINT FKao0my7hkyofhuxrvmewj7yi5v FOREIGN KEY (fk_region_id) REFERENCES region;
ALTER TABLE IF EXISTS contact
  ADD CONSTRAINT FKmsu5cybcsty7g7gip4j38fwgb FOREIGN KEY (registration_address_id) REFERENCES address;
ALTER TABLE IF EXISTS contact
  ADD CONSTRAINT FK1j41hxrwfh04hle1evjwi7hsb FOREIGN KEY (residence_address_id) REFERENCES address;
ALTER TABLE IF EXISTS contact
  ADD CONSTRAINT FK3owr7hjuhwsq3b2l0hy6ohpik FOREIGN KEY (id) REFERENCES registered_user;
ALTER TABLE IF EXISTS contact_students
  ADD CONSTRAINT FKtoch5ql8put1rtjmmn0xthb2j FOREIGN KEY (students_id) REFERENCES student;
ALTER TABLE IF EXISTS contact_students
  ADD CONSTRAINT FKapbjxyo5612qsimhe2rlke7vc FOREIGN KEY (family_representative_set_id) REFERENCES contact;
ALTER TABLE IF EXISTS enrollee
  ADD CONSTRAINT FKk9qi1yak321yosbki4drn6geg FOREIGN KEY (booking_details_id) REFERENCES booking_details;
ALTER TABLE IF EXISTS enrollee
  ADD CONSTRAINT FK7i9vdyhac19ybecakna9v03df FOREIGN KEY (fk_study_group_id) REFERENCES study_group;
ALTER TABLE IF EXISTS enrollee
  ADD CONSTRAINT FK9kk3g5nvilvdu3s7qjtulpnao FOREIGN KEY (id) REFERENCES child;
ALTER TABLE IF EXISTS event
  ADD CONSTRAINT FKher9dfyhflkmvlrqthct1kh4e FOREIGN KEY (fk_school_id) REFERENCES school;
ALTER TABLE IF EXISTS event_speaker
  ADD CONSTRAINT FK94k9kfry25ekvjndhncstcci3 FOREIGN KEY (speaker_id) REFERENCES speaker;
ALTER TABLE IF EXISTS event_speaker
  ADD CONSTRAINT FKgmqovdoeahyolu9b2kay886g9 FOREIGN KEY (events_id) REFERENCES event;
ALTER TABLE IF EXISTS lead
  ADD CONSTRAINT FK29e8s7841h3t1h9jdtcrnfmjx FOREIGN KEY (fk_city_id) REFERENCES city;
ALTER TABLE IF EXISTS lead
  ADD CONSTRAINT FKac07ptkaeq6tk1cdfo7akdjfa FOREIGN KEY (id) REFERENCES person;
ALTER TABLE IF EXISTS lead_event
  ADD CONSTRAINT FKb4tx90sm9fwqkcxnax5vro2a8 FOREIGN KEY (event_id) REFERENCES event;
ALTER TABLE IF EXISTS lead_event
  ADD CONSTRAINT FK6tmoaitv4jqc9t79hp5etop6i FOREIGN KEY (lead_id) REFERENCES lead;
ALTER TABLE IF EXISTS registered_user
  ADD CONSTRAINT FK15c06g16g6qoaj8g765sbfmke FOREIGN KEY (id) REFERENCES person;
ALTER TABLE IF EXISTS school
  ADD CONSTRAINT FKje7ysjh9qami0jc33ykm13g1d FOREIGN KEY (address_id) REFERENCES address;
ALTER TABLE IF EXISTS speaker
  ADD CONSTRAINT FK88mkdac5p10ifr3nuf8y5lwa9 FOREIGN KEY (id) REFERENCES registered_user;
ALTER TABLE IF EXISTS student
  ADD CONSTRAINT FKb69yaj5r9cpu7w9o2ypfk5a1h FOREIGN KEY (fk_study_group_id) REFERENCES study_group;
ALTER TABLE IF EXISTS student
  ADD CONSTRAINT FKglhyb940ixgy056el8bc4gc3n FOREIGN KEY (id) REFERENCES child;
ALTER TABLE IF EXISTS study_class
  ADD CONSTRAINT FKp2pohgr6xod64vh8v99d0jjio FOREIGN KEY (fk_school_id) REFERENCES school;
ALTER TABLE IF EXISTS study_class
  ADD CONSTRAINT FK4yb7rpiv3i6jhli83s384itpy FOREIGN KEY (waiting_queue_id) REFERENCES waiting_queue;
ALTER TABLE IF EXISTS study_class_study_groups
  ADD CONSTRAINT FKnqvolwsnrht96ct8l7kbqwpsk FOREIGN KEY (study_groups_id) REFERENCES study_group;
ALTER TABLE IF EXISTS study_class_study_groups
  ADD CONSTRAINT FKoo9jjb7drw141mpcl79lwdnqr FOREIGN KEY (study_class_id) REFERENCES study_class;
ALTER TABLE IF EXISTS study_group
  ADD CONSTRAINT FKdht2rv3nxlbm8ljjjd355i99f FOREIGN KEY (booking_id) REFERENCES booking;
ALTER TABLE IF EXISTS study_group
  ADD CONSTRAINT FK1ku4wtb3py58x68mmjb4dnlhw FOREIGN KEY (study_class_id) REFERENCES study_class;
ALTER TABLE IF EXISTS user_role
  ADD CONSTRAINT FKa68196081fvovjhkek5m97n3y FOREIGN KEY (role_id) REFERENCES role;
ALTER TABLE IF EXISTS user_role
  ADD CONSTRAINT FK5500lra2lcwij6u8tr580kqy3 FOREIGN KEY (user_id) REFERENCES registered_user;
ALTER TABLE IF EXISTS waiting_queue
  ADD CONSTRAINT FKqgb96fls5pxhmkop8q449go1x FOREIGN KEY (enrollee_id) REFERENCES enrollee;
ALTER TABLE IF EXISTS waiting_queue
  ADD CONSTRAINT FK32aygf3kyuryd7gh4yqys033x FOREIGN KEY (study_class_id) REFERENCES study_class;