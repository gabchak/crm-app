ALTER TABLE lead ADD COLUMN email varchar(255) ;
ALTER TABLE registered_user ADD COLUMN email varchar(255) ;

UPDATE lead SET email=person.email
FROM person
where lead.id=person.id;
UPDATE registered_user SET email=person.email
FROM person
where registered_user.id=person.id;

ALTER TABLE lead ALTER COLUMN email SET NOT NULL;
ALTER TABLE registered_user ALTER COLUMN email SET NOT NULL;

ALTER TABLE person DROP COLUMN email;