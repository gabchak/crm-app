alter table school DROP column director;

alter table registered_user_schools add column
position VARCHAR(20) DEFAULT 'NOT_DEFINED';

alter table registered_user_schools add column
id BIGSERIAL;