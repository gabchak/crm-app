INSERT INTO role (role) VALUES ('ROLE_CONTACT_MANAGER');
INSERT INTO role (role) VALUES ('ROLE_SCHOOL_ADMINISTRATOR');
INSERT INTO role (role) VALUES ('ROLE_FAMILY_PARTNER');
INSERT INTO role (role) VALUES ('ROLE_FAMILY_REPRESENTATIVE');
INSERT INTO role (role) VALUES ('ROLE_PARTNER_MANAGER');
INSERT INTO role (role) VALUES ('ROLE_CONTRACT_MANAGER_BO');
INSERT INTO role (role) VALUES ('ROLE_SYSTEM_ADMINISTRATOR');
INSERT INTO role (role) VALUES ('ROLE_GUEST');

INSERT INTO person (id, first_name, last_name, middle_name)
 VALUES (1, 'test', 'test', 'test');
INSERT INTO person (id, first_name, last_name, middle_name)
 VALUES (2, 'lead', 'lead', 'lead');

INSERT INTO registered_user (password, email, id) VALUES ('$2a$10$R8tHM4CsVZ7oW7duKC1FLeCgXP4Y/vroDRqFQ/spCrlGwo1YMWfX2', 't.test@thinkglobal.xyz', 1);
INSERT INTO registered_user (password, email, id) VALUES ('$2a$10$R8tHM4CsVZ7oW7duKC1FLeCgXP4Y/vroDRqFQ/spCrlGwo1YMWfX2', 'l.guest@thinkglobal.xyz', 2);

INSERT INTO position (user_id, school_id, position, id, role) VALUES (1, null, 'SCHOOL_ADMINISTRATOR', 4, 2);
INSERT INTO position (user_id, school_id, position, id, role) VALUES (1, null, 'SYSTEM_ADMINISTRATOR', 2, 7);
INSERT INTO public.position (user_id, school_id, position, id, role) VALUES (2, null, 'GUEST', 5, 8);
