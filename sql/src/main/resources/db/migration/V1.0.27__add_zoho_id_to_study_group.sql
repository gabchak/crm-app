alter table study_group
  add column zoho_id bigint;

alter table study_group
  add constraint study_group_zoho_id_unique unique (zoho_id);
