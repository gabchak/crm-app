alter table booking_details
  add column enrollee_id bigint;

update booking_details
set enrollee_id=enrollee.id
from enrollee
where booking_details.id = enrollee.booking_details_id;

alter table booking_details
  add constraint fk_enrollee foreign key (enrollee_id) references enrollee(id);

alter table enrollee
  drop constraint fkk9qi1yak321yosbki4drn6geg;

alter table enrollee
  drop column booking_details_id;