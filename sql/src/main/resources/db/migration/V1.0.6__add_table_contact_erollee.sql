create table contact_enrollee (
    contact_id  bigint not null,
    enrollee_id bigint not null,
    primary key(contact_id, enrollee_id)
);

alter table if exists contact_enrollee
  add constraint fk_contact foreign key (contact_id) references contact;

alter table if exists contact_enrollee
  add constraint fk_enrollee foreign key (enrollee_id) references enrollee;