alter table registered_user
  add constraint fk_unique_email
    unique (email);