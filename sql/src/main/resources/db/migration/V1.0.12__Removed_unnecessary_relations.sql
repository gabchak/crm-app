ALTER TABLE study_group DROP CONSTRAINT fkdht2rv3nxlbm8ljjjd355i99f;
ALTER TABLE study_class DROP CONSTRAINT fk4yb7rpiv3i6jhli83s384itpy;
ALTER TABLE study_group DROP COLUMN booking_id;
ALTER TABLE study_class DROP COLUMN waiting_queue_id;
