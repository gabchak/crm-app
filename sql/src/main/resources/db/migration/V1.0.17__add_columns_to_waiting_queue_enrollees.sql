alter table waiting_queue_enrollees
  add column id bigserial;

alter table waiting_queue_enrollees
  add column status varchar(255);

alter table waiting_queue_enrollees
  drop constraint waiting_queue_enrollees_pkey;

alter table waiting_queue_enrollees
  add constraint wqe_pkey
    primary key (id);

alter table waiting_queue_enrollees
  add constraint wqe_unique
    unique (enrollee_id, waiting_queue_id);
