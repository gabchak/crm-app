ALTER TABLE registered_user_schools RENAME TO position;
ALTER TABLE position ADD COLUMN role INTEGER default 8;
UPDATE position SET role=user_role.role_id
FROM user_role
where position.user_id=user_role.user_id;
ALTER TABLE position ALTER COLUMN role SET NOT NULL;

DROP TABLE user_role;
ALTER TABLE position ALTER COLUMN position TYPE VARCHAR(100) USING position::VARCHAR(100);
