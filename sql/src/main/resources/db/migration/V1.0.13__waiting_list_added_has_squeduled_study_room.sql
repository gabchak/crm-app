ALTER TABLE waiting_queue DROP COLUMN hasScheduledStudyRoom;
ALTER TABLE waiting_queue ADD COLUMN has_scheduled_study_room VARCHAR(255);