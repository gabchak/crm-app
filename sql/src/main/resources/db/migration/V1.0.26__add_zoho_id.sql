alter table person
  add column zoho_id bigint;

alter table person
  add constraint person_zoho_id_unique unique (zoho_id);


alter table school
  add column zoho_id bigint;

alter table school
  add constraint school_zoho_id_unique unique (zoho_id);
