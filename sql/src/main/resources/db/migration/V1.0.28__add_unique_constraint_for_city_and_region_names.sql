alter table region
  add constraint region_name_unique unique (name);

alter table city
  add constraint city_name_unique unique (name);
