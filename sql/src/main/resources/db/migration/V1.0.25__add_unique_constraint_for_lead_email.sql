alter table lead
  add constraint fk_lead_unique_email
    unique (email);