alter table waiting_queue drop constraint fkqgb96fls5pxhmkop8q449go1x;

alter table waiting_queue drop column enrollee_id;

create table waiting_queue_enrollees (
    waiting_queue_id    bigint,
    enrollee_id         bigint,
    primary key (waiting_queue_id, enrollee_id)
);

alter table if exists waiting_queue_enrollees
    add constraint wqe_waiting_queue_id_fk foreign key (waiting_queue_id)
    references waiting_queue;

alter table if exists waiting_queue_enrollees
    add constraint wqe_enrollee_id_fk foreign key (enrollee_id)
    references enrollee;