--create schools
--SCHOOL KB
insert into address (id, fk_city_id, street, house)
VALUES (10, 280, 'п-кт Тичини', '18-В');

insert into school (id, code, name, status, term_of_partnership, address_id)
VALUES (3, 'KB', 'Київ.Березняки', 'Діюча', 'Власна', 10);

--SCHOOL KP
insert into address (id, fk_city_id, street, house)
VALUES (11, 280, 'п-кт П. Григоренка', '5-а');

insert into school (id, code, name, status, term_of_partnership, address_id)
VALUES (4, 'KP', 'Київ.Позняки', 'Діюча', 'Власна', 11);

--SCHOOL KO
insert into address (id, fk_city_id, street, house)
VALUES (12, 280, 'Оболонська Набережна', '19');

insert into school (id, code, name, status, term_of_partnership, address_id)
VALUES (5, 'KO', 'Київ.Оболонь', 'Діюча', 'Власна', 12);

--create study classes
--SCHOOL KB
insert into study_class (id, name, fk_school_id)
VALUES (3, 1, 3);
insert into study_class (id, name, fk_school_id)
VALUES (4, 2, 3);
insert into study_class (id, name, fk_school_id)
VALUES (5, 3, 3);
insert into study_class (id, name, fk_school_id)
VALUES (6, 4, 3);
insert into study_class (id, name, fk_school_id)
VALUES (7, 5, 3);
insert into study_class (id, name, fk_school_id)
VALUES (8, 6, 3);
insert into study_class (id, name, fk_school_id)
VALUES (9, 7, 3);
insert into study_class (id, name, fk_school_id)
VALUES (10, 8, 3);
insert into study_class (id, name, fk_school_id)
VALUES (11, 9, 3);
insert into study_class (id, name, fk_school_id)
VALUES (12, 10, 3);

--SCHOOL KP
insert into study_class (id, name, fk_school_id)
VALUES (13, 1, 4);
insert into study_class (id, name, fk_school_id)
VALUES (14, 2, 4);
insert into study_class (id, name, fk_school_id)
VALUES (15, 3, 4);
insert into study_class (id, name, fk_school_id)
VALUES (16, 4, 4);
insert into study_class (id, name, fk_school_id)
VALUES (17, 5, 4);
insert into study_class (id, name, fk_school_id)
VALUES (18, 6, 4);
insert into study_class (id, name, fk_school_id)
VALUES (19, 7, 4);
insert into study_class (id, name, fk_school_id)
VALUES (20, 8, 4);
insert into study_class (id, name, fk_school_id)
VALUES (21, 9, 4);
insert into study_class (id, name, fk_school_id)
VALUES (22, 10, 4);

--SCHOOL KO
insert into study_class (id, name, fk_school_id)
VALUES (23, 1, 5);
insert into study_class (id, name, fk_school_id)
VALUES (24, 2, 5);
insert into study_class (id, name, fk_school_id)
VALUES (25, 3, 5);
insert into study_class (id, name, fk_school_id)
VALUES (26, 4, 5);
insert into study_class (id, name, fk_school_id)
VALUES (27, 5, 5);
insert into study_class (id, name, fk_school_id)
VALUES (28, 6, 5);
insert into study_class (id, name, fk_school_id)
VALUES (29, 7, 5);
insert into study_class (id, name, fk_school_id)
VALUES (30, 8, 5);
insert into study_class (id, name, fk_school_id)
VALUES (31, 9, 5);
insert into study_class (id, name, fk_school_id)
VALUES (32, 10, 5);

--create study groups
--SCHOOL KB
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (3, '2020-06-01', 'KB1A', 14, '2019-09-01', 'NEW', 3);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (5, '2020-06-01', 'KB1B', 14, '2019-09-01', 'NEW', 3);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (6, '2020-06-01', 'KB1C', 12, '2019-09-01', 'NEW', 3);

insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (7, '2020-06-01', 'KB2A', 14, '2019-09-01', 'NEW', 4);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (8, '2020-06-01', 'KB2B', 12, '2019-09-01', 'NEW', 4);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (9, '2020-06-01', 'KB2C', 13, '2019-09-01', 'NEW', 4);

insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (10, '2020-06-01', 'KB3A', 14, '2019-09-01', 'NEW', 5);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (11, '2020-06-01', 'KB3B', 14, '2019-09-01', 'NEW', 5);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (12, '2020-06-01', 'KB3C', 14, '2019-09-01', 'NEW', 5);

insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (13, '2020-06-01', 'KB4A', 14, '2019-09-01', 'NEW', 6);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (14, '2020-06-01', 'KB4B', 14, '2019-09-01', 'NEW', 6);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (15, '2020-06-01', 'KB4C', 14, '2019-09-01', 'NEW', 6);

insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (16, '2020-06-01', 'KB5A', 14, '2019-09-01', 'NEW', 7);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (17, '2020-06-01', 'KB5B', 14, '2019-09-01', 'NEW', 7);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (18, '2020-06-01', 'KB5C', 14, '2019-09-01', 'NEW', 7);

insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (19, '2020-06-01', 'KB6A', 14, '2019-09-01', 'NEW', 8);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (20, '2020-06-01', 'KB6B', 14, '2019-09-01', 'NEW', 8);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (21, '2020-06-01', 'KB6C', 14, '2019-09-01', 'NEW', 8);

insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (22, '2020-06-01', 'KB7A', 14, '2019-09-01', 'NEW', 9);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (23, '2020-06-01', 'KB7B', 14, '2019-09-01', 'NEW', 9);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (24, '2020-06-01', 'KB7C', 14, '2019-09-01', 'NEW', 9);

insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (25, '2020-06-01', 'KB8A', 14, '2019-09-01', 'NEW', 10);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (26, '2020-06-01', 'KB8B', 14, '2019-09-01', 'NEW', 10);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (27, '2020-06-01', 'KB8C', 14, '2019-09-01', 'NEW', 10);

insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (28, '2020-06-01', 'KB9A', 14, '2019-09-01', 'NEW', 11);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (29, '2020-06-01', 'KB9B', 14, '2019-09-01', 'NEW', 11);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (29, '2020-06-01', 'KB9C', 14, '2019-09-01', 'NEW', 11);

insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (30, '2020-06-01', 'KB10A', 14, '2019-09-01', 'NEW', 12);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (31, '2020-06-01', 'KB10B', 14, '2019-09-01', 'NEW', 12);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (32, '2020-06-01', 'KB10C', 14, '2019-09-01', 'NEW', 12);

--SCHOOL KP
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (33, '2020-06-01', 'KP1A', 14, '2019-09-01', 'NEW', 13);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (34, '2020-06-01', 'KP1B', 14, '2019-09-01', 'NEW', 13);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (34, '2020-06-01', 'KP1C', 12, '2019-09-01', 'NEW', 13);

insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (34, '2020-06-01', 'KP2A', 14, '2019-09-01', 'NEW', 14);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (35, '2020-06-01', 'KP2B', 12, '2019-09-01', 'NEW', 14);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (36, '2020-06-01', 'KP2C', 13, '2019-09-01', 'NEW', 14);

insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (37, '2020-06-01', 'KP3A', 14, '2019-09-01', 'NEW', 15);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (38, '2020-06-01', 'KP3B', 14, '2019-09-01', 'NEW', 15);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (39, '2020-06-01', 'KP3C', 14, '2019-09-01', 'NEW', 15);

insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (40, '2020-06-01', 'KP4A', 14, '2019-09-01', 'NEW', 16);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (41, '2020-06-01', 'KP4B', 14, '2019-09-01', 'NEW', 16);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (42, '2020-06-01', 'KP4C', 14, '2019-09-01', 'NEW', 16);

insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (43, '2020-06-01', 'KP5A', 14, '2019-09-01', 'NEW', 17);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (44, '2020-06-01', 'KP5B', 14, '2019-09-01', 'NEW', 17);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (45, '2020-06-01', 'KP5C', 14, '2019-09-01', 'NEW', 17);

insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (46, '2020-06-01', 'KP6A', 14, '2019-09-01', 'NEW', 18);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (47, '2020-06-01', 'KP6B', 14, '2019-09-01', 'NEW', 18);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (48, '2020-06-01', 'KP6C', 14, '2019-09-01', 'NEW', 18);

insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (49, '2020-06-01', 'KP7A', 14, '2019-09-01', 'NEW', 19);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (50, '2020-06-01', 'KP7B', 14, '2019-09-01', 'NEW', 19);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (51, '2020-06-01', 'KP7C', 14, '2019-09-01', 'NEW', 19);

insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (52, '2020-06-01', 'KP8A', 14, '2019-09-01', 'NEW', 20);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (53, '2020-06-01', 'KP8B', 14, '2019-09-01', 'NEW', 20);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (54, '2020-06-01', 'KP8C', 14, '2019-09-01', 'NEW', 20);

insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (55, '2020-06-01', 'KP9A', 14, '2019-09-01', 'NEW', 21);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (56, '2020-06-01', 'KP9B', 14, '2019-09-01', 'NEW', 21);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (57, '2020-06-01', 'KP9C', 14, '2019-09-01', 'NEW', 21);

insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (58, '2020-06-01', 'KP10A', 14, '2019-09-01', 'NEW', 22);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (59, '2020-06-01', 'KP10B', 14, '2019-09-01', 'NEW', 22);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (60, '2020-06-01', 'KP10C', 14, '2019-09-01', 'NEW', 22);

--SCHOOL KO
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (63, '2020-06-01', 'KO1A', 14, '2019-09-01', 'NEW', 23);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (64, '2020-06-01', 'KO1B', 14, '2019-09-01', 'NEW', 23);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (65, '2020-06-01', 'KO1C', 12, '2019-09-01', 'NEW', 23);

insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (66, '2020-06-01', 'KO2A', 14, '2019-09-01', 'NEW', 24);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (67, '2020-06-01', 'KO2B', 12, '2019-09-01', 'NEW', 24);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (68, '2020-06-01', 'KO2C', 13, '2019-09-01', 'NEW', 24);

insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (69, '2020-06-01', 'KO3A', 14, '2019-09-01', 'NEW', 25);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (70, '2020-06-01', 'KO3B', 14, '2019-09-01', 'NEW', 25);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (71, '2020-06-01', 'KO3C', 14, '2019-09-01', 'NEW', 25);

insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (72, '2020-06-01', 'KO4A', 14, '2019-09-01', 'NEW', 26);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (73, '2020-06-01', 'KO4B', 14, '2019-09-01', 'NEW', 26);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (74, '2020-06-01', 'KO4C', 14, '2019-09-01', 'NEW', 26);

insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (75, '2020-06-01', 'KO5A', 14, '2019-09-01', 'NEW', 27);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (76, '2020-06-01', 'KO5B', 14, '2019-09-01', 'NEW', 27);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (77, '2020-06-01', 'KO5C', 14, '2019-09-01', 'NEW', 27);

insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (78, '2020-06-01', 'KO6A', 14, '2019-09-01', 'NEW', 28);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (79, '2020-06-01', 'KO6B', 14, '2019-09-01', 'NEW', 28);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (80, '2020-06-01', 'KO6C', 14, '2019-09-01', 'NEW', 28);

insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (81, '2020-06-01', 'KO7A', 14, '2019-09-01', 'NEW', 29);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (82, '2020-06-01', 'KO7B', 14, '2019-09-01', 'NEW', 29);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (83, '2020-06-01', 'KO7C', 14, '2019-09-01', 'NEW', 29);

insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (84, '2020-06-01', 'KO8A', 14, '2019-09-01', 'NEW', 30);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (85, '2020-06-01', 'KO8B', 14, '2019-09-01', 'NEW', 30);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (86, '2020-06-01', 'KO8C', 14, '2019-09-01', 'NEW', 30);

insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (87, '2020-06-01', 'KO9A', 14, '2019-09-01', 'NEW', 31);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (88, '2020-06-01', 'KO9B', 14, '2019-09-01', 'NEW', 31);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (89, '2020-06-01', 'KO9C', 14, '2019-09-01', 'NEW', 31);

insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (90, '2020-06-01', 'KO10A', 14, '2019-09-01', 'NEW', 32);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (91, '2020-06-01', 'KO10B', 14, '2019-09-01', 'NEW', 32);
insert into study_group (id, close_date, name, number_of_seats, open_date, status, study_class_id)
VALUES (92, '2020-06-01', 'KO10C', 14, '2019-09-01', 'NEW', 32);

--waiting lists
--SCHOOL KB
insert into waiting_queue (id, registration_date, status, study_class_id, expiration_date, condition_for_booking_opening, has_scheduled_study_room, unique_code)
values (3, '2019-02-28', 'OPEN', 3, '2020-09-01', 'condition_for_booking_opening', 'has_scheduled_study_room', 'KBW1901');
insert into waiting_queue (id, registration_date, status, study_class_id, expiration_date, condition_for_booking_opening, has_scheduled_study_room, unique_code)
values (4, '2019-02-28', 'OPEN', 4, '2020-09-01', 'condition_for_booking_opening', 'has_scheduled_study_room', 'KBW1902');
insert into waiting_queue (id, registration_date, status, study_class_id, expiration_date, condition_for_booking_opening, has_scheduled_study_room, unique_code)
values (5, '2019-02-28', 'OPEN', 5, '2020-09-01', 'condition_for_booking_opening', 'has_scheduled_study_room', 'KBW1903');
insert into waiting_queue (id, registration_date, status, study_class_id, expiration_date, condition_for_booking_opening, has_scheduled_study_room, unique_code)
values (6, '2019-02-28', 'OPEN', 6, '2020-09-01', 'condition_for_booking_opening', 'has_scheduled_study_room', 'KBW1904');
insert into waiting_queue (id, registration_date, status, study_class_id, expiration_date, condition_for_booking_opening, has_scheduled_study_room, unique_code)
values (7, '2019-02-28', 'OPEN', 7, '2020-09-01', 'condition_for_booking_opening', 'has_scheduled_study_room', 'KBW1905');
insert into waiting_queue (id, registration_date, status, study_class_id, expiration_date, condition_for_booking_opening, has_scheduled_study_room, unique_code)
values (8, '2019-02-28', 'OPEN', 8, '2020-09-01', 'condition_for_booking_opening', 'has_scheduled_study_room', 'KBW1906');
insert into waiting_queue (id, registration_date, status, study_class_id, expiration_date, condition_for_booking_opening, has_scheduled_study_room, unique_code)
values (9, '2019-02-28', 'OPEN', 9, '2020-09-01', 'condition_for_booking_opening', 'has_scheduled_study_room', 'KBW1907');
insert into waiting_queue (id, registration_date, status, study_class_id, expiration_date, condition_for_booking_opening, has_scheduled_study_room, unique_code)
values (10, '2019-02-28', 'OPEN', 10, '2020-09-01', 'condition_for_booking_opening', 'has_scheduled_study_room', 'KBW1908');
insert into waiting_queue (id, registration_date, status, study_class_id, expiration_date, condition_for_booking_opening, has_scheduled_study_room, unique_code)
values (11, '2019-02-28', 'OPEN', 11, '2020-09-01', 'condition_for_booking_opening', 'has_scheduled_study_room', 'KBW1909');
insert into waiting_queue (id, registration_date, status, study_class_id, expiration_date, condition_for_booking_opening, has_scheduled_study_room, unique_code)
values (12, '2019-02-28', 'OPEN', 12, '2020-09-01', 'condition_for_booking_opening', 'has_scheduled_study_room', 'KBW1910');

--SCHOOL KP
insert into waiting_queue (id, registration_date, status, study_class_id, expiration_date, condition_for_booking_opening, has_scheduled_study_room, unique_code)
values (13, '2019-02-28', 'OPEN', 13, '2020-09-01', 'condition_for_booking_opening', 'has_scheduled_study_room', 'KPW1901');
insert into waiting_queue (id, registration_date, status, study_class_id, expiration_date, condition_for_booking_opening, has_scheduled_study_room, unique_code)
values (14, '2019-02-28', 'OPEN', 14, '2020-09-01', 'condition_for_booking_opening', 'has_scheduled_study_room', 'KPW1902');
insert into waiting_queue (id, registration_date, status, study_class_id, expiration_date, condition_for_booking_opening, has_scheduled_study_room, unique_code)
values (15, '2019-02-28', 'OPEN', 15, '2020-09-01', 'condition_for_booking_opening', 'has_scheduled_study_room', 'KPW1903');
insert into waiting_queue (id, registration_date, status, study_class_id, expiration_date, condition_for_booking_opening, has_scheduled_study_room, unique_code)
values (16, '2019-02-28', 'OPEN', 16, '2020-09-01', 'condition_for_booking_opening', 'has_scheduled_study_room', 'KPW1904');
insert into waiting_queue (id, registration_date, status, study_class_id, expiration_date, condition_for_booking_opening, has_scheduled_study_room, unique_code)
values (17, '2019-02-28', 'OPEN', 17, '2020-09-01', 'condition_for_booking_opening', 'has_scheduled_study_room', 'KPW1905');
insert into waiting_queue (id, registration_date, status, study_class_id, expiration_date, condition_for_booking_opening, has_scheduled_study_room, unique_code)
values (18, '2019-02-28', 'OPEN', 18, '2020-09-01', 'condition_for_booking_opening', 'has_scheduled_study_room', 'KPW1906');
insert into waiting_queue (id, registration_date, status, study_class_id, expiration_date, condition_for_booking_opening, has_scheduled_study_room, unique_code)
values (19, '2019-02-28', 'OPEN', 19, '2020-09-01', 'condition_for_booking_opening', 'has_scheduled_study_room', 'KPW1907');
insert into waiting_queue (id, registration_date, status, study_class_id, expiration_date, condition_for_booking_opening, has_scheduled_study_room, unique_code)
values (20, '2019-02-28', 'OPEN', 20, '2020-09-01', 'condition_for_booking_opening', 'has_scheduled_study_room', 'KPW1908');
insert into waiting_queue (id, registration_date, status, study_class_id, expiration_date, condition_for_booking_opening, has_scheduled_study_room, unique_code)
values (21, '2019-02-28', 'OPEN', 21, '2020-09-01', 'condition_for_booking_opening', 'has_scheduled_study_room', 'KPW1909');
insert into waiting_queue (id, registration_date, status, study_class_id, expiration_date, condition_for_booking_opening, has_scheduled_study_room, unique_code)
values (22, '2019-02-28', 'OPEN', 22, '2020-09-01', 'condition_for_booking_opening', 'has_scheduled_study_room', 'KPW1910');

--SCHOOL KP
insert into waiting_queue (id, registration_date, status, study_class_id, expiration_date, condition_for_booking_opening, has_scheduled_study_room, unique_code)
values (23, '2019-02-28', 'OPEN', 23, '2020-09-01', 'condition_for_booking_opening', 'has_scheduled_study_room', 'KOW1901');
insert into waiting_queue (id, registration_date, status, study_class_id, expiration_date, condition_for_booking_opening, has_scheduled_study_room, unique_code)
values (24, '2019-02-28', 'OPEN', 24, '2020-09-01', 'condition_for_booking_opening', 'has_scheduled_study_room', 'KOW1902');
insert into waiting_queue (id, registration_date, status, study_class_id, expiration_date, condition_for_booking_opening, has_scheduled_study_room, unique_code)
values (25, '2019-02-28', 'OPEN', 25, '2020-09-01', 'condition_for_booking_opening', 'has_scheduled_study_room', 'KOW1903');
insert into waiting_queue (id, registration_date, status, study_class_id, expiration_date, condition_for_booking_opening, has_scheduled_study_room, unique_code)
values (26, '2019-02-28', 'OPEN', 26, '2020-09-01', 'condition_for_booking_opening', 'has_scheduled_study_room', 'KOW1904');
insert into waiting_queue (id, registration_date, status, study_class_id, expiration_date, condition_for_booking_opening, has_scheduled_study_room, unique_code)
values (27, '2019-02-28', 'OPEN', 27, '2020-09-01', 'condition_for_booking_opening', 'has_scheduled_study_room', 'KOW1905');
insert into waiting_queue (id, registration_date, status, study_class_id, expiration_date, condition_for_booking_opening, has_scheduled_study_room, unique_code)
values (28, '2019-02-28', 'OPEN', 28, '2020-09-01', 'condition_for_booking_opening', 'has_scheduled_study_room', 'KOW1906');
insert into waiting_queue (id, registration_date, status, study_class_id, expiration_date, condition_for_booking_opening, has_scheduled_study_room, unique_code)
values (29, '2019-02-28', 'OPEN', 29, '2020-09-01', 'condition_for_booking_opening', 'has_scheduled_study_room', 'KOW1907');
insert into waiting_queue (id, registration_date, status, study_class_id, expiration_date, condition_for_booking_opening, has_scheduled_study_room, unique_code)
values (30, '2019-02-28', 'OPEN', 30, '2020-09-01', 'condition_for_booking_opening', 'has_scheduled_study_room', 'KOW1908');
insert into waiting_queue (id, registration_date, status, study_class_id, expiration_date, condition_for_booking_opening, has_scheduled_study_room, unique_code)
values (31, '2019-02-28', 'OPEN', 31, '2020-09-01', 'condition_for_booking_opening', 'has_scheduled_study_room', 'KOW1909');
insert into waiting_queue (id, registration_date, status, study_class_id, expiration_date, condition_for_booking_opening, has_scheduled_study_room, unique_code)
values (32, '2019-02-28', 'OPEN', 32, '2020-09-01', 'condition_for_booking_opening', 'has_scheduled_study_room', 'KOW1910');

--bookings
--SCHOOL KB
insert into booking (id, capacity, name, open_date, status, free_places, study_group_id)
VALUES (3, 14, 'KBB1901A', '2019-02-28', 'NEW', 14, 3);
insert into booking (id, capacity, name, open_date, status, free_places, study_group_id)
VALUES (5, 14, 'KBB1901C', '2019-02-28', 'NEW', 14, 5);

insert into booking (id, capacity, name, open_date, status, free_places, study_group_id)
VALUES (6, 14, 'KBB1902A', '2019-02-28', 'NEW', 14, 6);
insert into booking (id, capacity, name, open_date, status, free_places, study_group_id)
VALUES (7, 14, 'KBB1902B', '2019-02-28', 'NEW', 14, 7);
insert into booking (id, capacity, name, open_date, status, free_places, study_group_id)
VALUES (8, 14, 'KBB1902C', '2019-02-28', 'NEW', 14, 8);

insert into booking (id, capacity, name, open_date, status, free_places, study_group_id)
VALUES (9, 14, 'KBB1903A', '2019-02-28', 'NEW', 14, 9);
insert into booking (id, capacity, name, open_date, status, free_places, study_group_id)
VALUES (10, 14, 'KBB1903B', '2019-02-28', 'NEW', 14, 10);
insert into booking (id, capacity, name, open_date, status, free_places, study_group_id)
VALUES (11, 14, 'KBB1903C', '2019-02-28', 'NEW', 14, 11);

insert into booking (id, capacity, name, open_date, status, free_places, study_group_id)
VALUES (12, 14, 'KBB1904A', '2019-02-28', 'NEW', 14, 12);
insert into booking (id, capacity, name, open_date, status, free_places, study_group_id)
VALUES (13, 14, 'KBB1904B', '2019-02-28', 'NEW', 14, 13);
insert into booking (id, capacity, name, open_date, status, free_places, study_group_id)
VALUES (14, 14, 'KBB1904C', '2019-02-28', 'NEW', 14, 14);

insert into booking (id, capacity, name, open_date, status, free_places, study_group_id)
VALUES (15, 14, 'KBB1905A', '2019-02-28', 'NEW', 14, 15);
insert into booking (id, capacity, name, open_date, status, free_places, study_group_id)
VALUES (16, 14, 'KBB1905B', '2019-02-28', 'NEW', 14, 16);
insert into booking (id, capacity, name, open_date, status, free_places, study_group_id)
VALUES (17, 14, 'KBB1905C', '2019-02-28', 'NEW', 14, 17);

--SCHOOL KP
insert into booking (id, capacity, name, open_date, status, free_places, study_group_id)
VALUES (33, 14, 'KPB1901A', '2019-02-28', 'NEW', 14, 33);
insert into booking (id, capacity, name, open_date, status, free_places, study_group_id)
VALUES (34, 14, 'KPB1901B', '2019-02-28', 'NEW', 14, 34);
insert into booking (id, capacity, name, open_date, status, free_places, study_group_id)
VALUES (35, 14, 'KPB1901C', '2019-02-28', 'NEW', 14, 35);

insert into booking (id, capacity, name, open_date, status, free_places, study_group_id)
VALUES (36, 14, 'KPB1902A', '2019-02-28', 'NEW', 14, 36);
insert into booking (id, capacity, name, open_date, status, free_places, study_group_id)
VALUES (37, 14, 'KPB1902B', '2019-02-28', 'NEW', 14, 37);
insert into booking (id, capacity, name, open_date, status, free_places, study_group_id)
VALUES (38, 14, 'KPB1902C', '2019-02-28', 'NEW', 14, 38);

insert into booking (id, capacity, name, open_date, status, free_places, study_group_id)
VALUES (39, 14, 'KPB1903A', '2019-02-28', 'NEW', 14, 39);
insert into booking (id, capacity, name, open_date, status, free_places, study_group_id)
VALUES (40, 14, 'KPB1903B', '2019-02-28', 'NEW', 14, 40);
insert into booking (id, capacity, name, open_date, status, free_places, study_group_id)
VALUES (41, 14, 'KPB1903C', '2019-02-28', 'NEW', 14, 41);

insert into booking (id, capacity, name, open_date, status, free_places, study_group_id)
VALUES (42, 14, 'KPB1904A', '2019-02-28', 'NEW', 14, 42);
insert into booking (id, capacity, name, open_date, status, free_places, study_group_id)
VALUES (43, 14, 'KPB1904B', '2019-02-28', 'NEW', 14, 43);
insert into booking (id, capacity, name, open_date, status, free_places, study_group_id)
VALUES (44, 14, 'KPB1904C', '2019-02-28', 'NEW', 14, 44);

insert into booking (id, capacity, name, open_date, status, free_places, study_group_id)
VALUES (45, 14, 'KPB1905A', '2019-02-28', 'NEW', 14, 45);
insert into booking (id, capacity, name, open_date, status, free_places, study_group_id)
VALUES (46, 14, 'KPB1905B', '2019-02-28', 'NEW', 14, 46);
insert into booking (id, capacity, name, open_date, status, free_places, study_group_id)
VALUES (47, 14, 'KPB1905C', '2019-02-28', 'NEW', 14, 47);

--SCHOOL KO
insert into booking (id, capacity, name, open_date, status, free_places, study_group_id)
VALUES (63, 14, 'KOB1901A', '2019-02-28', 'NEW', 14, 63);
insert into booking (id, capacity, name, open_date, status, free_places, study_group_id)
VALUES (64, 14, 'KOB1901B', '2019-02-28', 'NEW', 14, 64);
insert into booking (id, capacity, name, open_date, status, free_places, study_group_id)
VALUES (65, 14, 'KOB1901C', '2019-02-28', 'NEW', 14, 65);

insert into booking (id, capacity, name, open_date, status, free_places, study_group_id)
VALUES (66, 14, 'KOB1902A', '2019-02-28', 'NEW', 14, 66);
insert into booking (id, capacity, name, open_date, status, free_places, study_group_id)
VALUES (67, 14, 'KOB1902B', '2019-02-28', 'NEW', 14, 67);
insert into booking (id, capacity, name, open_date, status, free_places, study_group_id)
VALUES (68, 14, 'KPB1902C', '2019-02-28', 'NEW', 14, 68);

insert into booking (id, capacity, name, open_date, status, free_places, study_group_id)
VALUES (69, 14, 'KOB1903A', '2019-02-28', 'NEW', 14, 69);
insert into booking (id, capacity, name, open_date, status, free_places, study_group_id)
VALUES (70, 14, 'KOB1903B', '2019-02-28', 'NEW', 14, 70);
insert into booking (id, capacity, name, open_date, status, free_places, study_group_id)
VALUES (71, 14, 'KOB1903C', '2019-02-28', 'NEW', 14, 71);

insert into booking (id, capacity, name, open_date, status, free_places, study_group_id)
VALUES (72, 14, 'KOB1904A', '2019-02-28', 'NEW', 14, 72);
insert into booking (id, capacity, name, open_date, status, free_places, study_group_id)
VALUES (73, 14, 'KOB1904B', '2019-02-28', 'NEW', 14, 73);
insert into booking (id, capacity, name, open_date, status, free_places, study_group_id)
VALUES (74, 14, 'KOB1904C', '2019-02-28', 'NEW', 14, 74);

insert into booking (id, capacity, name, open_date, status, free_places, study_group_id)
VALUES (75, 14, 'KOB1905A', '2019-02-28', 'NEW', 14, 75);
insert into booking (id, capacity, name, open_date, status, free_places, study_group_id)
VALUES (76, 14, 'KOB1905B', '2019-02-28', 'NEW', 14, 76);
insert into booking (id, capacity, name, open_date, status, free_places, study_group_id)
VALUES (77, 14, 'KOB1905C', '2019-02-28', 'NEW', 14, 77);

--contacts
insert into address (id, fk_city_id, street, house)
VALUES (20, 280, 'п-кт Тичини', '1');

insert into person (id, first_name, last_name, middle_name)
VALUES (100, 'contact', 'contact', 'contact');

insert into lead (status, id, fk_city_id, how_you_find_us, email, phone, additional_phone)
VALUES ('IN_PROGRESS', 100, 280, 'FACEBOOK', 'contact@test.com', '0509998877', '0671112233');

insert into registered_user (password, id, email, phone, additional_phone)
VALUES ('$2a$10$R8tHM4CsVZ7oW7duKC1FLeCgXP4Y/vroDRqFQ/spCrlGwo1YMWfX2',
        100, 'contact@test.com', '0509998877', '0671112233');

insert into contact (date_of_issue_of_passport, inn, lead, series_and_passport_number, who_issued_passport, id,
                     registration_address_id, residence_address_id)
VALUES ('2000-01-01', '1234431', 100, 'TE 123598', 'TEST', 100, 20, 20);


insert into address (id, fk_city_id, street, house)
VALUES (21, 280, 'п-кт Тичини', '2');

insert into person (id, first_name, last_name, middle_name)
VALUES (101, 'contact', 'contact', 'contact');

insert into lead (status, id, fk_city_id, how_you_find_us, email, phone, additional_phone)
VALUES ('IN_PROGRESS', 101, 280, 'FACEBOOK', 'contact2@test.com', '0509998877', '0671112233');

insert into registered_user (password, id, email, phone, additional_phone)
VALUES ('$2a$10$R8tHM4CsVZ7oW7duKC1FLeCgXP4Y/vroDRqFQ/spCrlGwo1YMWfX2',
        101, 'contact2@test.com', '0509998877', '0671112233');

insert into contact (date_of_issue_of_passport, inn, lead, series_and_passport_number, who_issued_passport, id,
                     registration_address_id, residence_address_id)
VALUES ('2000-01-01', '1234431', 101, 'TE 123598', 'TEST', 101, 21, 21);



--enrollees
--contact 10
insert into person (id, first_name, last_name, middle_name)
VALUES (20, 'enrollee', 'sur_enrollee', 'mid_enrollee');

insert into child (birth_certificate, date_of_birth, major_family_representative, id)
VALUES ('0000001', '2005-05-05', 100, 20);

INSERT into enrollee (id)
VALUES (20);

insert into person (id, first_name, last_name, middle_name)
VALUES (21, 'enrollee', 'sur_enrollee', 'mid_enrollee');

insert into child (birth_certificate, date_of_birth, major_family_representative, id)
VALUES ('0000002', '2005-05-05', 100, 21);

INSERT into enrollee (id)
VALUES (21);

insert into person (id, first_name, last_name, middle_name)
VALUES (22, 'enrollee', 'sur_enrollee', 'mid_enrollee');

insert into child (birth_certificate, date_of_birth, major_family_representative, id)
VALUES ('0000003', '2005-05-05', 100, 22);

INSERT into enrollee (id)
VALUES (22);

insert into person (id, first_name, last_name, middle_name)
VALUES (23, 'enrollee', 'sur_enrollee', 'mid_enrollee');

insert into child (birth_certificate, date_of_birth, major_family_representative, id)
VALUES ('0000004', '2005-05-05', 100, 23);

INSERT into enrollee (id)
VALUES (23);

--contact 11
insert into person (id, first_name, last_name, middle_name)
VALUES (30, 'enrollee', 'sur_enrollee', 'mid_enrollee');

insert into child (birth_certificate, date_of_birth, major_family_representative, id)
VALUES ('0000010', '2005-05-05', 101, 30);

INSERT into enrollee (id)
VALUES (30);

insert into person (id, first_name, last_name, middle_name)
VALUES (31, 'enrollee', 'sur_enrollee', 'mid_enrollee');

insert into child (birth_certificate, date_of_birth, major_family_representative, id)
VALUES ('0000020', '2005-05-05', 101, 31);

INSERT into enrollee (id)
VALUES (31);

insert into person (id, first_name, last_name, middle_name)
VALUES (32, 'enrollee', 'sur_enrollee', 'mid_enrollee');

insert into child (birth_certificate, date_of_birth, major_family_representative, id)
VALUES ('0000030', '2005-05-05', 101, 32);

INSERT into enrollee (id)
VALUES (32);

insert into person (id, first_name, last_name, middle_name)
VALUES (33, 'enrollee', 'sur_enrollee', 'mid_enrollee');

insert into child (birth_certificate, date_of_birth, major_family_representative, id)
VALUES ('0000040', '2005-05-05', 101, 33);

INSERT into enrollee (id)
VALUES (33);

--waiting queue enrollee
insert into waiting_queue_enrollees (waiting_queue_id, enrollee_id, id, status)
VALUES (3, 20, 10, 'INACTIVE');

insert into waiting_queue_enrollees (waiting_queue_id, enrollee_id, id, status)
VALUES (3, 21, 11, 'ACTIVE');

insert into waiting_queue_enrollees (waiting_queue_id, enrollee_id, id, status)
VALUES (4, 22, 12, 'INACTIVE');

insert into waiting_queue_enrollees (waiting_queue_id, enrollee_id, id, status)
VALUES (5, 30, 13, 'ACTIVE');

insert into waiting_queue_enrollees (waiting_queue_id, enrollee_id, id, status)
VALUES (6, 31, 14, 'INACTIVE');
insert into waiting_queue_enrollees (waiting_queue_id, enrollee_id, id, status)
VALUES (6, 32, 15, 'ACTIVE');

--booking details
insert into booking_details (id, expiration_date, registration_date, booking_id, enrollee_id)
VALUES (10, '2020-04-01', '2019-02-28', 3, 20);
insert into booking_details (id, expiration_date, registration_date, booking_id, enrollee_id)
VALUES (11, '2020-04-01', '2019-02-28', 5, 22);
insert into booking_details (id, expiration_date, registration_date, booking_id, enrollee_id)
VALUES (12, '2020-04-01', '2019-02-28', 17, 31);

