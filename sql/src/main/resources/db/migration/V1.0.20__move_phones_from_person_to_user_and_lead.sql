alter table lead
  add column phone varchar(12);
alter table lead
  add column additional_phone varchar(12);

alter table registered_user
  add column phone varchar(12);
alter table registered_user
  add column additional_phone varchar(12);

update lead
set phone=person.phone
from person
where lead.id = person.id;

update lead
set additional_phone=person.additional_phone
from person
where lead.id = person.id;

update registered_user
set phone=person.phone
from person
where registered_user.id = person.id;

update registered_user
set additional_phone=person.additional_phone
from person
where registered_user.id = person.id;

alter table person
  drop column phone;
alter table person
  drop column additional_phone;

