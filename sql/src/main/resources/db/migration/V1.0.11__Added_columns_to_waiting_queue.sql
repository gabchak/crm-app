ALTER TABLE waiting_queue ADD COLUMN expiration_date DATE;
ALTER TABLE waiting_queue ADD COLUMN condition_for_booking_opening VARCHAR(255);
ALTER TABLE waiting_queue ADD COLUMN hasScheduledStudyRoom VARCHAR(255);
DROP TABLE study_class_study_groups;
CREATE UNIQUE INDEX position_user_id_school_id_position_uindex ON position (user_id, school_id, position);
CREATE INDEX address_flat_house_street_uindex ON address (flat, house, street, fk_city_id);