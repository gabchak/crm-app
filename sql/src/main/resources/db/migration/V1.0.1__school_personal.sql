create table registered_user_schools(
  user_id   bigint,
  school_id bigint,
  constraint fk_user
    foreign key (user_id)
      references registered_user(id),
  constraint fk_school
    foreign key (school_id)
      references school(id)
);

alter table school add column director bigint;